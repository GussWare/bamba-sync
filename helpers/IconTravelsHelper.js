import IconTravelStyleBackOffice from '../services/backoffice/IconTravelStyleBackOffice';

/**
 * Recover information from icon travels
 * 
 * @param {int} IconTravel unique keyston id
 * @returns {object} Travel style
 */
exports.getIconTravel = async (IconTravel) => {
    var icon = {};
    var iconTravelStyle = new IconTravelStyleBackOffice();
    var responseTravel = await iconTravelStyle.get({
        id: IconTravel
    });

    if (responseTravel.status == global.constants.STATUS_200_OK) {
        icon = responseTravel.body["icon travel style"];
    }

    return icon;
};