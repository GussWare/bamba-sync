'use strict'

import jwt from 'jwt-simple';
import moment from 'moment';

exports.encode = async (payload) => {

    payload.sub = payload.UserID;
    payload.iat = moment().unix();
    payload.exp = moment().add(1, 'days').unix;

    return jwt.encode(payload, global.config.jwt.secret);
};