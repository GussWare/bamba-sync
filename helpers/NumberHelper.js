'use strict'

exports.isInt = async (num) =>{

    return num % 1 === 0;
}

exports.formatNumber = async (num, decimales = 2) => {
    var signo = (num >= 0 ? 1 : -1);
    num = num * signo;

    if (decimales === 0) {
        return signo * Math.round(num);
    }

    num = num.toString().split('e');
    num = Math.round(+(num[0] + 'e' + (num[1] ? (+num[1] + decimales) : decimales)));
    num = num.toString().split('e');

    var total = signo * (num[0] + 'e' + (num[1] ? (+num[1] - decimales) : -decimales));

    if (await exports.isInt(total)) {
        total = total.toString() + ".00";
    } else {
        var totalString = "" + total;
        var totalSplit = totalString.split(".");
        
        if(totalSplit.length > 0) {
            var decimalString = "" + totalSplit[1];
            if(decimalString.length < 2){
                total = totalSplit[0] + "." + decimalString + "0";
            }
        }
    }

    return total.toString();
};


exports.formatNumber2 = async (num, decimales, prefix = '') => {
    prefix = prefix || '';
    num += '';

    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : "";

    if (decimales !== undefined) {
        if (splitRight.length - 1 > decimales) {
            if (decimales == 0) {
                splitRight = "";
            } else {
                splitRight = splitRight.substr(0, decimales + 1);
            }
        } else {
            if (splitRight.length == 0 && decimales > 0) {
                splitRight = ".";
            }
            while (splitRight.length <= decimales) {
                splitRight += "0";
            }
        }
    }

    return prefix + splitLeft + splitRight;
}