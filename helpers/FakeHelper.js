'use strict'
import ConfirmAndPayBamba from '../services/bamba/ConfirmAndPayBamba';

exports.childrenAge = async (childrens) => {
    var childrenAge = [];

    for (var i = 0; i < childrens; i++) {
        childrenAge.push(global.constants.FAKE_CHILDREN_AGE);
    }

    return childrenAge;
};

exports.passengers = async (NumAdults, NumChildrens) => {

    var passengers = [];
    var passengerItem = {};
    var conta = 1;

    if (NumAdults > 0) {
        for (var i = 0; i < NumAdults; i++) {
            passengerItem = {};
            passengerItem.Name = 'Adult' + conta;
            passengerItem.Surname = 'Test';
            passengerItem.DateOfBirth = global.constants.PASSENGER_DATE_OF_BIRTH;

            passengers.push(passengerItem);
            conta++;
        }
    }

    if (NumChildrens > 0) {
        conta = 1;
        for (var j = 0; j < NumChildrens; j++) {

            passengerItem = {};
            passengerItem.Name = 'Children' + conta;
            passengerItem.Surname = 'Test';
            passengerItem.DateOfBirth = global.constants.PASSENGER_CHILDREN_DATE_OF_BIRTH;

            passengers.push(passengerItem);
            conta++;
        }
    }

    return passengers;
}

exports.reservationItems = async (ReservationItems) => {

    var reservationItems = [];
    var reservationItem = {};

    if (Array.isArray(ReservationItems)) {

        for (const i in ReservationItems) {
            reservationItem = {};

            if (ReservationItems[i].UnitID) {
                reservationItem.UnitID = ReservationItems[i].UnitID;
            }

            reservationItem.StartDate = ReservationItems[i].StartDate;
            reservationItem.EndDate = ReservationItems[i].EndDate;
            reservationItem.Passengers = await exports.passengers(ReservationItems[i].Adults, ReservationItems[i].Childrens);
            reservationItem.SelectedServices = ReservationItems[i].Services;

            reservationItems.push(reservationItem);
        }
    }

    return reservationItems;
}

exports.adhocFake = async (Customer) => {

    const confirmAndPayBamba = new ConfirmAndPayBamba();

    return {
        AdHocItemName: "Tailor made trip form inquiry",
        StartDate: "2019-01-16",
        EndDate: "2019-10-19",
        Passengers: [
            {
                Name: await confirmAndPayBamba.getPassengerName(Customer.FullName),
                Surname: await confirmAndPayBamba.getPassengerSurname(Customer.FullName),
                DateOfBirth: Customer.BirthDate
            }
        ],
        SelectedServices: [
            {
                ServiceID: 205,
                Amount:1,
                AdHocPrice: 0
            }
        ]
    }
}