'use strict'

exports.ArrayUnique = async (arr) => {
    var len = arr.length,
        out = [],
        obj = {},
        i=0;

    for (i = 0; i < len; i++) {
        obj[arr[i]] = 0;
    }

    for (i in obj) {
        out.push(i);
    }

    return out;
};


exports.returnPositionRandom = async (arr) => {
    var size = arr.length;
    var numRandom = Math.round(Math.random() * (size - 1));
    var newArr = [];

    if(typeof arr[numRandom] != "undefined") {
        newArr.push(arr[numRandom]);
        return newArr;
    } else {
        if(size > 0) {
            newArr.push(arr[0]);
            return newArr;
        }
    }

    return null;
}