import TravelBackOffice from '../services/backoffice/TravelStyleBackOffice';

/**
 * Retrieve the information of a keystone travel style based on your lemax id
 * 
 * @param {int} CategoryID unique lemax identifier
 * @returns {object} Travel style
 */
exports.getByLemaxId = async (CategoryID) => {
    var style = {};
    var styleBackOffice = new TravelBackOffice();
    var responseTravel = await styleBackOffice.getAll({
        page: 1,
        perPage: 1,
        filter: '{"lemaxId":"' + CategoryID + '"}'
    });

    if (responseTravel.status == global.constants.STATUS_200_OK) {
        if (responseTravel.body["travel-styles"].results.length > 0) {
            style = responseTravel.body["travel-styles"].results[0];
        }
    }

    return style;
};