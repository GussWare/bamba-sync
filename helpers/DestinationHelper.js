'use strict'

import CountryDestinationModel from '../models/CountryDestinationModel';
import CountryModel from '../models/CountryModel';
import DestinationModel from '../models/DestinationModel';
import ArrayHelper from './ArrayHelper';

/**
 * Retrieve a list of countries to which the Detinos belong
 * 
 * @param {array} DestinationList List of destinations
 * @returns {array} List of countries
 */
exports.getCountriesDestination = async (DestinationList) => {

    var destinationIDList = [];
    var countryIDList = [];
    var countries = [];

    for (const i in DestinationList.Destination) {
        destinationIDList.push(DestinationList.Destination[i].DestinationID);
    }

    destinationIDList = await ArrayHelper.ArrayUnique(destinationIDList);

    for (const i in destinationIDList) {
        destinationIDList[i] = parseInt(destinationIDList[i]);
    }

    var destinationCountryList = await CountryDestinationModel.find({ destinationLemaxId: { $in: destinationIDList } });

    for (const i in destinationCountryList) {
        countryIDList.push(destinationCountryList[i].countryLemaxId);
    }

    countryIDList = await ArrayHelper.ArrayUnique(countryIDList);

    for (const i in countryIDList) {
        countryIDList[i] = parseInt(countryIDList[i]);
    }

    countries = await CountryModel.find({ lemaxId: { $in: countryIDList } });

    return countries || [];
};

/**
 * Retrieve the name of the country to which the destination belongs
 * 
 * @param {int} idDestination Unique identifier of the destination
 * @returns {country} Country information 
 */
exports.getCountryDestination = async (idDestination) => {

    var country = null;
    var destinationCountry = await CountryDestinationModel.findOne({ destinationLemaxId: idDestination });

    if (destinationCountry) {
        country = await CountryModel.findOne({ lemaxId: destinationCountry.countryLemaxId });
    }

    return country;
}

/**
 * Retrieve the destination of keystone based on the id of lemax
 * 
 * @param {int} idDestination id of lemax
 * @returns {object|null} Destination information
 */
exports.getDestination = async (idDestination) => {
    var destination = await DestinationModel.findOne({
        lemaxId:idDestination
    });

    return destination;
}

exports.getDestinationByName = async (DestinationName) => {
    var destination = await DestinationModel.findOne({
        name:DestinationName
    });

    return destination;
}

exports.getCountriesBycountries = async (countries) => {
    var newcountries = [];
    var countriesSearch = [];

    if(Array.isArray(countries)) {
        for(const i in countries) {
            countriesSearch.push(countries[i].CountryID);
        }

        console.log(JSON.stringify(countriesSearch));
        newcountries = await CountryModel.find({ lemaxId: { $in: countriesSearch } });
    }

    return newcountries;
}