'use strict'
var moment = require('moment');

/**
 * Add a year to a date
 * 
 * @param {date} fechaInicial Initial date
 * @returns {date} Final date
 */
exports.addYear = async (fechaInicial) => {
  var startDate = new Date(fechaInicial);
  startDate.setFullYear(startDate.getFullYear() + 1);
  var endDate = startDate.toISOString().slice(0, 10).replace(/-/g, "-");

  return endDate;
};

exports.addDate = async (fechaInicial, numDates) => {
  var fechaMoment = moment(fechaInicial);
  var futureMountMoment = moment(fechaMoment).add(numDates, 'days').format('YYYY-MM-DD');

  return futureMountMoment;
};

/**
 * 
 */
exports.addMounts = async (fecha, mounts) => {
  var fechaMoment = moment(fecha);
  var futureMountMoment = moment(fechaMoment).add(mounts, 'M').format('YYYY-MM-DD');

  return futureMountMoment;
};

/**
 * Retrieve current day with yyymmdd format
 * 
 * @returns {date}
 */
exports.now = async () => {
  var dateNow = new Date();
  var startDay = dateNow.toISOString().slice(0, 10).replace(/-/g, "-");

  return startDay;
}

/**
 * Format yyyy-mm-dd by default to a date
 * 
 * @param {date}
 * @returns {date}
 */
exports.DefaultFormat = async (fecha) => {
  var dateFormat = new Date(fecha);
  var dateReturn = dateFormat.toISOString().slice(0, 10).replace(/-/g, "-");
  return dateReturn;
}

/**
 * retrieves days of difference between two dates
 * 
 * @param {date} startDat
 * @param {date} endDate
 * @returns {int}
 */
exports.daysDiff = async (startDat, endDate) => {
  var fecha1 = moment(startDat);
  var fecha2 = moment(endDate);

  return fecha2.diff(fecha1, 'days');
}

/**
 * Recover the day
 * 
 * @param {date} startDat
 * @returns {int}
 */
exports.getFormatDay = async (startDat) => {
  return moment(startDat).format('DD');
}

exports.orderDates = async (dates) => {

  dates.sort(function (a, b) {
    var c = new Date(a);
    var d = new Date(b);

    return c - d;
  });

  return dates;
}

exports.getDatesInYear = async (fechaInicio) => {
    fechaInicio = moment(fechaInicio);
    var fechas = [];

    for(var i = 1; i <= 365; i++) {
        fechas.push(fechaInicio.add(1, 'day').format('YYYY-MM-DD'));
    }

    return fechas;
}
