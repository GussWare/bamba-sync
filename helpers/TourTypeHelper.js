'use strict'

import TourTypeBackOffice from '../services/backoffice/TourTypeBackOffice';

exports.getTourTypes = async () => { 
    
    const tourTypeBackOffice = new TourTypeBackOffice();
    const response = await tourTypeBackOffice.getAll({
        page:1,
        perPage:100
    });

    return response.body["tour-types"].results;
};

exports.verifiType = async (tourType, typesArr) => { 

    var lemaxId = null;

    for(const i in typesArr) {
        if(typesArr[i]._id == tourType) {
            lemaxId = typesArr[i].lemaxId;
        }
    }

    return lemaxId;
}