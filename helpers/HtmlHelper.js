var HTMLParser = require('node-html-parser');

/**
 * Read a text and generate an arrangement with the structure of the html
 * 
 * @param {string} texto Html text
 * @returns {array} Structure html in an array
 */
exports.getHtmlParser = async (texto) => {

    var htmlRoot = HTMLParser.parse(texto);
    var htmlStructure = htmlRoot.childNodes || [];

    return htmlStructure;
};


/**
 * Generate an array with the information described in the html
 * 
 * @param {string} html
 * @returns {array}
 */
exports.getHtmlData = async (html) => {
    var htmlRoot = HTMLParser.parse(html);
    var htmlStructure = htmlRoot.childNodes || [];
    var childNodes = [];
    var data = [];

    for (const i in htmlStructure) {
        childNodes = [];
        if (htmlStructure[i].childNodes.length > 0) {
            childNodes = htmlStructure[i].childNodes;
            for (const j in childNodes) {
                if (childNodes[j].text != "" && childNodes[j].text != " ") {
                    data.push(childNodes[j].text.trim());
                }
            }
        }
    }

    return data;
}