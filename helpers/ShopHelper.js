'use strict'


exports.quitarFotosGallery = async (Gallery) => {

    var photos = [];
    var conta = 0;

    if (Array.isArray(Gallery)) {
        for (const i in Gallery) {

            if (conta <= 2) {
                if(typeof Gallery[i].PhotoID != "undefined") {
                    photos.push(Gallery[i]);
                }
            }

            conta++;
        }
    }

    return photos;
};

exports.sortby = async (sort) => {

    var sortint = 1;

    if(sort== 'Ascending') {
        sortint = 1;
    }

    if(sort == 'Descending') {
        sortint = -1;
    }

    return sortint;
};