'use strict'

import ThemeCategoriesService from '../services/backoffice/ThemeCategoriesBackOffice';

/**
 * Return data structure for the view
 * @param {*} PackageResult 
 */
exports.getThemeCategoriesIDList = async () => {

    var categoriesLis = [];
    var responseResult = [];
    var themeCatService = new ThemeCategoriesService();
    var response = await themeCatService.getAll({
        page: 1,
        perPage: 1000
    });

    if (response.status == global.constants.STATUS_200_OK) {
        responseResult = response.body["theme-categories"].results;
        if (responseResult.length > 0) {
            for(const i in responseResult) {
                if (typeof responseResult[i].lemaxId != "undefined") {
                    categoriesLis.push(responseResult[i].lemaxId);
                }
            }
        }
    }

    return categoriesLis;

};