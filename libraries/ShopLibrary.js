import DestinationHelper from '../helpers/DestinationHelper';
import ShopInterface from '../interface/shopInterface';
import TourLibrary from './TourLibrary';
import FitTourLibrary from './FitTourLibrary';
import InsertReservationBamba from '../services/bamba/InsertReservationBamba';
import StyleHelper from '../helpers/StyleHelper';
import LanguageHelper from '../helpers/LanguageHelper';

export default class ShopLibrary {

    constructor() {
        this.TourLibrary = new TourLibrary();
        this.FitTourLibrary = new FitTourLibrary();
        this.InsertReservationBamba = new InsertReservationBamba();
    }

    /**
     * Form the array of the product listing for the store page
     * 
     * @param {PackageResult} PackageResult 
     */
    async getProductsList(PackageResult) {

        var productList = [];
        var result = await ShopInterface.getShopInterface();
        var packageTour = PackageResult.PackageTourList.PackageTour;
        var atributeGroup = [];
        var atributeGroupListAtribute = [];
        var countriesList = [];

        for (const i in packageTour) {
            var tourData = await ShopInterface.getShopItemInterface();

            atributeGroup = [];
            atributeGroupListAtribute = [];

            tourData.TourID = packageTour[i].ObjectID || '';
            tourData.TourTitle = packageTour[i].Name || '';

            tourData.Type = (packageTour[i].ObjectType) ? packageTour[i].ObjectType.ObjectTypeID : null;
            tourData.TypeName = (packageTour[i].ObjectType) ? packageTour[i].ObjectType.ObjectTypeName : null;

            if (packageTour[i].PackageUnitList) {
                tourData.Galery = await this.getGallery(tourData.Type, packageTour[i].PackageUnitList.PackageUnit[0], packageTour[i].PhotoList);
                tourData.Mapa = await this.TourLibrary.getMapa({
                    Photo: tourData.Galery
                });
            }

            tourData.Style = (packageTour[i].CategoryList) ? [await StyleHelper.getByLemaxId(packageTour[i].CategoryList.Category[0].CategoryID)] : [];
            tourData.UnitID = (packageTour[i].PackageUnitList) ? packageTour[i].PackageUnitList.PackageUnit[0].UnitID : null;

            if (packageTour[i].PackageUnitList) {
                if (Array.isArray(packageTour[i].PackageUnitList.PackageUnit)) {
                    if (packageTour[i].PackageUnitList.PackageUnit.length > 0) {
                        tourData.PriceInfo = await this.TourLibrary.getPriceInfo(tourData.Type, packageTour[i].PackageUnitList.PackageUnit[0], packageTour[i].NoteList, null);
                    }
                }
            }

            if (packageTour[i].DestinationList) {
                countriesList = await DestinationHelper.getCountriesDestination(packageTour[i].DestinationList);

                if (countriesList.length > 0) {
                    tourData.Countries.push(countriesList[0]);
                    tourData.NumberOfCountries = countriesList.length;
                }
            }

            if (packageTour[i].AttributeGroupList) {
                atributeGroup = packageTour[i].AttributeGroupList.AttributeGroup

                for (const j in atributeGroup) {
                    if (atributeGroup[j].GroupID == global.constants.GROUP_NAME_MAIN_THEME) {
                        atributeGroupListAtribute = atributeGroup[j].AttributeList.Attribute;

                        for (const k in atributeGroupListAtribute) {
                            tourData.MainTheme.push({
                                MainThemeID: atributeGroupListAtribute[k].AttributeID,
                                MainThemeName: atributeGroupListAtribute[k].AttributeName,
                                MainThemeValue: atributeGroupListAtribute[k].AttributeValue
                            });
                        }
                    }

                    if (atributeGroup[j].GroupID == global.constants.GROUP_NAME_SIMPLE_TOUR_DEPARTURE_AND_ARRIBAL_INFO) {
                        atributeGroupListAtribute = atributeGroup[j].AttributeList.Attribute;

                        for (const k in atributeGroupListAtribute) {
                            if (atributeGroupListAtribute[k].AttributeID == global.constants.ATTRIBUTE_DURATION_DD) {
                                tourData.DurationDD = atributeGroupListAtribute[k].AttributeValue;
                            }

                            if (atributeGroupListAtribute[k].AttributeID == global.constants.ATTRIBUTE_DURATION_HR) {
                                tourData.DurationHR = atributeGroupListAtribute[k].AttributeValue;
                            }
                        }
                    }

                    // agregado para las stars
                    if (atributeGroup[j].GroupID == tourData.Type) {
                        atributeGroupListAtribute = atributeGroup[j].AttributeList.Attribute;
                        for (const k in atributeGroupListAtribute) {
                            if (atributeGroupListAtribute[k].AttributeID == global.constants.ATTRIBUTE_ACCOMODATION_TYPE) {
                                tourData.stars = atributeGroupListAtribute[k].AttributeValue;
                            }
                        }
                    }
                }
            }

            if (tourData.DurationHR && tourData.DurationDD == null) {
                tourData.DurationDD = 1;
            }

            productList.push(tourData);
        }

        result.PackageSearchResults = productList;
        result.Currency = (PackageResult.Currency) ? PackageResult.Currency : null;
        result.Pagination.CurrentPage = PackageResult.CurrentPage || '';
        result.Pagination.PageSize = PackageResult.PageSize || '';
        result.Pagination.TotalNumberOfResults = PackageResult.TotalNumberOfResults || '';

        return result;
    }

    /**
     * 
     */
    async getGallery(Type, PackageUnit, PhotoList) {

        var Gallery = await this.TourLibrary.getGallery(PhotoList);

        if (Gallery.length == 0) {
            if (Type == global.constants.TOUR_TYPE_FIT_TOUR) {

                var serviceID = null;
                var unitID = PackageUnit.UnitID;

                if (PackageUnit.ServiceList) {
                    if (Array.isArray(PackageUnit.ServiceList.Service)) {
                        if (PackageUnit.ServiceList.Service.length > 0) {
                            serviceID = PackageUnit.ServiceList.Service[0].ServiceID;
                        }
                    }
                }

                if (unitID && serviceID) {
                    var components = await this.InsertReservationBamba.getComponents(unitID, serviceID, global.constants.DEFAULT_CURRENCY_ID, global.constants.DEFAULT_PAYMENT_METHOD_ID, await LanguageHelper.getLang());

                    components = await this.FitTourLibrary.ordenarComponentes(components);

                    var photos = await this.FitTourLibrary.getGallery(Gallery, components);
                    var contaPhotos = 0;
                
                    Gallery = [];

                    for (const i in photos) {

                        Gallery.push(photos[i]);

                        if (contaPhotos >= 2) {
                            break;
                        }

                        contaPhotos++;
                    }
                }
            }
        }

        return Gallery;
    }
}