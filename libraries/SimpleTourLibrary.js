import TourInterface from '../interface/tourInterface';
import ItineraryInterface from '../interface/itinerary';
import SimpleTourInteface from '../interface/simpleTourIterface';
import CheckoutInterface from '../interface/checkoutInterface';
import TourLibrary from './TourLibrary';
import HtmlHelper from '../helpers/HtmlHelper';
import DestinationHelper from '../helpers/DestinationHelper';
import ReservationCanNotStartDatesBamba from '../services/bamba/ReservationCanNotStartDatesBamba';
import LanguageHelper from '../helpers/LanguageHelper';
import DateHelper from '../helpers/DateHelper';
import PriceForServiceListLibrary from '../libraries/PriceForServiceListLibrary';

export default class SimpleTourLibrary extends TourLibrary {

    constructor() {
        super();

        this.ReservationCanNotStartDatesBamba = new ReservationCanNotStartDatesBamba();
        this.PriceForServiceListLibrary = new PriceForServiceListLibrary();
    }

    /**
     * Retrieve information from the simple tour package
     * 
     * @param {PackageResult} PackageResult Reply from lemax api GetPackageSearchResult
     * @returns {object} Data of the simple tour package
     */
    async getPackage(PackageTour) {

        var htmlShort = [];
        var htmlDesc = [];
        var tourData = await TourInterface.getTourInterface();
        var countriesList = [];

        if (PackageTour) {

            tourData.TourID = PackageTour.ObjectID || '';
            tourData.TourTitle = PackageTour.Name || '';

            tourData.Type = (PackageTour.ObjectType) ? PackageTour.ObjectType.ObjectTypeID : null;
            tourData.TypeName = (PackageTour.ObjectType) ? PackageTour.ObjectType.ObjectTypeName : null;

            if (PackageTour.PackageUnitList) {
                if (Array.isArray(PackageTour.PackageUnitList.PackageUnit)) {
                    if (PackageTour.PackageUnitList.PackageUnit.length > 0) {
                        tourData.PriceInfo = await this.getPriceInfo(tourData.Type, PackageTour.PackageUnitList.PackageUnit[0], PackageTour.NoteList, null);
                    }
                }
            }

            tourData.Style = await this.getStyle(PackageTour.CategoryList);
            tourData.Mapa = await this.getMapa(PackageTour.PhotoList);
            tourData.Galery = await this.getGallery(PackageTour.PhotoList);
            tourData.MainTheme = await this.getMainTheme(PackageTour.AttributeGroupList);
            tourData.DurationDD = await this.getDurationDD(PackageTour.AttributeGroupList);
            tourData.BambaRecommendedTrip = await this.getBambaRecommended(PackageTour.AttributeGroupList);

            if (PackageTour.DestinationList) {
                countriesList = await DestinationHelper.getCountriesDestination(PackageTour.DestinationList);

                tourData.Countries = [];
                if (countriesList.length > 0) {
                    tourData.Countries.push(countriesList[0]);
                    tourData.NumberOfCountries = countriesList.length;
                }
            }

            tourData.StartDestination = await this.getStartDestination(PackageTour.DestinationList);
            tourData.EndDestination = await this.getEndDestination(tourData.Type, PackageTour.AttributeGroupList.AttributeGroup);

            if (!tourData.EndDestination) {
                tourData.EndDestination = tourData.StartDestination;
            }

            tourData.Themes = await this.getThemes(PackageTour.AttributeGroupList);

            htmlShort = await HtmlHelper.getHtmlParser(PackageTour.ShortDescription);
            tourData.Overview = await this.getOverview(htmlShort);
            tourData.Highlights = await this.getHighlights(htmlShort);
            htmlDesc = await HtmlHelper.getHtmlParser(PackageTour.Description);
            tourData.WhatYouGet = await this.getWhatYouGet(htmlDesc);
            tourData.WhatIsNotIncluded = await this.getWhatIsNotIncluded(htmlDesc);
            tourData.Itinerary = await this.getItinerary(htmlShort, htmlDesc);

            tourData.AccommodationPickupIncluded = await this.getAccomodationPickupIncluded(PackageTour.AttributeGroupList);
            tourData.AccommodationDropOffIncluded = await this.getAccomodationDropOffIncluded(PackageTour.AttributeGroupList);

            tourData.LocalPayment = await this.getLocalPayment(PackageTour.AttributeGroupList);
            
            if (tourData.LocalPayment) {
                tourData.WhatIsNotIncluded.push('*' + tourData.LocalPayment);
            }

            if (PackageTour.PackageUnitList) {
                if (Array.isArray(PackageTour.PackageUnitList.PackageUnit)) {
                    if (PackageTour.PackageUnitList.PackageUnit.length > 0) {
                        tourData.UnitID = await this.getUnitIDBasic(PackageTour.PackageUnitList.PackageUnit);
                        tourData.ServiceID = await this.getServiceIDBasic(PackageTour.PackageUnitList.PackageUnit);

                        tourData.AccommodationObjectList = await this.getAccomodationList(tourData.TourTitle, PackageTour.PackageUnitList.PackageUnit, true);
                        tourData.AccommodationObjectList = await this.PriceForServiceListLibrary.getPriceForServiceList(tourData.AccommodationObjectList, await DateHelper.now(), await DateHelper.now(), global.constants.DEFAULT_CURRENCY_ID, global.constants.DEFAULT_PAYMENT_METHOD_ID, await LanguageHelper.getLang());

                        if (tourData.UnitID) {
                            tourData.ListNotStartDays = await this.ReservationCanNotStartDatesBamba.getCanNotDates({
                                UnitID: tourData.UnitID
                            });
                        }
                    }
                }
            }

            tourData.Easy = await this.getEasy(PackageTour.AttributeGroupList);
            tourData.Moderate = await this.getModerate(PackageTour.AttributeGroupList);
            tourData.MildlyChallenging = await this.getMildlyChallenging(PackageTour.AttributeGroupList);
            tourData.Challenging = await this.getChallenging(PackageTour.AttributeGroupList);
            tourData.VeryChallenging = await this.getVeryChallenging(PackageTour.AttributeGroupList);

            tourData.NoteAdditionalInformation = await this.getNoteAdditionalInformation(PackageTour.NoteList);
            tourData.NoteImportantInformation = await this.getNoteImportantInformation(PackageTour.NoteList);
            tourData.NoteCoupon = await this.getNoteCupon(PackageTour.NoteList);
            tourData.NoteDiscount = await this.getNoteDiscount(PackageTour.NoteList);

            tourData.LanguageID = await LanguageHelper.getLang();
        }

        return tourData;
    }

    /**
     * 
     * @param {*} htmlStructureShort 
     * @param {*} htmlStructureDesc 
     */
    async getItinerary(htmlStructureShort, htmlStructureDesc) {
        var itinerary = await ItineraryInterface.getLevelItineraryInterface();
        var diasDesc = await this.getLongDescription(htmlStructureDesc);
        var diasShort = await this.getShortDescription(htmlStructureShort);
        var country = '';
        var lastDestination = '';
        var lastCountry = '';
        var destination = '';
        var totalDestination = 0;
        var contaDestination = 0;
        var contaCountry = 0;
        var day = '';

        for (const i in diasDesc) {
            for (const j in diasShort) {
                if (diasDesc[i].title == diasShort[j].title) {
                    day = diasDesc[i].title.split(":")[0].substring(3).trim();
                    destination = diasDesc[i].title.split(":")[1].split(",")[0].trim();
                    country = diasDesc[i].title.split(":")[1].split(",")[1].trim();

                    if (lastCountry != country) {
                        itinerary.Itinerary[contaCountry] = await ItineraryInterface.getLevelContryInterfeace();
                        itinerary.Itinerary[contaCountry].Country = country;
                        itinerary.Itinerary[contaCountry].StartDay = day;

                        lastCountry = country;
                        lastDestination = '';
                        contaDestination = 0;
                        contaCountry++;
                    }


                    if (lastDestination != destination) {
                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination] = await ItineraryInterface.getLevelDestinationInterface();
                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination].Destination = destination;
                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination].StartDay = day;

                        lastDestination = destination;
                        contaDestination++;
                        totalDestination++;
                    }

                    itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary.push({
                        Title: diasDesc[i].title,
                        LongDescription: diasDesc[i].longDescription,
                        ShortDescription: diasShort[j].shortDescription
                    });

                }
            }
        }

        var endDestination = null;
        var endDestinatonDay = 0;
        var endCountry = null;
        var destinationLength = 0;
        var diasLength = 0;

        for (const i in itinerary.Itinerary) {

            if (Array.isArray(itinerary.Itinerary[i].Itinerary)) {
                for (const j in itinerary.Itinerary[i].Itinerary) {
                    if (Array.isArray(itinerary.Itinerary[i].Itinerary[j].Itinerary)) {
                        diasLength = itinerary.Itinerary[i].Itinerary[j].Itinerary.length;

                        endDestination = itinerary.Itinerary[i].Itinerary[j].Itinerary[diasLength - 1];
                        endDestinatonDay = endDestination.Title.split(":")[0].substring(3).trim();

                        itinerary.Itinerary[i].Itinerary[j].EndDay = endDestinatonDay;
                        itinerary.Itinerary[i].Itinerary[j].Duration = ((parseInt(itinerary.Itinerary[i].Itinerary[j].EndDay) - parseInt(itinerary.Itinerary[i].Itinerary[j].StartDay)) + 1);
                    }
                }
            }

            destinationLength = itinerary.Itinerary[i].Itinerary.length;
            endCountry = itinerary.Itinerary[i].Itinerary[destinationLength - 1];

            itinerary.Itinerary[i].EndDay = endCountry.EndDay;
            itinerary.Itinerary[i].Duration = ((parseInt(itinerary.Itinerary[i].EndDay) - parseInt(itinerary.Itinerary[i].StartDay)) + 1);

        }

        itinerary.TotalDestination = totalDestination;

        // para crear todos los items faltantes en null
        itinerary.Itinerary = await this.addItemsItinerary(itinerary.Itinerary);

        return itinerary;
    }


    /**
         * 
         * @param {*} htmlStructureShort 
         */
    async getShortDescription(htmlStructureShort) {

        var childNodes = [];
        var contaHeaders = 0;
        var diasShort = [];
        var contaDias = 0;

        for (const i in htmlStructureShort) {

            if (typeof htmlStructureShort[i].tagName != 'undefined') {
                if (htmlStructureShort[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 3) {
                if (htmlStructureShort[i].tagName == 'h5') {
                    diasShort[contaDias] = await SimpleTourInteface.getSimpletourItineraryInterface();
                    diasShort[contaDias].title = htmlStructureShort[i].text.trim();
                    contaDias++;
                } else {
                    childNodes = [];
                    if (htmlStructureShort[i].childNodes.length > 0) {
                        childNodes = htmlStructureShort[i].childNodes;
                        for (const j in childNodes) {
                            if (childNodes[j].text != "" && childNodes[j].text != " ") {
                                diasShort[contaDias - 1].shortDescription.push(childNodes[j].text.trim());
                            }
                        }
                    }
                }
            }
        }

        return diasShort;
    }

    async getLongDescription(htmlStructureDesc) {

        var contaHeaders = 0;
        var diasDesc = [];
        var childNodes = [];
        var contaDias = 0;

        for (const i in htmlStructureDesc) {

            if (typeof htmlStructureDesc[i].tagName != 'undefined') {
                if (htmlStructureDesc[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 1) {
                if (htmlStructureDesc[i].tagName == 'h5') {
                    diasDesc[contaDias] = {
                        title: htmlStructureDesc[i].text.trim(),
                        longDescription: []
                    };
                    contaDias++;
                } else {
                    childNodes = [];
                    if (htmlStructureDesc[i].childNodes.length > 0) {
                        childNodes = htmlStructureDesc[i].childNodes;
                        for (const j in childNodes) {
                            if (childNodes[j].text != "" && childNodes[j].text != " ") {
                                diasDesc[contaDias - 1].longDescription.push(childNodes[j].text.trim());
                            }
                        }
                    }
                }
            }
        }

        return diasDesc;
    }

    /**
     * 
     * @param {*} atributeGroup 
     */
    async getAirportPickupIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_SIMPLE_TOUR_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_AIRPORT_PICKUP_INCLUDED, atributeGroupList);

        return value;
    }

    async getAccomodationPickupIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_SIMPLE_TOUR_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_PICKUP_INCLUDED, atributeGroupList);

        return value;
    }

    async getAccomodationDropOffIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_SIMPLE_TOUR_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_DROPOFF_INCLUDED, atributeGroupList);

        return value;
    }

    /**
     * Recover the duration in days
     * 
     * @param {array} atributeGroup 
     * @return {int} 
     */
    async getDurationDD(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_SIMPLE_TOUR_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_DURATION_DD, atributeGroupList);

        return value;
    }

    /**
    * Retrieve the information of the units and the additionalservices
    * 
    * @param {string} Name 
    * @param {array} PackageUnit 
    * @param {boolean} AdditionalService 
    * @returns {array}
    */
    async getAccomodationList(Name, PackageUnit, AdditionalService) {
        var AccommodationObjectList = [];
        var itemAccomodationObject = null;
        var typeGroup = null;

        if (PackageUnit.length > 0) {
            for (const i in PackageUnit) {

                itemAccomodationObject = await CheckoutInterface.getAccomodationItemInterface();
                itemAccomodationObject.UnitID = PackageUnit[i].UnitID;
                itemAccomodationObject.AvailabilityStatus = PackageUnit[i].AvailabilityStatus;
                itemAccomodationObject.ComponentName = Name;
                itemAccomodationObject.RoomOccupateRuleList = await this.getRoomRules(PackageUnit[i]);

                typeGroup = PackageUnit[i].Type.UnitTypeID;

                // sacar nombre
                if (PackageUnit[i].AttributeGroupList) {
                    if (Array.isArray(PackageUnit[i].AttributeGroupList.AttributeGroup)) {
                        if (PackageUnit[i].AttributeGroupList.AttributeGroup.length > 0) {
                            for (const j in PackageUnit[i].AttributeGroupList.AttributeGroup) {
                                if (PackageUnit[i].AttributeGroupList.AttributeGroup[j].GroupID == parseInt(typeGroup)) {
                                    if (PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList) {
                                        if (Array.isArray(PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute)) {
                                            for (const k in PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute) {
                                                if (PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute[k].AttributeID == global.constants.ATTRIBUTE_DESCRIPTION) {
                                                    itemAccomodationObject.UnitName = PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute[k].AttributeValue;
                                                    itemAccomodationObject.Description = PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute[k].AttributeValue;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (PackageUnit[i].ServiceList) {
                    if (Array.isArray(PackageUnit[i].ServiceList.Service)) {
                        if (PackageUnit[i].ServiceList.Service.length > 0) {
                            itemAccomodationObject.ServiceID = PackageUnit[i].ServiceList.Service[0].ServiceID;
                            itemAccomodationObject.ServiceName = PackageUnit[i].ServiceList.Service[0].ServiceName;
                        }
                    }

                    if (AdditionalService) {
                        itemAccomodationObject.AdditionalServicesList = await this.getServicesList(PackageUnit[i]);
                    }
                }

                AccommodationObjectList.push(itemAccomodationObject);
            }
        }

        return AccommodationObjectList;
    }

}

