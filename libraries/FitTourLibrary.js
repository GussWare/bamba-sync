import InsertReservationBamba from '../services/bamba/InsertReservationBamba';
import DestinationHelper from '../helpers/DestinationHelper';
import DateHelper from '../helpers/DateHelper';
import TourInterface from '../interface/tourInterface';
import HtmlHelper from '../helpers/HtmlHelper';
import TransportInterface from '../interface/transportInterface';
import AccomodationInterface from '../interface/accomodationInterface';
import ActivityInterface from '../interface/activityInterface';
import ItineraryInterface from '../interface/itinerary';
import SimpleTourInterface from '../interface/simpleTourIterface';
import TourLibrary from './TourLibrary';
import SimpleTourLibrary from './SimpleTourLibrary';
import ActivitiesTourLibrary from '../libraries/ActivitiesTourLibrary';
import ReservationCanNotStartDatesBamba from '../services/bamba/ReservationCanNotStartDatesBamba';
import LanguageHelper from '../helpers/LanguageHelper';
import CheckoutInterface from '../interface/checkoutInterface';
import ChildrenToursBamba from '../services/bamba/ChildrenToursBamba';
import PriceForServiceListLibrary from '../libraries/PriceForServiceListLibrary';

import moment from 'moment';

export default class FitTourLibrary extends TourLibrary {

    constructor() {
        super();

        this.lastStartDate = null;
        this.lastEndDate = null;
        this.lastStartDay = null;
        this.lastEndDay = null;

        this.ReservationCanNotStartDatesBamba = new ReservationCanNotStartDatesBamba();
        this.InsertReservationBamba = new InsertReservationBamba();
        this.ChildrenToursBamba = new ChildrenToursBamba();
        this.SimpleTourLibrary = new SimpleTourLibrary();
        this.ActivitiesTourLibrary = new ActivitiesTourLibrary();
        this.PriceForServiceListLibrary = new PriceForServiceListLibrary();
    }

    /**
     * Retrieve the information of a package of a fit tour
     * 
     * @param {PackageResult} PackageResult 
     * @returns {object} Data of the fit tour package
     */
    async getPackage(PackageTour) {

        var tourData = await TourInterface.getTourInterface();
        var htmlShortDescription = null;
        var listStars = [];
        var countriesList = [];


        if (PackageTour) {

            tourData.TourID = PackageTour.ObjectID || '';
            tourData.TourTitle = PackageTour.Name || '';

            tourData.Type = (PackageTour.ObjectType) ? PackageTour.ObjectType.ObjectTypeID : null;
            tourData.TypeName = (PackageTour.ObjectType) ? PackageTour.ObjectType.ObjectTypeName : null;

            if (PackageTour.PackageUnitList) {
                if (Array.isArray(PackageTour.PackageUnitList.PackageUnit)) {
                    if (PackageTour.PackageUnitList.PackageUnit.length > 0) {
                        tourData.PriceInfo = await this.getPriceInfo(tourData.Type, PackageTour.PackageUnitList.PackageUnit[0], PackageTour.NoteList, null);
                    }
                }
            }

            tourData.Style = await this.getStyle(PackageTour.CategoryList);
            tourData.Mapa = await this.getMapa(PackageTour.PhotoList);
            tourData.Galery = await super.getGallery(PackageTour.PhotoList);
            tourData.MainTheme = await this.getMainTheme(PackageTour.AttributeGroupList);
            tourData.BambaRecommendedTrip = await this.getBambaRecommended(PackageTour.AttributeGroupList);

            if (PackageTour.DestinationList) {
                countriesList = await DestinationHelper.getCountriesDestination(PackageTour.DestinationList);

                tourData.Countries = [];
                if (countriesList.length > 0) {
                    tourData.Countries.push(countriesList[0]);
                    tourData.NumberOfCountries = countriesList.length;
                }
            }

            tourData.StartDestination = await this.getStartDestination(tourData.Type, PackageTour.AttributeGroupList.AttributeGroup);
            tourData.EndDestination = await this.getEndDestination(tourData.Type, PackageTour.AttributeGroupList.AttributeGroup);

            if (!tourData.EndDestination) {
                tourData.EndDestination = tourData.StartDestination;
            }

            tourData.Themes = await this.getThemes(PackageTour.AttributeGroupList);

            htmlShortDescription = await HtmlHelper.getHtmlParser(PackageTour.ShortDescription);
            tourData.Overview = await this.getOverview(htmlShortDescription);
            tourData.Highlights = await this.getHighlights(htmlShortDescription);

            listStars = await this.getParentStars(tourData.TourID, PackageTour.AttributeGroupList);

            // llamada a inserts
            if (typeof PackageTour.PackageUnitList.PackageUnit != "undefined") {
                if (Array.isArray(PackageTour.PackageUnitList.PackageUnit)) {

                    tourData.UnitID = await this.getUnitIDBasic(PackageTour.PackageUnitList.PackageUnit);
                    tourData.ServiceID = await this.getServiceIDBasic(PackageTour.PackageUnitList.PackageUnit);

                    if (tourData.UnitID && tourData.ServiceID) {
                        var components = await this.InsertReservationBamba.getComponents(tourData.UnitID, tourData.ServiceID, global.constants.DEFAULT_CURRENCY_ID, global.constants.DEFAULT_PAYMENT_METHOD_ID, await LanguageHelper.getLang());
                        components = await this.ordenarComponentes(components);

                        tourData.Galery = await this.getGallery(tourData.Galery, components);

                        tourData.DurationDD = await this.getDurationDD(components);
                        tourData.WhatYouGet = await this.getWhatYouGet(listStars, components);
                        tourData.WhatIsNotIncluded = await this.getWhatIsNotIncluded(components);
                        tourData.Itinerary = await this.getItinerary(components);

                        tourData.AccommodationObjectList = await this.getAccomodationList(tourData.TourID,PackageTour.AttributeGroupList, PackageTour.PackageUnitList.PackageUnit, true);

                        tourData.ListNotStartDays = await this.ReservationCanNotStartDatesBamba.getCanNotDates({
                            UnitID: tourData.UnitID
                        });
                    }
                }
            }

            tourData.AccommodationPickupIncluded = await this.getAccomodationPickupIncluded(PackageTour.AttributeGroupList);
            tourData.AccommodationDropOffIncluded = await this.getAccomodationDropOffIncluded(PackageTour.AttributeGroupList);

            tourData.LocalPayment = await this.getLocalPayment(PackageTour.AttributeGroupList);
            if (tourData.LocalPayment) {
                tourData.WhatIsNotIncluded.push('*' + tourData.LocalPayment);
            }

            tourData.Easy = await this.getEasy(PackageTour.AttributeGroupList);
            tourData.Moderate = await this.getModerate(PackageTour.AttributeGroupList);
            tourData.MildlyChallenging = await this.getMildlyChallenging(PackageTour.AttributeGroupList);
            tourData.Challenging = await this.getChallenging(PackageTour.AttributeGroupList);
            tourData.VeryChallenging = await this.getVeryChallenging(PackageTour.AttributeGroupList);

            tourData.NoteAdditionalInformation = await this.getNoteAdditionalInformation(PackageTour.NoteList);
            tourData.NoteImportantInformation = await this.getNoteImportantInformation(PackageTour.NoteList);
            tourData.NoteCoupon = await this.getNoteCupon(PackageTour.NoteList);
            tourData.NoteDiscount = await this.getNoteDiscount(PackageTour.NoteList);

            tourData.LanguageID = await LanguageHelper.getLang();
        }

        return tourData;
    }

    /**
     * Recover the duration in days
     * 
     * @param {array} atributeGroup 
     * @return {int} 
     */
    async getDurationDD(insertReservationItemRS) {

        var durationDD = 0;
        var arrStartDate = [];
        var arrEndDate = [];
        var totalReservations = null;
        var datesLength = 0;

        if (Array.isArray(insertReservationItemRS)) {
            totalReservations = insertReservationItemRS.length;
            if (totalReservations > 0) {

                // recuperamos el primer valor
                for (const i in insertReservationItemRS) {

                    // accomodation or activity
                    if (typeof insertReservationItemRS[i].AccommodationObject != "undefined") {

                        if (insertReservationItemRS[i].AccommodationObject.DestinationID) {
                            arrStartDate.push(insertReservationItemRS[i].StartDate);
                            arrEndDate.push(insertReservationItemRS[i].EndDate);
                        }
                    }

                    // tranport
                    if (typeof insertReservationItemRS[i].Transportation != "undefined") {

                        if (insertReservationItemRS[i].TransportationUnit) {
                            if (insertReservationItemRS[i].TransportationUnit.DestinationList) {
                                if (Array.isArray(insertReservationItemRS[i].TransportationUnit.DestinationList.Destination)) {
                                    if (insertReservationItemRS[i].TransportationUnit.DestinationList.Destination.length > 0) {
                                        arrStartDate.push(insertReservationItemRS[i].StartDate);
                                        arrEndDate.push(insertReservationItemRS[i].EndDate);
                                    }
                                }
                            }
                        }
                    }

                    // simple tour
                    if (typeof insertReservationItemRS[i].PackageTour != "undefined") {
                        if (insertReservationItemRS[i].PackageTour.DestinationID) {
                            arrStartDate.push(insertReservationItemRS[i].StartDate);
                            arrEndDate.push(insertReservationItemRS[i].EndDate);
                        }
                    }
                }


                arrStartDate = await DateHelper.orderDates(arrStartDate);
                arrEndDate = await DateHelper.orderDates(arrEndDate);

                datesLength = arrEndDate.length;

                durationDD = (await DateHelper.daysDiff(arrStartDate[0], arrEndDate[datesLength - 1]) + 1);
            }
        }

        return durationDD;
    }

    /**
     * @override
     * @param {array} components 
     */
    async getWhatYouGet(listStars, components) {

        var WhatYouGet = [
            {
                number: 0,
                title: "Accommodation",
                subtitle: await LanguageHelper.lang('TXT_ACCOMODATION_SUBTITLE_DEFAULT'),
                list: [],
                listStars: listStars
            },
            {
                number: 0,
                title: "Transport",
                subtitle: "",
                list: []
            },
            {
                number: 0,
                title: "Meals",
                subtitle: "",
                list: []
            },
            {
                number: 0,
                title: "Activities",
                subtitle: await LanguageHelper.lang('TXT_ACTIVITIES_SUBTITLE_DEFAULT'),
                list: []
            },
            {
                number: null,
                title: "Guides",
                subtitle: await LanguageHelper.lang('TXT_GUIDE_SUBTITLE_DEFAULT'),
                list: []
            },
            {
                number: 0,
                title: "Extras",
                subtitle: "Texto por definir",
                list: []
            }
        ];

        var wathYouGetSimpleTour = [];
        var attributeGroup = [];
        var subActivities = [];
        var subAccomodation = [];
        var subTransportation = [];
        var subMeals = [];
        var htmlSctructure = [];
        var destination = '';

        if (Array.isArray(components)) {
            for (const i in components) {
                attributeGroup = [];
                htmlSctructure = [];

                if (typeof components[i].AccommodationUnit != "undefined") {
                    // accomodation
                    if (components[i].AccommodationUnit.Type.UnitTypeID != global.constants.ACCOMODATION_TYPE_ACTIVITY_ID) {
                        WhatYouGet[0].number++;

                        if (components[i].AccommodationUnit.AttributeGroupList) {
                            if (Array.isArray(components[i].AccommodationUnit.AttributeGroupList.AttributeGroup)) {
                                if (components[i].AccommodationUnit.AttributeGroupList.AttributeGroup.length > 0) {
                                    attributeGroup = components[i].AccommodationUnit.AttributeGroupList.AttributeGroup;
                                    if (attributeGroup.length > 0) {
                                        for (const j in attributeGroup) {
                                            if (attributeGroup[j].GroupID == components[i].AccommodationUnit.Type.UnitTypeID) {
                                                subAccomodation = await this.existeSubCategoria(attributeGroup[j].GroupID, attributeGroup[j].GroupName, subAccomodation);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (components[i].AccommodationObject) {

                            if (components[i].AccommodationObject.DestinationID) {
                                destination = await DestinationHelper.getDestination(components[i].AccommodationObject.DestinationID);
                                WhatYouGet[0].list = await this.existeAccomodationList(destination.lemaxId, destination.name, components[i].StartDate, components[i].EndDate, WhatYouGet[0].list);
                            }

                            // revisamos si hay meals
                            if (components[i].AccommodationObject.AttributeGroupList) {
                                if (Array.isArray(components[i].AccommodationObject.AttributeGroupList.AttributeGroup)) {
                                    if (components[i].AccommodationObject.AttributeGroupList.AttributeGroup.length > 0) {
                                        attributeGroup = components[i].AccommodationObject.AttributeGroupList.AttributeGroup;

                                        if (attributeGroup.length > 0) {
                                            for (const j in attributeGroup) {
                                                if (attributeGroup[j].GroupID == global.constants.GROUP_NAME_MEALS) {

                                                    if (attributeGroup[j].AttributeList) {
                                                        if (Array.isArray(attributeGroup[j].AttributeList.Attribute)) {
                                                            if (attributeGroup[j].AttributeList.Attribute.length > 0) {
                                                                for (const k in attributeGroup[j].AttributeList.Attribute) {
                                                                    if (attributeGroup[j].AttributeList.Attribute[k].AttributeID != global.constants.ATTRIBUTE_MEALS_SNACK) {
                                                                        WhatYouGet[2].number++;
                                                                        subMeals = await this.existeSubCategoria(attributeGroup[j].AttributeList.Attribute[k].AttributeID, attributeGroup[j].AttributeList.Attribute[k].AttributeName, subMeals);
                                                                        if (destination) {
                                                                            WhatYouGet[2].list = await this.existeMealsListado(destination.lemaxId, destination.name, attributeGroup[j].AttributeList.Attribute[k].AttributeID, await this.quitarNumerosMeals(attributeGroup[j].AttributeList.Attribute[k].AttributeName), WhatYouGet[2].list);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // activity 
                    if (components[i].AccommodationUnit.Type.UnitTypeID == global.constants.ACCOMODATION_TYPE_ACTIVITY_ID) {
                        WhatYouGet[3].number++;

                        if (components[i].AccommodationUnit.AttributeGroupList) {
                            if (Array.isArray(components[i].AccommodationUnit.AttributeGroupList.AttributeGroup)) {
                                if (components[i].AccommodationUnit.AttributeGroupList.AttributeGroup.length > 0) {
                                    attributeGroup = components[i].AccommodationUnit.AttributeGroupList.AttributeGroup;
                                    if (attributeGroup.length > 0) {
                                        for (const j in attributeGroup) {
                                            if (attributeGroup[j].GroupID == components[i].AccommodationUnit.Type.UnitTypeID) {
                                                subActivities = await this.existeSubCategoria(attributeGroup[j].GroupID, attributeGroup[j].GroupName, subActivities);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (components[i].AccommodationObject.Name) {
                            WhatYouGet[3].list.push(components[i].AccommodationObject.Name);

                            if (components[i].AccommodationObject.DestinationID) {
                                destination = await DestinationHelper.getDestination(components[i].AccommodationObject.DestinationID);
                            }

                            // revisamos si hay meals
                            if (components[i].AccommodationObject.AttributeGroupList) {
                                if (Array.isArray(components[i].AccommodationObject.AttributeGroupList.AttributeGroup)) {
                                    if (components[i].AccommodationObject.AttributeGroupList.AttributeGroup.length > 0) {
                                        attributeGroup = components[i].AccommodationObject.AttributeGroupList.AttributeGroup;

                                        if (attributeGroup.length > 0) {
                                            for (const j in attributeGroup) {
                                                if (attributeGroup[j].GroupID == global.constants.GROUP_NAME_MEALS) {

                                                    if (attributeGroup[j].AttributeList) {
                                                        if (Array.isArray(attributeGroup[j].AttributeList.Attribute)) {
                                                            if (attributeGroup[j].AttributeList.Attribute.length > 0) {
                                                                for (const k in attributeGroup[j].AttributeList.Attribute) {

                                                                    if (attributeGroup[j].AttributeList.Attribute[k].AttributeID != global.constants.ATTRIBUTE_MEALS_SNACK) {
                                                                        WhatYouGet[2].number++;
                                                                        subMeals = await this.existeSubCategoria(attributeGroup[j].AttributeList.Attribute[k].AttributeID, attributeGroup[j].AttributeList.Attribute[k].AttributeName, subMeals);
                                                                        if (destination) {
                                                                            WhatYouGet[2].list = await this.existeMealsListado(destination.lemaxId, destination.name, attributeGroup[j].AttributeList.Attribute[k].AttributeID, await this.quitarNumerosMeals(attributeGroup[j].AttributeList.Attribute[k].AttributeName), WhatYouGet[2].list);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

                // transporte
                if (typeof components[i].Transportation != "undefined") {
                    WhatYouGet[1].number++;

                    if (components[i].Transportation.Name) {
                        WhatYouGet[1].list.push(components[i].Transportation.Name);
                    }

                    if (components[i].TransportationUnit.AttributeGroupList) {
                        if (Array.isArray(components[i].TransportationUnit.AttributeGroupList.AttributeGroup)) {
                            if (components[i].TransportationUnit.AttributeGroupList.AttributeGroup.length > 0) {
                                attributeGroup = components[i].TransportationUnit.AttributeGroupList.AttributeGroup;

                                if (attributeGroup.length > 0) {
                                    for (const j in attributeGroup) {
                                        if (attributeGroup[j].GroupID == components[i].TransportationUnit.Type.UnitTypeID) {
                                            subTransportation = await this.existeSubCategoria(attributeGroup[j].GroupID, attributeGroup[j].GroupName, subTransportation);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // simple tour
                if (typeof components[i].PackageTour != "undefined") {
                    if (typeof components[i].PackageTour.Description != "undefined") {
                        htmlSctructure = await HtmlHelper.getHtmlParser(components[i].PackageTour.Description);
                        if (htmlSctructure.length > 0) {
                            wathYouGetSimpleTour.push(await super.getWhatYouGet(htmlSctructure));
                        }
                    }
                }
            }
        }

        if (WhatYouGet[0].list.length > 0) {
            WhatYouGet[0].list = await this.getListAccomodation(WhatYouGet[0].list);
        }

        if (WhatYouGet[2].list.length > 0) {
            WhatYouGet[2].list = await this.getListMeals(WhatYouGet[2].list);
        }

        WhatYouGet = await this.getMergeWhatYouGet(WhatYouGet, wathYouGetSimpleTour);

        if (subTransportation.length > 0) {
            WhatYouGet[1].subtitle = await this.getSubtitle(subTransportation);
        }

        if (subMeals.length > 0) {
            WhatYouGet[2].subtitle = await this.getSubtitle(subMeals, 3);
        }

        if (WhatYouGet[3].list.length > 0) {
            WhatYouGet[4].list = WhatYouGet[3].list;
        } else {
            WhatYouGet[4].subtitle = "";
        }

        return WhatYouGet;
    }

    /**
     * 
     * @param {array} WhatYouGet 
     * @param {array} WhatYouGetSimpleTour 
     * @returns {array}
     */
    async getMergeWhatYouGet(WhatYouGet, WhatYouGetSimpleTour) {

        for (const i in WhatYouGet) {

            for (const j in WhatYouGetSimpleTour) {

                for (const k in WhatYouGetSimpleTour[j]) {
                    if (i == k) {
                        WhatYouGet[i].number = (parseInt(WhatYouGet[i].number) + parseInt(WhatYouGetSimpleTour[j][k].number));
                        if (Array.isArray(WhatYouGetSimpleTour[j][k].list)) {
                            if (WhatYouGetSimpleTour[j][k].list.length > 0) {
                                for (const l in WhatYouGetSimpleTour[j][k].list) {
                                    WhatYouGet[i].list.push(WhatYouGetSimpleTour[j][k].list[l]);
                                }
                            }
                        }
                    }
                }
            }
        }

        return WhatYouGet;
    }

    /**
     * @override
     * @param {*} components 
     */
    async getWhatIsNotIncluded(components) {

        var whatIsNotIncluded = [];
        var superWhatIsNotIncluded = [];
        var htmlSctructure = null;

        if (Array.isArray(components)) {
            for (const i in components) {

                htmlSctructure = null;

                if (typeof components[i].AccommodationUnit != "undefined") {
                    if (components[i].AccommodationUnit.Type.UnitTypeID == global.constants.ACCOMODATION_TYPE_ACTIVITY_ID) {
                        htmlSctructure = await HtmlHelper.getHtmlParser(components[i].AccommodationObject.Description);
                        superWhatIsNotIncluded.push(await super.getWhatIsNotIncluded(htmlSctructure));
                    }
                }

                if (typeof components[i].PackageTour != "undefined") {
                    htmlSctructure = await HtmlHelper.getHtmlParser(components[i].PackageTour.Description);
                    superWhatIsNotIncluded.push(await super.getWhatIsNotIncluded(htmlSctructure));
                }
            }
        }

        for (const i in superWhatIsNotIncluded) {
            for (const j in superWhatIsNotIncluded[i]) {
                whatIsNotIncluded.push(superWhatIsNotIncluded[i][j]);
            }
        }

        return whatIsNotIncluded;

    }

    /**
     * 
     * @param {array} components 
     * @returns {array}
     */
    async getItinerary(components) {

        var itinerary = await ItineraryInterface.getLevelItineraryInterface();

        var transportation = null;
        var accomodation = null;
        var activity = null;
        var simpleTour = null;

        var lastCountryId = null;
        var lastDestinationId = null

        var country = null;
        var destination = null;
        var contaCountry = 0;
        var totalDestination = 0;
        var contaDestination = 0;

        if (Array.isArray(components)) {
            if (components.length > 0) {

                for (const i in components) {

                    if (typeof components[i].AccommodationUnit != "undefined") {
                        // accomodation
                        if (components[i].AccommodationUnit.Type.UnitTypeID != global.constants.ACCOMODATION_TYPE_ACTIVITY_ID) {
                            if (components[i].AccommodationObject.DestinationID) {
                                destination = await DestinationHelper.getDestination(components[i].AccommodationObject.DestinationID);

                                if (typeof destination != "undefined") {
                                    country = await DestinationHelper.getCountryDestination(destination.lemaxId);

                                    if (typeof country != "undefined") {

                                        // recuperamos los datos del accomodation
                                        accomodation = await this.getAccomodation(components[i]);
                                        accomodation.Country = country.name;
                                        accomodation.DestinationID = destination.lemaxId;
                                        accomodation.Destination = destination.name;

                                        // agrupacion por pais
                                        if (parseInt(lastCountryId) != parseInt(country.lemaxId)) {

                                            contaCountry++;
                                            contaDestination = 0;

                                            itinerary.Itinerary[contaCountry - 1] = await ItineraryInterface.getLevelContryInterfeace();
                                            itinerary.Itinerary[contaCountry - 1].Country = country.name;

                                            lastCountryId = parseInt(country.lemaxId);
                                        }

                                        // agrupacion por destino
                                        if (parseInt(lastDestinationId) != parseInt(destination.lemaxId)) {

                                            totalDestination++;
                                            contaDestination++;

                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1] = await ItineraryInterface.getLevelDestinationInterface();
                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Destination = destination.name;

                                            lastDestinationId = destination.lemaxId;
                                        }

                                        if (itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary.length == 0) {
                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0] = {};
                                        }

                                        if (typeof itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Accomodation == "undefined") {
                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Accomodation = [];
                                        }

                                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Accomodation.push(accomodation);
                                    }
                                }
                            }
                        }

                        // activity 
                        if (components[i].AccommodationUnit.Type.UnitTypeID == global.constants.ACCOMODATION_TYPE_ACTIVITY_ID) {
                            if (components[i].AccommodationObject.DestinationID) {
                                destination = await DestinationHelper.getDestination(components[i].AccommodationObject.DestinationID);

                                if (destination) {
                                    country = await DestinationHelper.getCountryDestination(destination.lemaxId);

                                    if (country) {

                                        activity = await this.getActivity(components[i]);
                                        activity.Country = country.name;
                                        activity.DestinationID = destination.lemaxId;
                                        activity.Destination = destination.name;

                                        if (parseInt(lastCountryId) != parseInt(country.lemaxId)) {
                                            contaCountry++;
                                            contaDestination = 0;

                                            itinerary.Itinerary[contaCountry - 1] = await ItineraryInterface.getLevelContryInterfeace();
                                            itinerary.Itinerary[contaCountry - 1].Country = country.name;

                                            lastCountryId = country.lemaxId;
                                        }

                                        if (parseInt(lastDestinationId) != parseInt(destination.lemaxId)) {
                                            contaDestination++;
                                            totalDestination++;

                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1] = await ItineraryInterface.getLevelDestinationInterface();
                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Destination = destination.name;

                                            lastDestinationId = destination.lemaxId;
                                        }

                                        if (itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary.length == 0) {
                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0] = {};
                                        }

                                        if (typeof itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Activities == "undefined") {
                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Activities = [];
                                        }

                                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Activities.push(activity);
                                    }
                                }
                            }
                        }
                    }

                    //transporte
                    if (typeof components[i].Transportation != "undefined") {
                        if (components[i].TransportationUnit.DestinationList) {
                            if (Array.isArray(components[i].TransportationUnit.DestinationList.Destination)) {
                                if (components[i].TransportationUnit.DestinationList.Destination.length > 0) {
                                    destination = components[i].TransportationUnit.DestinationList.Destination[0];

                                    if (destination) {
                                        country = await DestinationHelper.getCountryDestination(destination.DestinationID);

                                        if (country) {

                                            transportation = await this.getTransportation(components[i]);
                                            transportation.Country = country.name;
                                            transportation.DestinationID = destination.DestinationID;
                                            transportation.Destination = destination.DestinationName;

                                            if (parseInt(lastCountryId) != parseInt(country.lemaxId)) {
                                                contaCountry++;
                                                contaDestination = 0;

                                                itinerary.Itinerary[contaCountry - 1] = await ItineraryInterface.getLevelContryInterfeace();
                                                itinerary.Itinerary[contaCountry - 1].Country = country.name;
                                                lastCountryId = country.lemaxId;
                                            }

                                            if (parseInt(lastDestinationId) != parseInt(destination.DestinationID)) {
                                                lastDestinationId = destination.DestinationID;

                                                contaDestination++;
                                                totalDestination++;

                                                itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1] = await ItineraryInterface.getLevelDestinationInterface();
                                                itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Destination = destination.DestinationName;
                                            }

                                            if (itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary.length == 0) {
                                                itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0] = {};
                                            }

                                            if (typeof itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Transportation == "undefined") {
                                                itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Transportation = [];
                                            }

                                            itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].Transportation.push(transportation);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // simple tour
                    if (typeof components[i].PackageTour != "undefined") {
                        if (components[i].PackageTour.DestinationID) {
                            destination = await DestinationHelper.getDestination(components[i].PackageTour.DestinationID);
                            if (typeof destination != "undefined") {
                                country = await DestinationHelper.getCountryDestination(components[i].PackageTour.DestinationID);

                                if (typeof country != "undefined") {

                                    simpleTour = await this.getSimpleTour(components[i]);
                                    simpleTour.Country = country.name;
                                    simpleTour.DestinationID = destination.lemaxId;
                                    simpleTour.Destination = destination.name;

                                    if (parseInt(lastCountryId) != parseInt(country.lemaxId)) {
                                        contaCountry++;
                                        contaDestination = 0;

                                        itinerary.Itinerary[contaCountry - 1] = await ItineraryInterface.getLevelContryInterfeace();
                                        itinerary.Itinerary[contaCountry - 1].Country = country.name;
                                        lastCountryId = country.lemaxId;
                                    }

                                    if (parseInt(lastDestinationId) != parseInt(destination.lemaxId)) {
                                        contaDestination++;
                                        totalDestination++;

                                        lastDestinationId = destination.lemaxId;
                                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1] = await ItineraryInterface.getLevelDestinationInterface();
                                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Destination = destination.name;
                                    }

                                    if (itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary.length == 0) {
                                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0] = {};
                                    }

                                    if (typeof itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].SimpleTour == "undefined") {
                                        itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].SimpleTour = [];
                                    }

                                    itinerary.Itinerary[contaCountry - 1].Itinerary[contaDestination - 1].Itinerary[0].SimpleTour.push(simpleTour);
                                }
                            }
                        }
                    }
                }

                //itinerary = await this.validarFechasLibres(itinerary);

                var totalItemsDestino = 0;
                var fechaPrimerItem = null;
                var fechaUltimoItem = null;
                var itemDestinoInicial = null;
                var itemDestinoFInal = null;
                var DayPrimerItem = null;
                var DayUltimoItem = null;
                var StartDateFake = null;
                var diasDiff = null;

                // para calcular las fechas y dias
                if (itinerary.Itinerary.length > 0) {
                    // paises
                    for (const i in itinerary.Itinerary) {
                        if (typeof itinerary.Itinerary[i].Itinerary != "undefined") {
                            // destinos
                            totalItemsDestino = itinerary.Itinerary[i].Itinerary.length;
                            if (totalItemsDestino > 0) {
                                for (const j in itinerary.Itinerary[i].Itinerary) {
                                    if (typeof itinerary.Itinerary[i].Itinerary[j].Itinerary != "undefined") {

                                        if (itinerary.Itinerary[i].Itinerary[j].Itinerary[0]) {
                                            // itinerary
                                            fechaPrimerItem = await this.getPrimerFechaItem(itinerary.Itinerary[i].Itinerary[j].Itinerary[0]);
                                            fechaUltimoItem = await this.getUltimoFechaItem(itinerary.Itinerary[i].Itinerary[j].Itinerary[0]);

                                            DayPrimerItem = await this.getPrimerItem(itinerary.Itinerary[i].Itinerary[j].Itinerary[0]);
                                            DayUltimoItem = await this.getUltimoItem(itinerary.Itinerary[i].Itinerary[j].Itinerary[0]);

                                            if (this.lastEndDate) {
                                                StartDateFake = await DateHelper.addDate(this.lastEndDate, 1);
                                                diasDiff = await this.validarFechaLibre(StartDateFake, fechaPrimerItem);

                                                if (diasDiff >= 1) {
                                                    itinerary.Itinerary[i].Itinerary[j].StartDay = DayPrimerItem - diasDiff;
                                                    itinerary.Itinerary[i].Itinerary[j].EndDay = DayUltimoItem;

                                                    if (itinerary.Itinerary[i].Itinerary[j].StartDay && DayUltimoItem) {
                                                        itinerary.Itinerary[i].Itinerary[j].Duration = ((DayUltimoItem - DayPrimerItem) + 1);
                                                    }
                                                } else {
                                                    itinerary.Itinerary[i].Itinerary[j].StartDay = DayPrimerItem;
                                                    itinerary.Itinerary[i].Itinerary[j].EndDay = DayUltimoItem;

                                                    if (DayPrimerItem && DayUltimoItem) {
                                                        itinerary.Itinerary[i].Itinerary[j].Duration = ((DayUltimoItem - DayPrimerItem) + 1);
                                                    }
                                                }
                                            } else {
                                                itinerary.Itinerary[i].Itinerary[j].StartDay = DayPrimerItem;
                                                itinerary.Itinerary[i].Itinerary[j].EndDay = DayUltimoItem;

                                                if (DayPrimerItem && DayUltimoItem) {
                                                    itinerary.Itinerary[i].Itinerary[j].Duration = ((DayUltimoItem - DayPrimerItem) + 1);
                                                }
                                            }

                                            this.lastEndDate = fechaUltimoItem;
                                            this.lastEndDay = DayUltimoItem;
                                        }
                                    }
                                }

                                itemDestinoInicial = itinerary.Itinerary[i].Itinerary[0];
                                itemDestinoFInal = itinerary.Itinerary[i].Itinerary[totalItemsDestino - 1];

                                itinerary.Itinerary[i].StartDay = itemDestinoInicial.StartDay;
                                itinerary.Itinerary[i].EndDay = itemDestinoFInal.EndDay;

                                if (itemDestinoInicial.StartDay && itemDestinoFInal.EndDay) {
                                    itinerary.Itinerary[i].Duration = ((itemDestinoFInal.EndDay - itemDestinoInicial.StartDay) + 1);
                                }
                            }
                        }
                    }
                }
            }
        }

        itinerary.TotalDestination = totalDestination;

        // para crear todos los items faltantes en null
        itinerary.Itinerary = await this.addItemsItinerary(itinerary.Itinerary);

        return itinerary;
    }

    /**
     * Retrieve the accomodation information
     * 
     * @param {object}
     * @returns {object}
     */
    async getAccomodation(insertReservationItemRS) {

        var accomodation = await AccomodationInterface.getAccomodationInterface();
        var startDate = moment(insertReservationItemRS.StartDate).format('YYYY-MM-DD');
        var endDate = moment(insertReservationItemRS.EndDate).format('YYYY-MM-DD');

        var objectTypeID = null;
        var attributeGroup = [];
        var attibuteListAtribute = [];

        accomodation.ID = insertReservationItemRS.UnitID;
        accomodation.StartDate = startDate;
        accomodation.EndDate = endDate;
        accomodation.Duration = await this.getDuration(startDate, endDate);

        if (this.lastStartDate == null) {

            accomodation.StartDay = 1;
            accomodation.EndDay = await this.getCalEndDay(accomodation.StartDay, accomodation.Duration);
            accomodation.Day = accomodation.StartDay;

            this.lastStartDay = accomodation.StartDay;
            this.lastEndDay = accomodation.EndDay;

        } else {

            var f1 = moment(this.lastStartDate).format('YYYY-MM-DD');
            var f2 = moment(startDate).format('YYYY-MM-DD');

            if (f1 == f2) {
                accomodation.StartDay = this.lastStartDay;
                accomodation.EndDay = await this.getCalEndDay(accomodation.StartDay, accomodation.Duration);
                accomodation.Day = accomodation.StartDay;
            } else {
                accomodation.StartDay = await this.getCalStartDay(this.lastEndDate, startDate, this.lastEndDay);
                accomodation.EndDay = await this.getCalEndDay(accomodation.StartDay, accomodation.Duration);
                accomodation.Day = accomodation.StartDay;
            }

            this.lastStartDay = accomodation.StartDay;
            this.lastEndDay = accomodation.EndDay;
        }

        this.lastStartDate = startDate;
        this.lastEndDate = endDate;

        if (insertReservationItemRS.AccommodationObject.Name) {
            accomodation.Name = insertReservationItemRS.AccommodationObject.Name;
        }

        if (insertReservationItemRS.AccommodationObject.Description) {
            accomodation.LongDescription = await HtmlHelper.getHtmlData(insertReservationItemRS.AccommodationObject.Description);
        }

        if (insertReservationItemRS.AccommodationObject.ObjectType.ObjectTypeID) {
            objectTypeID = insertReservationItemRS.AccommodationObject.ObjectType.ObjectTypeID;
        }

        if (insertReservationItemRS.AccommodationObject.AttributeGroupList) {
            if (insertReservationItemRS.AccommodationObject.AttributeGroupList.AttributeGroup.length > 0) {
                attributeGroup = insertReservationItemRS.AccommodationObject.AttributeGroupList.AttributeGroup;

                for (const i in attributeGroup) {
                    if (attributeGroup[i].GroupID == objectTypeID) {

                        accomodation.ServiceType = attributeGroup[i].GroupName;

                        if (attributeGroup[i].AttributeList) {
                            if (attributeGroup[i].AttributeList.Attribute.length > 0) {
                                attibuteListAtribute = attributeGroup[i].AttributeList.Attribute;
                                for (const j in attibuteListAtribute) {

                                    // number of stars
                                    if (attibuteListAtribute[j].AttributeID == global.constants.ATTRIBUTE_NUMBER_OF_STARTS) {
                                        accomodation.NumberOfStars = attibuteListAtribute[j].AttributeValue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return accomodation;
    }

    /**
     * Create an object with activity information
     * 
     * @param {object} insertReservationItemRS Item of the xml component
     * @returns {object} Activity information
     */
    async getActivity(insertReservationItemRS) {
        var activity = await ActivityInterface.getActivityInterface();
        var startDate = moment(insertReservationItemRS.StartDate).format('YYYY-MM-DD');
        var endDate = moment(insertReservationItemRS.EndDate).format('YYYY-MM-DD');

        activity.ID = insertReservationItemRS.AccommodationObject.ObjectID;
        activity.StartDate = startDate;
        activity.EndDate = endDate;
        activity.Type = global.constants.TOUR_TYPE_ACTIVITIES_TOUR;
        activity.Duration = await this.getDuration(startDate, endDate);

        if (this.lastStartDate == null) {

            activity.StartDay = 1;
            activity.EndDay = await this.getCalEndDay(activity.StartDay, activity.Duration);
            activity.Day = activity.StartDay;

            this.lastStartDay = activity.StartDay;
            this.lastEndDay = activity.EndDay;

        } else {

            var f1 = moment(this.lastStartDate).format('YYYY-MM-DD');
            var f2 = moment(startDate).format('YYYY-MM-DD');

            if (f1 == f2) {
                activity.StartDay = this.lastStartDay;
                activity.EndDay = await this.getCalEndDay(activity.StartDay, activity.Duration);
                activity.Day = activity.StartDay;
            } else {
                activity.StartDay = await this.getCalStartDay(this.lastEndDate, startDate, this.lastEndDay);
                activity.EndDay = await this.getCalEndDay(activity.StartDay, activity.Duration);
                activity.Day = activity.StartDay;
            }

            this.lastStartDay = activity.StartDay;
            this.lastEndDay = activity.EndDay;
        }

        this.lastStartDate = startDate;
        this.lastEndDate = endDate;

        if (insertReservationItemRS.AccommodationObject.Name) {
            activity.Name = insertReservationItemRS.AccommodationObject.Name;
        }

        if (insertReservationItemRS.AccommodationObject.ShortDescription) {
            var shortDescription = await HtmlHelper.getHtmlParser(insertReservationItemRS.AccommodationObject.ShortDescription);
            activity.ShortDescription = await this.getShortDescription(shortDescription);
        }

        return activity;
    }

    /**
     * Group transport information into an object and return it
     * 
     * @param {object} insertReservationItemRS Item of the xml component
     * @returns {object} Transport information
     */
    async getTransportation(insertReservationItemRS) {

        var transportation = await TransportInterface.getTransportInterface();
        var startDate = moment(insertReservationItemRS.StartDate).format('YYYY-MM-DD');
        var endDate = moment(insertReservationItemRS.EndDate).format('YYYY-MM-DD');
        var objectTypeID = insertReservationItemRS.TransportationUnit.Type.UnitTypeID;
        var transUnitAttributeGroup = [];
        var transUnitAttribute = [];

        transportation.ID = insertReservationItemRS.UnitID;
        transportation.StartDate = startDate;
        transportation.EndDate = endDate;
        transportation.Duration = await this.getDuration(startDate, endDate);

        if (this.lastStartDate == null) {
            transportation.StartDay = 1;
            transportation.EndDay = await this.getCalEndDay(transportation.StartDay, transportation.Duration);
            transportation.Day = transportation.StartDay;

            this.lastStartDay = transportation.StartDay;
            this.lastEndDay = transportation.EndDay;
        } else {

            var f1 = moment(this.lastStartDate).format('YYYY-MM-DD');
            var f2 = moment(startDate).format('YYYY-MM-DD');

            if (f1 == f2) {
                transportation.StartDay = this.lastStartDay;
                transportation.EndDay = await this.getCalEndDay(transportation.StartDay, transportation.Duration);
                transportation.Day = transportation.StartDay;
            } else {
                transportation.StartDay = await this.getCalStartDay(this.lastEndDate, startDate, this.lastEndDay);
                transportation.EndDay = await this.getCalEndDay(transportation.StartDay, transportation.Duration);
                transportation.Day = transportation.StartDay;
            }

            this.lastStartDay = transportation.StartDay;
            this.lastEndDay = transportation.EndDay;
        }

        this.lastStartDate = startDate;
        this.lastEndDate = endDate;

        // name
        if (insertReservationItemRS.Transportation.AttributeGroupList) {
            transportation.Name = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_TRANSFER_TRANSPORTATION, global.constants.ATTRIBUTE_SERVICE_NAME, insertReservationItemRS.Transportation.AttributeGroupList);
        }

        // short descrption
        if (typeof insertReservationItemRS.TransportationUnit.ShortDescription != "undefined") {
            transportation.ShortDescription = await HtmlHelper.getHtmlData(insertReservationItemRS.TransportationUnit.ShortDescription);
        }

        if (insertReservationItemRS.TransportationUnit.AttributeGroupList) {
            if (insertReservationItemRS.TransportationUnit.AttributeGroupList.AttributeGroup.length > 0) {
                transUnitAttributeGroup = insertReservationItemRS.TransportationUnit.AttributeGroupList.AttributeGroup;

                for (const j in transUnitAttributeGroup) {

                    if (transUnitAttributeGroup[j].GroupID == objectTypeID) {
                        transportation.TypeOfService = transUnitAttributeGroup[j].GroupID;
                        transportation.TypeOfServiceName = transUnitAttributeGroup[j].GroupName;

                        if (transUnitAttributeGroup[j].AttributeList.Attribute.length > 0) {
                            transUnitAttribute = transUnitAttributeGroup[j].AttributeList.Attribute;

                            for (const k in transUnitAttribute) {
                                // Departure point 
                                if (transUnitAttribute[k].AttributeID == global.constants.ATTRIBUTE_DEPARTURE_POINT) {
                                    transportation.DeparturePoint = transUnitAttribute[k].AttributeValue;
                                }

                                // Arribal point
                                if (transUnitAttribute[k].AttributeID == global.constants.ATTRIBUTE_ARRIVAL_POINT) {
                                    transportation.ArrivalPoint = transUnitAttribute[k].AttributeValue;
                                }

                                // Departure point
                                if (transUnitAttribute[k].AttributeID == global.constants.ATTRIBUTE_USER_SELECT_DEPARTURE_POINT) {
                                    transportation.UserSelectDeapertureTime = transUnitAttribute[k].AttributeValue;
                                }

                                // Departure times
                                if (transUnitAttribute[k].AttributeID == global.constants.ATTRIBUTE_DEPARTURE_TIMES) {
                                    transportation.DepartureTimes = transUnitAttribute[k].AttributeValue;
                                }

                                // duration
                                if (transUnitAttribute[k].AttributeID == global.constants.ATTRIBUTE_DURATION_HR) {
                                    transportation.DurationHR = transUnitAttribute[k].AttributeValue;
                                }

                                if (transUnitAttribute[k].AttributeID == global.constants.ATTRIBUTE_PICKUP_INCLUDED) {
                                    transportation.PickupIncluded = transUnitAttribute[k].AttributeValue;
                                }

                                if (transUnitAttribute[k].AttributeID == global.constants.ATTRIBUTE_DROPOFF_INCLUDED) {
                                    transportation.DropoffIncluded = transUnitAttribute[k].AttributeValue;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        return transportation;
    }


    /**
     * Retrieve information from simple tour
     * 
     * @param {object} insertReservationItemRS Item of the component
     * @returns {array} Tour description information
     */
    async getSimpleTour(insertReservationItemRS) {
        var simpleTour = await SimpleTourInterface.getSimpleTourInterface();
        var htmlSctructure = await HtmlHelper.getHtmlParser(insertReservationItemRS.PackageTour.ShortDescription);
        var description = await this.SimpleTourLibrary.getShortDescription(htmlSctructure);

        var startDate = moment(insertReservationItemRS.StartDate).format('YYYY-MM-DD');
        var endDate = moment(insertReservationItemRS.EndDate).format('YYYY-MM-DD');

        simpleTour.ID = insertReservationItemRS.PackageTour.ObjectID;
        simpleTour.TourID = insertReservationItemRS.PackageTour.ObjectID;
        simpleTour.TourTitle = insertReservationItemRS.PackageTour.Name;
        simpleTour.Type = global.constants.TOUR_TYPE_SIMPLE_TOUR;
        simpleTour.StartDate = startDate;
        simpleTour.EndDate = endDate;
        simpleTour.Duration = await this.getDuration(startDate, endDate);

        if (this.lastStartDate == null) {

            simpleTour.StartDay = 1;
            simpleTour.EndDay = await this.getCalEndDay(simpleTour.StartDay, simpleTour.Duration);
            simpleTour.Day = simpleTour.StartDay;

            this.lastStartDay = simpleTour.StartDay;
            this.lastEndDay = simpleTour.EndDay;
        } else {

            var f1 = moment(this.lastStartDate).format('YYYY-MM-DD');
            var f2 = moment(startDate).format('YYYY-MM-DD');

            if (f1 == f2) {
                simpleTour.StartDay = this.lastStartDay;
                simpleTour.EndDay = await this.getCalEndDay(simpleTour.StartDay, simpleTour.Duration);
                simpleTour.Day = simpleTour.StartDay;
            } else {
                simpleTour.StartDay = await this.getCalStartDay(this.lastEndDate, startDate, this.lastEndDay);
                simpleTour.EndDay = await this.getCalEndDay(simpleTour.StartDay, simpleTour.Duration);
                simpleTour.Day = simpleTour.StartDay;
            }

            this.lastStartDay = simpleTour.StartDay;
            this.lastEndDay = simpleTour.EndDay;
        }

        this.lastStartDate = startDate;
        this.lastEndDate = endDate;

        simpleTour.Itinerary = description;

        return simpleTour;
    }


    /**
     * Retrieve the short description for accomodation and activity
     * 
     * @param {array} htmlSctructure 
     * @returns {array}
     */
    async getShortDescription(htmlSctructure) {

        var contaHeaders = 0;
        var shortDescription = [];
        var childNodes = [];

        for (const i in htmlSctructure) {
            if (typeof htmlSctructure[i].tagName != 'undefined') {
                if (htmlSctructure[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 1) {
                childNodes = [];
                if (htmlSctructure[i].childNodes.length > 0) {
                    childNodes = htmlSctructure[i].childNodes;
                    for (const j in childNodes) {
                        if (childNodes[j].text != "" && childNodes[j].text != " ") {
                            shortDescription.push(childNodes[j].text.trim());
                        }
                    }
                }
            }

            if (contaHeaders > 1) {
                break;
            }
        }

        return shortDescription;
    }

    /**
     * Retrieve the day of the first item
     * 
     * @param {array} itinerary 
     * @returns {date}
     */
    async getPrimerItem(itinerary) {

        var contaItems = Object.keys(itinerary).length;
        var itemArray = [];
        var fechaItem = null;
        var item = null;
        var conta = 0;

        if (contaItems > 0) {
            for (const i in itinerary) {
                if (itinerary[i]) {
                    itemArray = itinerary[i];
                    break;
                }
            }
        }

        if (Array.isArray(itemArray)) {
            if (itemArray.length > 0) {
                item = itemArray[0];

                if (typeof item.StartDay != "undefined") {
                    fechaItem = item.StartDay;
                }
            }
        }

        return fechaItem;
    }

    async getPrimerFechaItem(itinerary) {

        var contaItems = Object.keys(itinerary).length;
        var itemArray = [];
        var fechaItem = null;
        var item = null;
        var conta = 0;

        if (contaItems > 0) {
            for (const i in itinerary) {
                if (itinerary[i]) {
                    itemArray = itinerary[i];
                    break;
                }
            }
        }

        if (Array.isArray(itemArray)) {
            if (itemArray.length > 0) {
                item = itemArray[0];

                if (typeof item.StartDate != "undefined") {
                    fechaItem = item.StartDate;
                }
            }
        }

        return fechaItem;
    }

    /**
     * Retrieve the last day of the last item
     * 
     * @param {array}
     * @returns {int}
     */
    async getUltimoItem(itinerary) {
        var contaItems = Object.keys(itinerary).length;
        var item = [];
        var itemArray = [];
        var totalArray = 0;
        var fechaItem = null;

        if (contaItems >= 1) {
            for (const i in itinerary) {
                if (itinerary[i]) {
                    itemArray = itinerary[i];
                }
            }
        }

        if (Array.isArray(itemArray)) {
            totalArray = itemArray.length;
            if (totalArray > 0) {
                item = itemArray[totalArray - 1];
            }
        }

        if (item) {
            if (typeof item.EndDay != "undefined") {
                fechaItem = item.EndDay;
            }
        }

        return fechaItem;
    }

    async getUltimoFechaItem(itinerary) {
        var contaItems = Object.keys(itinerary).length;
        var item = [];
        var itemArray = [];
        var totalArray = 0;
        var fechaItem = null;

        if (contaItems >= 1) {
            for (const i in itinerary) {
                if (itinerary[i]) {
                    itemArray = itinerary[i];
                }
            }
        }

        if (Array.isArray(itemArray)) {
            totalArray = itemArray.length;
            if (totalArray > 0) {
                item = itemArray[totalArray - 1];
            }
        }

        if (item) {
            if (typeof item.EndDate != "undefined") {
                fechaItem = item.EndDate;
            }
        }

        return fechaItem;
    }

    /**
     * Method that calculates the duration between two dates
     * 
     * @param {date} startDate 
     * @param {date} endDate 
     * @returns {int}
     */
    async getDuration(startDate, endDate) {
        var duration = await DateHelper.daysDiff(startDate, endDate) + 1;
        return duration;
    }

    /**
     * 
     * @param {*} endDate 
     * @param {*} startDate 
     * @param {*} lastStartDay 
     */
    async getCalStartDay(endDate, startDate, lastEndDay) {
        var daysDiff = await DateHelper.daysDiff(endDate, startDate);
        var calSartDate = parseInt(daysDiff) + parseInt(lastEndDay);

        return calSartDate;
    }

    /**
     * Method that calculates the final date of a component
     * 
     * @param {int} day 
     * @param {int} duration 
     * @returns {int}
     */
    async getCalEndDay(day, duration) {
        var endDate = (day + duration - 1);
        return endDate;
    }

    /**
     * Retrieve the initial city from the list of attributes
     * 
     * @override
     * @param {int} type Tour Type
     * @param {array} attributeGroup List of destinations
     * @returns {string} Initial destination information
     */
    async getStartDestination(type, attributeGroup) {
        var startCity = null;
        if (Array.isArray(attributeGroup)) {
            if (attributeGroup.length > 0) {
                for (const i in attributeGroup) {
                    if (attributeGroup[i].GroupID == type) {
                        if (attributeGroup[i].AttributeList) {
                            if (typeof attributeGroup[i].AttributeList.Attribute != "undefined") {
                                if (Array.isArray(attributeGroup[i].AttributeList.Attribute)) {
                                    if (attributeGroup[i].AttributeList.Attribute.length > 0) {
                                        for (const j in attributeGroup[i].AttributeList.Attribute) {
                                            if (attributeGroup[i].AttributeList.Attribute[j].AttributeID == global.constants.ATTRIBUTE_START_CITY_COUNTRY) {
                                                startCity = attributeGroup[i].AttributeList.Attribute[j].AttributeValue;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return startCity;
    }


    /**
     * Validate if a subcategory exists within the array, 
     * if it exists the counter increases otherwise it adds the category to the array
     * 
     * @param {int} itemID 
     * @param {string} itemName 
     * @param {array} subcategories 
     */
    async existeSubCategoria(itemID, itemName, subcategories) {

        var existe = false;

        if (!subcategories) {
            subcategories = [];
        }

        if (Array.isArray(subcategories)) {
            if (subcategories.length > 0) {
                for (const i in subcategories) {
                    existe = false;
                    if (subcategories[i].ID == itemID) {
                        subcategories[i].Number++;
                        existe = true;
                    }
                }
            }
        }

        if (existe == false) {
            subcategories.push({
                ID: itemID,
                Number: 1,
                Name: itemName
            });
        }

        return subcategories;
    }

    /**
     * 
     * @param {int} destinationID 
     * @param {string} destinationName 
     * @param {int} mealsID 
     * @param {string} mealsName 
     * @param {array} lista 
     */
    async existeMealsListado(destinationID, destinationName, mealsID, mealsName, lista) {
        var existe = false;

        if (!lista) {
            lista = [];
        }

        if (Array.isArray(lista)) {
            if (lista.length > 0) {
                for (const i in lista) {
                    if (lista[i].DestinationID == destinationID && lista[i].MealsID == mealsID) {
                        existe = true;
                        lista[i].Number++;
                    }
                }
            }
        }

        if (existe == false) {
            lista.push({
                DestinationID: destinationID,
                DestinationName: destinationName,
                Number: 1,
                MealsID: mealsID,
                MealsName: mealsName
            });
        }

        return lista;
    }

    /**
     * 
     * @param {int} destinationID 
     * @param {string} destinationName 
     * @param {date} startDate 
     * @param {date} endDate 
     * @param {array} lista 
     */
    async existeAccomodationList(destinationID, destinationName, startDate, endDate, lista) {

        if (!lista) {
            lista = [];
        }

        lista.push({
            DestinationID: destinationID,
            DestinationName: destinationName,
            Number: await DateHelper.daysDiff(startDate, endDate)
        });

        return lista;
    }

    /**
     * Retrieve the text that will go in the subtitle part
     * 
     * @param {array} subtitles 
     * @returns {string}
     */
    async getSubtitle(subtitles, subst) {

        var subtitle = '';

        if (subtitles.length > 0) {
            for (const i in subtitles) {
                if (subst) {
                    subtitle += subtitles[i].Number + ' ' + (subtitles[i].Name.substring(subst)) + ',';
                } else {
                    subtitle += subtitles[i].Number + ' ' + subtitles[i].Name + ',';
                }
            }
        }

        subtitle = subtitle.substr(0, subtitle.length - 1);

        return subtitle;
    }

    /**
     * 
     * @param {array} list
     * @returns {array} 
     */
    async getListMeals(list) {
        var listMeals = [];

        if (Array.isArray(list)) {
            if (list.length > 0) {
                for (const i in list) {
                    listMeals.push(list[i].Number + ' ' + (list[i].DestinationName) + ' ' + list[i].MealsName);
                }
            }
        }

        return listMeals;
    }

    /**
     * 
     * @param {array} list 
     * @returns {array}
     */
    async getListAccomodation(list) {
        var listAccomodation = [];

        if (Array.isArray(list)) {
            if (list.length > 0) {
                for (const i in list) {
                    listAccomodation.push(list[i].Number + ' ' + list[i].DestinationName);
                }
            }
        }

        return listAccomodation;
    }

    /**
     * 
     * @param {*} tourID 
     */
    async getParentStars(tourID, attributeGroupList) {
        var listStars = [];

        if (attributeGroupList) {

            var value = await this.AttributeGroupLibrary.getAttribute(global.constants.TOUR_TYPE_FIT_TOUR, global.constants.ATTRIBUTE_ACCOMODATION_TYPE, attributeGroupList);

            if (value) {
                listStars.push(value);
            }
        }


        var listChildrens = await this.ChildrenToursBamba.getParentStars(tourID);

        for(const i in listChildrens) {
            listStars.push(listChildrens[i]);
        }

        return listStars;
    }

    /**
     * 
     * @param {*} components 
     */
    async ordenarComponentes(components) {
        components.sort(function (a, b) {
            var c = new Date(a.StartDate);
            var d = new Date(b.StartDate);

            return c - d;
        });

        return components;
    }


    async quitarNumerosMeals(meals) {
        var strMeals = '';
        var reg = /^[a-zA-Z]*$/;

        if (!meals.match(reg)) {
            strMeals = meals.substring(3, meals.length);
        } else {
            strMeals = meals;
        }

        return strMeals;
    }

    /**
     * 
     * @param {*} atributeGroup 
     */
    async getAirportPickupIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_FIT_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_AIRPORT_PICKUP_INCLUDED, atributeGroupList);

        return value;
    }


    async getStartDestinationID(StartDestination) {
        return super.getEndDestinationID(StartDestination);
    }

    async getGallery(Gallery, components) {
        var photos = [];

        if (Array.isArray(components)) {
            if (components.length > 0) {
                for (const i in components) {
                    photos = [];
                    if (typeof components[i].AccommodationUnit != "undefined") {
                        if (components[i].AccommodationUnit.Type.UnitTypeID == global.constants.ACCOMODATION_TYPE_ACTIVITY_ID) {
                            photos = await super.getGallery(components[i].AccommodationObject.PhotoList);
                        }
                    }

                    if (typeof components[i].PackageTour != "undefined") {
                        photos = await super.getGallery(components[i].PackageTour.PhotoList);
                    }

                    if (Array.isArray(photos)) {
                        if (photos.length > 0) {
                            for (const j in photos) {
                                Gallery.push(photos[j]);
                            }
                        }
                    }
                }
            }
        }

        return Gallery;
    }


    async validarFechasLibres(itinerary) {
        var itineraryNuevo = [];
        var itineraryPaisItem = [];
        var itineraryDestino = [];
        var itineraryDestinoItem = {};
        var fakeItem = null;
        var fechaPrimerItem = null;
        var fechaUltimoItem = null;
        var diasDiff = 0;
        var DayPrimerItem = null;
        var DayUltimoItem = null;
        var Duration = null;
        var StartDateFake = null;

        this.lastEndDate = null;

        if (itinerary.Itinerary.length > 0) {
            // paises
            for (const i in itinerary.Itinerary) {

                itineraryPaisItem = itinerary.Itinerary[i];

                if (typeof itinerary.Itinerary[i].Itinerary != "undefined") {

                    if (Array.isArray(itinerary.Itinerary[i].Itinerary)) {
                        if (itinerary.Itinerary[i].Itinerary.length > 0) {

                            itineraryDestino = [];
                            itineraryDestinoItem = null;

                            // recorremos destinos
                            for (const j in itinerary.Itinerary[i].Itinerary) {
                                itineraryDestinoItem = itinerary.Itinerary[i].Itinerary[j];

                                // aqui hacer validación
                                if (itinerary.Itinerary[i].Itinerary[j].Itinerary[0]) {
                                    fechaPrimerItem = await this.getPrimerFechaItem(itinerary.Itinerary[i].Itinerary[j].Itinerary[0]);
                                    fechaUltimoItem = await this.getUltimoFechaItem(itinerary.Itinerary[i].Itinerary[j].Itinerary[0]);

                                    DayPrimerItem = await this.getPrimerItem(itinerary.Itinerary[i].Itinerary[j].Itinerary[0]);
                                    DayUltimoItem = await this.getUltimoItem(itinerary.Itinerary[i].Itinerary[j].Itinerary[0]);

                                    if (this.lastEndDate) {
                                        StartDateFake = await DateHelper.addDate(this.lastEndDate, 1);
                                        diasDiff = await this.validarFechaLibre(StartDateFake, fechaPrimerItem);

                                        if (diasDiff >= 1) {
                                            fakeItem = await ItineraryInterface.getLevelDestinationInterface();
                                            Duration = await this.getDuration(StartDateFake, fechaPrimerItem);

                                            fakeItem.StartDay = this.lastEndDay + 1;
                                            fakeItem.EndDay = await this.getCalEndDay(fakeItem.StartDay, Duration);

                                            itineraryDestino.push(fakeItem);
                                        }
                                    }

                                    this.lastEndDate = fechaUltimoItem;
                                    this.lastEndDay = DayUltimoItem;
                                }

                                itineraryDestino.push(itineraryDestinoItem);
                            }

                            itineraryPaisItem.Itinerary = itineraryDestino;
                        }
                    }

                    itineraryNuevo.push(itineraryPaisItem);
                }
            }
        }

        itinerary.Itinerary = itineraryNuevo;

        return itinerary;
    }

    async validarFechaLibre(lastEndDate, StartDate) {

        var dias = await DateHelper.daysDiff(lastEndDate, StartDate);
        return dias;
    }

    async getAccomodationPickupIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_FIT_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_PICKUP_INCLUDED, atributeGroupList);

        return value;
    }

    async getAccomodationDropOffIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_FIT_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_DROPOFF_INCLUDED, atributeGroupList);

        return value;
    }

    /**
     * Retrieve the information of the units and the additionalservices
     * 
     * @param {int} TourID 
     * @param {int} Type 
     * @param {array} AttributeGroupList 
     * @param {array} PackageUnit 
     * @param {boolean} AdditionalServiceList 
     * @returns {array}
     */
    async getAccomodationList(TourID, AttributeGroupList, PackageUnit, AdditionalServiceList) {

        console.log("----------------------  Accomodation list");
        console.log("TourID", TourID);
        console.log("AttributeGroupList", AttributeGroupList);
        console.log("AdditionalServiceList", AdditionalServiceList);
        console.log("----------------------  Fin Accomodation list");

        var AccommodationObjectList = [];
        var itemAccomodationObject = await CheckoutInterface.getAccomodationItemInterface();

        if (PackageUnit) {
            if (Array.isArray(PackageUnit)) {
                if (PackageUnit.length > 0) {
                    itemAccomodationObject.UnitID = PackageUnit[0].UnitID;

                    if (PackageUnit[0].ServiceList) {
                        if (Array.isArray(PackageUnit[0].ServiceList.Service)) {
                            if (PackageUnit[0].ServiceList.Service.length > 0) {
                                itemAccomodationObject.ServiceID = PackageUnit[0].ServiceList.Service[0].ServiceID;
                            }
                        }
                    }
                }
            }
        }

        // recuperamos la posicion 0 del accomodationobjectlist
        itemAccomodationObject.UnitName = await this.AttributeGroupLibrary.getAttribute(global.constants.TOUR_TYPE_FIT_TOUR, AttributeGroupList, global.constants.ATTRIBUTE_ACCOMODATION_TYPE);
        var AccomodationObjectFirstTour = await this.getAccomodationByComponents(itemAccomodationObject.UnitID, itemAccomodationObject.ServiceID, AdditionalServiceList);

        if(Array.isArray(AccomodationObjectFirstTour)) {
            if(AccomodationObjectFirstTour.length > 0) {
                for(const i in AccomodationObjectFirstTour) {
                    AccommodationObjectList.push(AccomodationObjectFirstTour[i]);
                }
            }
        }

        var AccomodationObjectChildrens = await this.getChildrensAccomodation(TourID, AdditionalServiceList);

        if(Array.isArray(AccomodationObjectChildrens)) {
            if(AccomodationObjectChildrens.length > 0) {
                for(const i in AccomodationObjectChildrens) {
                    AccommodationObjectList.push(AccomodationObjectChildrens[i]);
                }
            }
        }

        return AccommodationObjectList;
    }

    /**
     * Retrieve the information of the units and additional services of the children's tours
     * 
     * @param {int} ParentID 
     * @param {boolean} AdditionalServiceList 
     * @returns {array}
     */
    async getChildrensAccomodation(ParentID, AdditionalServiceList) {

        var ChildrensTours = await this.ChildrenToursBamba.getChildrenTours({
            ParentIDFilter:ParentID,
            LanguageID: await LanguageHelper.getLang(),
            PageSize:100,
            CurrentPage: 1
        });

        var UnitID = null;
        var ServiceID = null;
        var AccomodationObjectChildrens = [];
        var AccomodationObjectList = [];

        if(Array.isArray(ChildrensTours)) {
            if(ChildrensTours.length > 0) {

                for(const i in ChildrensTours) {
                    UnitID= null;
                    ServiceID = null;

                    if (ChildrensTours[i].PackageUnit) {
                        if (Array.isArray(PackageUnit)) {
                            if (PackageUnit.length > 0) {
                                UnitID = PackageUnit[0].UnitID;
            
                                if (PackageUnit[0].ServiceList) {
                                    if (Array.isArray(PackageUnit[0].ServiceList.Service)) {
                                        if (PackageUnit[0].ServiceList.Service.length > 0) {
                                            ServiceID = PackageUnit[0].ServiceList.Service[0].ServiceID;
                                            AccomodationObjectChildrens = await this.getAccomodationByComponents(UnitID, ServiceID, AdditionalServiceList);

                                            if(Array.isArray(AccomodationObjectChildrens)) {
                                                if(AccomodationObjectChildrens.length > 0) {
                                                    for (const j in AccomodationObjectChildrens) {
                                                        AccomodationObjectList.push(AccomodationObjectChildrens[j]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return AccomodationObjectList;
    }

    /**
     * Retrieve the information of the units and additionalservices for each simple component tour and activity
     * 
     * @param {int} UnitID 
     * @param {int} ServiceID 
     * @param {boolean} AdditionalServiceList 
     * @returns {array}
     */
    async getAccomodationByComponents(UnitID, ServiceID, AdditionalServiceList) {
        var AccommodationObjectList = [];

        const components = await this.InsertReservationBamba.getComponents(UnitID, ServiceID, global.constants.DEFAULT_CURRENCY_ID, global.constants.DEFAULT_PAYMENT_METHOD_ID, await LanguageHelper.getLang());

        if (Array.isArray(components)) {
            if (components.length > 0) {
                for (const i in components) {
                    if (typeof components[i].PackageTour != "undefined") {
                        if (components[i].PackageUnit) {
                            var packageUnitSimpleTour = []
                            if(Array.isArray(components[i].PackageUnit)) {
                                packageUnitSimpleTour = components[i].PackageUnit;
                            } else {
                                packageUnitSimpleTour[0] = components[i].PackageUnit;
                            }

                            if (packageUnitSimpleTour.length > 0) {
                                var AccommodationObjectListSimpleTour = await this.SimpleTourLibrary.getAccomodationList(components[i].PackageTour.Name,packageUnitSimpleTour, AdditionalServiceList);
                                AccommodationObjectListSimpleTour= await this.PriceForServiceListLibrary.getPriceForServiceList(AccommodationObjectListSimpleTour, await DateHelper.now(), await DateHelper.now(), global.constants.DEFAULT_CURRENCY_ID, global.constants.DEFAULT_PAYMENT_METHOD_ID, await LanguageHelper.getLang());

                                if (Array.isArray(AccommodationObjectListSimpleTour)) {
                                    if (AccommodationObjectListSimpleTour.length > 0) {
                                        for (const i in AccommodationObjectListSimpleTour) {
                                            AccommodationObjectList.push(AccommodationObjectListSimpleTour[i]);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (typeof components[i].AccommodationUnit != "undefined") {
                        if (components[i].AccommodationUnit.Type.UnitTypeID == global.constants.ACCOMODATION_TYPE_ACTIVITY_ID) {
                            if (components[i].AccommodationObject) {
                                var AccommodationObjectListActivityTour = await this.ActivitiesTourLibrary.getAccomodationList(components[i].AccommodationObject.Name,components[i].AccommodationObject.UnitList.AccommodationUnit, AdditionalServiceList);
                                AccommodationObjectListActivityTour= await this.PriceForServiceListLibrary.getPriceForServiceList(AccommodationObjectListActivityTour, await DateHelper.now(), await DateHelper.now(), global.constants.DEFAULT_CURRENCY_ID, global.constants.DEFAULT_PAYMENT_METHOD_ID, await LanguageHelper.getLang());
                                if (Array.isArray(AccommodationObjectListActivityTour)) {
                                    if (AccommodationObjectListActivityTour.length > 0) {
                                        for (const i in AccommodationObjectListActivityTour) {
                                            AccommodationObjectList.push(AccommodationObjectListActivityTour[i]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return AccommodationObjectList;
    }
}