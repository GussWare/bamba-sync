import CheckoutInterface from '../interface/checkoutInterface';
import GetPackageDetailedDescription from '../services/lemax/GetPackageDetailedDescription';
import SimpleTourLibrary from '../libraries/SimpleTourLibrary';

export default class CheckOutSimpleTourLibrary {

    constructor() {
        this.SimpleTourLibrary = new SimpleTourLibrary();
    }

    /**
     * Generates an object for the reception section of the tab one of the simple tour
     * 
     * @param {object} params 
     * @returns {object}
     */
    async getCheckOut(params) {

        var tour = await CheckoutInterface.getCheckoutInterface();
        var packageDetailed = new GetPackageDetailedDescription();

        if (params.TourID) {
            packageDetailed.PackageTourID = params.TourID;
        }

        if (params.PackageTourCode) {
            packageDetailed.PackageTourCode = params.PackageTourCode;
        }

        if (params.LanguageID) {
            packageDetailed.LanguageID = params.LanguageID;
        } else {
            packageDetailed.LanguageID = await LanguageHelper.getLang();
        }

        if (params.CurrencyID) {
            packageDetailed.CurrencyID = params.CurrencyID;
        } else {
            packageDetailed.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (params.InPriceType) {
            packageDetailed.InPriceType = params.InPriceType;
        } else {
            packageDetailed.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await packageDetailed.GetPackageDetailedDescription();
        var GetPackageDetailedDescriptionResult = response.GetPackageDetailedDescriptionResult;

        if (GetPackageDetailedDescriptionResult.Status.Code == 'OK') {

            if (typeof GetPackageDetailedDescriptionResult.PackageTour != "undefined") {
                if (GetPackageDetailedDescriptionResult.PackageTour) {

                    var tourResult = GetPackageDetailedDescriptionResult.PackageTour;

                    tour.TourID = tourResult.ObjectID;
                    tour.Type = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeID : null;
                    tour.TypeName = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeName : null;
                    tour.TourTitle = tourResult.Name;

                    tour.Countries = (GetPackageDetailedDescriptionResult.CountryList) ? GetPackageDetailedDescriptionResult.CountryList.Country : [];
                    tour.NumberOfCountries = tour.Countries.length || 0;

                    tour.Style = await this.SimpleTourLibrary.getStyle(tourResult.CategoryList);
                    tour.DurationDD = await this.SimpleTourLibrary.getDurationDD(tourResult.AttributeGroupList);

                    tour.AccommodationPickupIncluded = await this.SimpleTourLibrary.getAccomodationPickupIncluded(tourResult.AttributeGroupList);
                    tour.AccommodationDropOffIncluded = await this.SimpleTourLibrary.getAccomodationDropOffIncluded(tourResult.AttributeGroupList);

                    if (tourResult.PackageUnitList) {
                        if (Array.isArray(tourResult.PackageUnitList.PackageUnit)) {
                            if (tourResult.PackageUnitList.PackageUnit.length > 0) {
                                tour.PriceInfo = await this.SimpleTourLibrary.getPriceInfo(tour.Type, tourResult.PackageUnitList.PackageUnit[0], tourResult.NoteList, null);
                            }
                        }
                    }

                    tour.UnitID = null;
                    tour.ServiceID = null;
                    tour.AccommodationObjectList = [];

                    if (tourResult.PackageUnitList) {
                        if (Array.isArray(tourResult.PackageUnitList.PackageUnit)) {
                            if (tourResult.PackageUnitList.PackageUnit.length > 0) {
                                tour.UnitID = await this.SimpleTourLibrary.getUnitIDBasic(tourResult.PackageUnitList.PackageUnit);
                                tour.ServiceID = await this.SimpleTourLibrary.getServiceIDBasic(tourResult.PackageUnitList.PackageUnit);

                                tour.AccommodationObjectList = await this.SimpleTourLibrary.getAccomodationList(tourResult.Name, tourResult.PackageUnitList.PackageUnit, false);
                            }
                        }
                    }
                }
            }
        }

        return tour;
    }
}