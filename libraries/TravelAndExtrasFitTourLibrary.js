import FitTourLibrary from '../libraries/FitTourLibrary';
import TravelAndExtrasInterface from '../interface/travelAndExtrasInterface';
import GetPackageDetailedDescription from '../services/lemax/GetPackageDetailedDescription';
import PriceForServiceListLibrary from '../libraries/PriceForServiceListLibrary';
import AttributeGroupLibrary from '../libraries/AttributeGroupLibrary';
import LanguageHelper from '../helpers/LanguageHelper';


export default class TravelAndExtrasFitTourLibrary {

    constructor() {
        this.FitTourLibrary = new FitTourLibrary();
        this.PriceForServiceListLibrary = new PriceForServiceListLibrary();
        this.AttributeGroupLibrary = new AttributeGroupLibrary();
    }

    /**
     * Generate an object with the necessary information to fill the information of the travels and extras tab for when it is a fit tour
     * 
     * @param {object} params 
     * @returns {object}
     */
    async getTravelAndExtras(params) {
        var tour = await TravelAndExtrasInterface.getTravelAndExtrasInterface();
        var packageDetailed = new GetPackageDetailedDescription();

        if (params.TourID) {
            packageDetailed.PackageTourID = params.TourID;
        }

        if (params.PackageTourCode) {
            packageDetailed.PackageTourCode = params.PackageTourCode;
        }

        if (params.LanguageID) {
            packageDetailed.LanguageID = params.LanguageID;
        } else {
            packageDetailed.LanguageID = await LanguageHelper.getLang();
        }

        if (params.CurrencyID) {
            packageDetailed.CurrencyID = params.CurrencyID;
        } else {
            packageDetailed.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (params.InPriceType) {
            packageDetailed.InPriceType = params.InPriceType;
        } else {
            packageDetailed.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await packageDetailed.GetPackageDetailedDescription();
        var GetPackageDetailedDescriptionResult = response.GetPackageDetailedDescriptionResult;

        if (GetPackageDetailedDescriptionResult.Status.Code == 'OK') {
            if (typeof GetPackageDetailedDescriptionResult.PackageTour != "undefined") {
                if (GetPackageDetailedDescriptionResult.PackageTour) {

                    var tourResult = GetPackageDetailedDescriptionResult.PackageTour;
                    var Type = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeID : null;
                    var TourID = tourResult.ObjectID;
                    var UnitID = null;
                    var ServiceID = null;

                    tour.AccommodationObjectList = [];

                    if (tourResult.PackageUnitList) {
                        if (Array.isArray(tourResult.PackageUnitList.PackageUnit)) {
                            if (tourResult.PackageUnitList.PackageUnit.length > 0) {
                                UnitID = tourResult.PackageUnitList.PackageUnit[0].UnitID;

                                if (tourResult.PackageUnitList.PackageUnit[0].ServiceList) {
                                    if (Array.isArray(tourResult.PackageUnitList.PackageUnit[0].ServiceList.Service)) {
                                        if (tourResult.PackageUnitList.PackageUnit[0].ServiceList.Service.length > 0) {
                                            ServiceID = tourResult.PackageUnitList.PackageUnit[0].ServiceList.Service[0].ServiceID;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    tour.StartDestination = await this.FitTourLibrary.getStartDestination(Type, tourResult.AttributeGroupList.AttributeGroup);
                    tour.EndDestination = await this.FitTourLibrary.getEndDestination(Type, tourResult.AttributeGroupList.AttributeGroup);

                    tour.StartDestinationID = await this.FitTourLibrary.getStartDestinationID(tour.StartDestination);
                    tour.EndDestinationID = await this.FitTourLibrary.getEndDestinationID(tour.EndDestination);

                    if (tourResult.PackageUnitList) {
                        tour.AccommodationObjectList = await this.FitTourLibrary.getAccomodationList(TourID, tourResult.AttributeGroupList.AttributeGroup, tourResult.PackageUnitList.PackageUnit, true);
                    }

                    if (tourResult.AttributeGroupList) {
                        if (Array.isArray(tourResult.AttributeGroupList.AttributeGroup)) {
                            tour.AirportPickup = await this.FitTourLibrary.getAirportPickupIncluded(tourResult.AttributeGroupList);
                        }
                    }
                }
            }
        }

        return tour;
    }
}