export default class FilterPriceShopLibrary {

    constructor() {

    }

    /**
     * Method that is responsible for filtering according to the initial and final price granted by the user in the search
     * 
     * @param {float} PriceFrom 
     * @param {float} PriceTo 
     * @param {array} PackageList 
     * @returns {array}
     */    
    async getFilterPrice(PriceFrom, PriceTo, PackageList) {

        var tourList = [];

        if(Array.isArray(PackageList)) {
            if(PackageList.length > 0) {
                for(const i in PackageList) {
                    if(typeof PackageList[i].PriceInfo != "undefined") {
                        if(PackageList[i].PriceInfo) {
                            if(parseFloat(PackageList[i].PriceInfo.Price) >= parseFloat(PriceFrom) && parseFloat(PackageList[i].PriceInfo.Price) <= parseFloat(PriceTo)) {
                                tourList.push(PackageList[i]);
                            }
                        }
                    }
                }
            }
        }

        return tourList;
    }
}