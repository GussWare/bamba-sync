import TourLibrary from './TourLibrary';
import CuponsAndDiscountInterface from '../interface/cuponsAndDiscountInterface';
import GetPackageDetailedDescription from '../services/lemax/GetPackageDetailedDescription';
import moment from 'moment';

export default class CouponsAndDiscountsActivityLibrary {

    constructor() {
        this.TourLibrary = new TourLibrary();
    }

    async getCuponAndDiscount(UnitID, params) {
        var tour = await CuponsAndDiscountInterface.getTourInterface();
        var packageDetailed = new GetPackageDetailedDescription();

        if (params.TourID) {
            packageDetailed.PackageTourID = params.TourID;
        }

        if (params.LanguageID) {
            packageDetailed.LanguageID = params.LanguageID;
        }

        if (params.CurrencyID) {
            packageDetailed.CurrencyID = params.CurrencyID;
        }

        if (params.InPriceType) {
            packageDetailed.InPriceType = params.InPriceType;
        }

        if (params.InPriceType) {
            packageDetailed.InPriceType = params.InPriceType;
        } else {
            packageDetailed.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await packageDetailed.GetPackageDetailedDescription();
        var GetPackageDetailedDescriptionResult = response.GetPackageDetailedDescriptionResult;

        if (GetPackageDetailedDescriptionResult.Status.Code == 'OK') {
            if (typeof GetPackageDetailedDescriptionResult.PackageTour != "undefined") {

                if (GetPackageDetailedDescriptionResult.PackageTour) {
                    var tourResult = GetPackageDetailedDescriptionResult.PackageTour;
                    var type = tourResult.Type = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeID : null;

                    var noteDiscount = await this.TourLibrary.getNoteDiscount(tourResult.NoteList);
                    var noteCoupon = await this.TourLibrary.getNoteCupon(tourResult.NoteList);
                    var specialOffer = null;
                    var packageUnit = await this.TourLibrary.getPackageUnitByID(UnitID, tourResult.PackageUnitList.PackageUnit);

                    if (tourResult.PackageUnitList) {
                        specialOffer = await this.TourLibrary.getSpecialOffer(type, tourResult.PackageUnitList.PackageUnit[0]);
                    }

                    if (noteDiscount) {
                        tour.noteDiscount = noteDiscount;
                    }

                    if (noteCoupon) {
                        tour.noteCoupon = noteCoupon;
                    }

                    if (specialOffer) {
                        tour.specialOffer = specialOffer;
                    }

                    if (packageUnit) {
                        tour.availabilityStatus = packageUnit.AvailabilityStatus;
                    }
                }
            }
        }

        return tour;
    }
}