import ActivitiesTourLibrary from '../libraries/ActivitiesTourLibrary';
import TravelAndExtrasInterface from '../interface/travelAndExtrasInterface';
import GetSearchResults from '../services/lemax/GetSearchResults';
import PriceForServiceListLibrary from '../libraries/PriceForServiceListLibrary';

export default class TravelAndExtrasActivityTourLibrary {

    constructor() {
        this.ActivitiesTourLibrary = new ActivitiesTourLibrary();
        this.PriceForServiceListLibrary = new PriceForServiceListLibrary();
    }

    /**
     * Generate an object with the necessary information to fill the information of the travels and extras tab for when it is a activity tour
     * 
     * @param {object} params 
     * @returns {object}
     */
    async getTravelAndExtras(params) {
        var tour = await TravelAndExtrasInterface.getTravelAndExtrasInterface();
        var searchResults = new GetSearchResults();

        searchResults.ShowFileds = [
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 4
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 4
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 100
            }
        ];
        
        searchResults.LanguageID = params.LanguageID;
        searchResults.FilterType = [global.constants.FILTER_BY_ACTIVITIES];

        if (typeof params.TourID != "undefined") {
            searchResults.ObjectIDList = [params.TourID];
        }

        if (typeof params.InPriceType != "undefined") {
            searchResults.InPriceType = params.InPriceType;
        } else {
            searchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await searchResults.GetSearchResults();
        if (typeof response.GetSearchResultsResult != "undefined") {
            if (response.GetSearchResultsResult.Status.Code == 'OK') {

                if (response.GetSearchResultsResult) {
                    if (response.GetSearchResultsResult.AccommodationObjectList) {

                        var tourResult = response.GetSearchResultsResult.AccommodationObjectList.AccommodationObject[0];
                        var UnitID = null;
                        var ServiceID = null;

                        tour.StartDestination = await this.ActivitiesTourLibrary.getStartDestination(response.GetSearchResultsResult.DestinationList);
                        tour.EndDestination = await this.ActivitiesTourLibrary.getEndDestination(tourResult.ObjectType.ObjectTypeID, tourResult.AttributeGroupList.AttributeGroup);
                        tour.StartDestinationID = await this.ActivitiesTourLibrary.getStartDestinationID(response.GetSearchResultsResult.DestinationList.Destination);
                        tour.EndDestinationID = await this.ActivitiesTourLibrary.getEndDestinationID(tour.EndDestination);
                        tour.AirportPickup = await this.ActivitiesTourLibrary.getAirportPickupIncluded(tourResult.AttributeGroupList);
                        tour.AccommodationObjectList = null;

                        if (tourResult.UnitList) {
                            if (Array.isArray(tourResult.UnitList.AccommodationUnit)) {
                                if (tourResult.UnitList.AccommodationUnit.length > 0) {
                                    UnitID = tourResult.UnitList.AccommodationUnit[0].UnitID;

                                    if (tourResult.UnitList.AccommodationUnit[0].ServiceList) {
                                        if (Array.isArray(tourResult.UnitList.AccommodationUnit[0].ServiceList.Service)) {
                                            if (tourResult.UnitList.AccommodationUnit[0].ServiceList.Service.length > 0) {
                                                ServiceID = tourResult.UnitList.AccommodationUnit[0].ServiceList.Service[0].ServiceID;
                                            }
                                        }
                                    }
                                }

                                tour.AccommodationObjectList = await this.ActivitiesTourLibrary.getAccomodationList(tourResult.Name, tourResult.UnitList.AccommodationUnit, true);
                                tour.AccommodationObjectList = await this.PriceForServiceListLibrary.getPriceForServiceList(tour.AccommodationObjectList, params.StartDate, params.EndDate, params.CurrencyID, global.constants.DEFAULT_PAYMENT_METHOD_ID, params.LanguageID);
                            }
                        }
                    }
                }
            }
        }

        return tour;
    }
}