import InsertReservationBamba from '../services/bamba/InsertReservationBamba';

export default class PriceForServiceListLibrary {

    constructor() {

    }

    /**
     * 
     */
    async getPriceForServiceList(AccommodationObjectList, StartDate, EndDate, CurrencyID, PAYMENT_METHOD_ID, LanguageID) {
        var AdditionalServicesList = [];

        if (AccommodationObjectList) {
            if (AccommodationObjectList.length > 0) {
                const insertReservationBamba = new InsertReservationBamba();

                for (const i in AccommodationObjectList) {
                    if (Array.isArray(AccommodationObjectList[i].AdditionalServicesList)) {

                        if (AccommodationObjectList[i].AdditionalServicesList.length > 0) {
                            AdditionalServicesList = await insertReservationBamba.getPriceForServiceList(AccommodationObjectList[i].UnitID, await this.getPrepareServiceList(AccommodationObjectList[i].AdditionalServicesList), StartDate, EndDate, CurrencyID, PAYMENT_METHOD_ID, LanguageID);

                            for (const j in AdditionalServicesList) {
                                for (const k in AccommodationObjectList[i].AdditionalServicesList) {
                                    if (AccommodationObjectList[i].AdditionalServicesList[k].ServiceID == AdditionalServicesList[j].ServiceID) {
                                        AccommodationObjectList[i].AdditionalServicesList[k].Price = AdditionalServicesList[j].Price;
                                        AccommodationObjectList[i].AdditionalServicesList[k].PriceFormated = AdditionalServicesList[j].PriceFormated;
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        return AccommodationObjectList;
    }

    async getPrepareServiceList(AdditionalServicesList) {
        var ServicesList = [];

        if (Array.isArray(AdditionalServicesList)) {
            if (AdditionalServicesList.length > 0) {
                for (const i in AdditionalServicesList) {
                    if (ServicesList.indexOf(AdditionalServicesList[i].ServiceID) <= -1) {
                        ServicesList.push(AdditionalServicesList[i].ServiceID);
                    }
                }
            }
        }

        return ServicesList;
    }
}