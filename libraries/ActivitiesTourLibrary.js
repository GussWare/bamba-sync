import TourLibrary from './TourLibrary';
import TourInterface from '../interface/tourInterface';
import ItineraryInterface from '../interface/itinerary';
import CheckoutInterface from '../interface/checkoutInterface';
import HtmlHelper from '../helpers/HtmlHelper';
import ReservationCanNotStartDatesBamba from '../services/bamba/ReservationCanNotStartDatesBamba';
import LanguageHelper from '../helpers/LanguageHelper';
import DestinationHelper from '../helpers/DestinationHelper';
import DateHelper from '../helpers/DateHelper';
import PriceForServiceListLibrary from '../libraries/PriceForServiceListLibrary';

var HTMLParser = require('node-html-parser');

export default class ActivitiesTourLibrary extends TourLibrary {

    constructor() {
        super();

        this.ReservationCanNotStartDatesBamba = new ReservationCanNotStartDatesBamba();
        this.PriceForServiceListLibrary = new PriceForServiceListLibrary();
    }

    /**
     * Retrieve activity-type tour information 
     * 
     * @param {*} PackageResult 
     */
    async getPackage(PackageResult) {

        var tour = null;
        var htmlShort = [];
        var htmlDesc = [];
        var tourData = await TourInterface.getTourInterface();

        if (PackageResult) {
            if (PackageResult.AccommodationObjectList) {
                if (typeof PackageResult.AccommodationObjectList.AccommodationObject != "undefined") {
                    if (Array.isArray(PackageResult.AccommodationObjectList.AccommodationObject)) {

                        tour = PackageResult.AccommodationObjectList.AccommodationObject[0];

                        tourData.TourID = tour.ObjectID || '';
                        tourData.TourTitle = tour.Name || '';

                        tourData.Type = (tour.ObjectType) ? tour.ObjectType.ObjectTypeID : null;
                        tourData.TypeName = (tour.ObjectType) ? tour.ObjectType.ObjectTypeName : null;

                        if (tour.UnitList) {
                            if (Array.isArray(tour.UnitList.AccommodationUnit)) {
                                if (tour.UnitList.AccommodationUnit.length > 0) {
                                    tourData.PriceInfo = await this.getPriceInfo(tourData.Type, tour.UnitList.AccommodationUnit[0], tour.NoteList, null);
                                }
                            }
                        }

                        tourData.Style = await this.getStyle(tour.CategoryList);
                        tourData.Mapa = await this.getMapa(tour.PhotoList);
                        tourData.Galery = await this.getGallery(tour.PhotoList);
                        tourData.MainTheme = await this.getMainTheme(tour.AttributeGroupList);
                        tourData.DurationDD = 1;
                        tourData.DurationHR = await this.getDurationHR(tour.AttributeGroupList);
                        tourData.DeparturePoint = await this.getDeparturePoint(tour.AttributeGroupList);
                        tourData.DepartureTimes = await this.getDepartureTimes(tour.AttributeGroupList);
                        tourData.ArrivalPoint = await this.getArrivalPoint(tour.AttributeGroupList);
                        tourData.BambaRecommendedTrip = await this.getBambaRecommended(tour.AttributeGroupList);

                        tourData.Countries = (PackageResult.CountryList) ? await DestinationHelper.getCountriesBycountries(PackageResult.CountryList.Country) : [];

                        tourData.NumberOfCountries = tourData.Countries.length || 0;
                        tourData.StartDestination = await this.getStartDestination(PackageResult.DestinationList);
                        tourData.EndDestination = await this.getEndDestination(tourData.Type, tour.AttributeGroupList.AttributeGroup);

                        if (!tourData.EndDestination) {
                            tourData.EndDestination = tourData.StartDestination;
                        }

                        tourData.Themes = await this.getThemes(tour.AttributeGroupList);

                        htmlShort = await HtmlHelper.getHtmlParser(tour.ShortDescription);
                        tourData.Overview = await this.getOverview(htmlShort);
                        tourData.Highlights = await this.getHighlights(htmlShort);

                        htmlDesc = await HtmlHelper.getHtmlParser(tour.Description);
                        tourData.WhatYouGet = await this.getWhatYouGet(htmlDesc);
                        tourData.WhatIsNotIncluded = await this.getWhatIsNotIncluded(htmlDesc);
                        tourData.Itinerary = await this.getItinerary(htmlShort, htmlDesc);


                        tourData.AccommodationPickupIncluded = await this.getAccomodationPickupIncluded(tour.AttributeGroupList);
                        tourData.AccommodationDropOffIncluded = await this.getAccomodationDropOffIncluded(tour.AttributeGroupList);
                        tourData.LocalPayment = await this.getLocalPayment(tour.AttributeGroupList);

                        if (tourData.LocalPayment) {
                            tourData.WhatIsNotIncluded.push('*' + tourData.LocalPayment);
                        }

                        if (tour.UnitList) {
                            if (Array.isArray(tour.UnitList.AccommodationUnit)) {
                                if (tour.UnitList.AccommodationUnit.length > 0) {
                                    tourData.UnitID = await this.getUnitIDBasic(tour.UnitList.AccommodationUnit);
                                    tourData.ServiceID = await this.getServiceIDBasic(tour.UnitList.AccommodationUnit);

                                    tourData.AccommodationObjectList = await this.getAccomodationList(tourData.TourTitle, tour.UnitList.AccommodationUnit, true);
                                    tourData.AccommodationObjectList = await this.PriceForServiceListLibrary.getPriceForServiceList(tourData.AccommodationObjectList, await DateHelper.now(), await DateHelper.now(), global.constants.DEFAULT_CURRENCY_ID, global.constants.DEFAULT_PAYMENT_METHOD_ID, await LanguageHelper.getLang());

                                    if (tourData.UnitID) {
                                        tourData.ListNotStartDays = await this.ReservationCanNotStartDatesBamba.getCanNotDates({
                                            UnitID: tourData.UnitID
                                        });
                                    }
                                }
                            }
                        }

                        tourData.Easy = await this.getEasy(tour.AttributeGroupList);
                        tourData.Moderate = await this.getModerate(tour.AttributeGroupList);
                        tourData.MildlyChallenging = await this.getMildlyChallenging(tour.AttributeGroupList);
                        tourData.Challenging = await this.getChallenging(tour.AttributeGroupList);
                        tourData.VeryChallenging = await this.getVeryChallenging(tour.AttributeGroupList);

                        tourData.NoteAdditionalInformation = await this.getNoteAdditionalInformation(tour.NoteList);
                        tourData.NoteImportantInformation = await this.getNoteImportantInformation(tour.NoteList);
                        tourData.NoteCoupon = await this.getNoteCupon(tour.NoteList);
                        tourData.NoteDiscount = await this.getNoteDiscount(tour.NoteList);

                        tourData.LanguageID = await LanguageHelper.getLang();
                    }

                }
            }
        }

        return tourData;

    }

    /**
     * 
     * @override
     * @param {*} CategoryID 
     */
    async getStyle(styleCategory) {

        var style = [];

        if (styleCategory) {
            if (typeof styleCategory.Category != "undefined") {
                if (Array.isArray(styleCategory.Category)) {
                    if (styleCategory.Category.length > 0) {
                        for (const i in styleCategory.Category) {
                            switch (parseInt(styleCategory.Category[i].CategoryID)) {
                                case global.constants.ACCOMODATION_CATEGORY_SHARED_TOUR:
                                case global.constants.ACCOMODATION_CATEGORY_PRIVATE_TOUR:
                                    styleCategory.Category[i].CategoryID = global.constants.TRAVEL_STYLE_GROUP_ADVENTURES;
                                    style = super.getStyle(styleCategory);
                                    break;
                                case global.constants.ACCOMODATION_CATEGORY_SELF_GUIDE_TOUR:
                                    styleCategory.Category[i].CategoryID = global.constants.ACCOMODATION_CATEGORY_SELF_GUIDE_TOUR;
                                    style = super.getStyle(styleCategory);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return style;
    }

    /**
     * Recover the starting point
     * 
     * @param {*} atributeGroup 
     */
    async getDeparturePoint(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_ACTIVITY_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_DEPARTURE_POINT, atributeGroupList);

        return value;
    }

    /**
     * Retrieve the departure time
     * 
     * @param {*} atributeGroup 
     */
    async getDepartureTimes(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_ACTIVITY_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_DEPARTURE_TIMES, atributeGroupList);

        return value;
    }

    /**
     * Regain the duration in hours
     * 
     * @param {*} atributeGroup 
     */
    async getDurationHR(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_ACTIVITY_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_DURATION_HR, atributeGroupList);

        return value;
    }

    /**
     * Retrieve the point of arrival
     * 
     * @param {*} atributeGroup 
     */
    async getArrivalPoint(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_ACTIVITY_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_ARRIVAL_POINT, atributeGroupList);

        return value;
    }

    /**
     * 
     * @param {*} atributeGroup 
     */
    async getAirportPickupIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_ACTIVITY_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_AIRPORT_PICKUP_INCLUDED, atributeGroupList);

        return value;
    }

    /**
     * 
     * @override
     * @param {*} htmlStructure 
     */
    async getWhatYouGet(htmlStructure) {
        var whatYouGet = [{
            number: null,
            title: null,
            subtitle: null,
            list: []
        }];
        var totalElements = htmlStructure.length;
        var contaHeaders = 0;
        var childNodes = [];

        for (var i = 0; i < totalElements; i++) {
            if (typeof htmlStructure[i].tagName != 'undefined') {
                if (htmlStructure[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 2) {

                if (htmlStructure[i].tagName != 'h4') {
                    if (htmlStructure[i].childNodes.length > 0) {
                        childNodes = htmlStructure[i].childNodes;
                        for (const j in childNodes) {
                            if (childNodes[j].text != "" && childNodes[j].text != " ") {
                                whatYouGet[0].list.push(childNodes[j].text.trim());
                            }
                        }
                    }
                }
            }
        }

        return whatYouGet;
    }

    /**
     * 
     * @param {*} htmlStructureShort 
     * @param {*} htmlStructureDesc 
     */
    async getItinerary(htmlStructureShort, htmlStructureDesc) {
        var itinerary = await ItineraryInterface.getLevelItineraryInterface();
        itinerary.Itinerary.push(await ItineraryInterface.getLevelContryInterfeace());
        itinerary.Itinerary[0].Itinerary.push(await ItineraryInterface.getLevelDestinationInterface());
        itinerary.Itinerary[0].Itinerary[0].Itinerary.push({
            Title: null,
            ShortDescription: [],
            LongDescription: []
        });

        var childNodes = [];
        var contaHeaders = 0;

        for (var i = 0; i < htmlStructureShort.length; i++) {
            if (typeof htmlStructureShort[i].tagName != 'undefined') {
                if (htmlStructureShort[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 1) {
                if (htmlStructureShort[i].tagName != 'h4') {
                    if (htmlStructureShort[i].childNodes.length > 0) {
                        childNodes = htmlStructureShort[i].childNodes;
                        for (const j in childNodes) {
                            if (childNodes[j].text != "" && childNodes[j].text != " ") {
                                itinerary.Itinerary[0].Itinerary[0].Itinerary[0].ShortDescription.push(childNodes[j].text.trim());
                            }
                        }
                    }
                }
            }

            if (contaHeaders > 1) {
                break;
            }
        }

        contaHeaders = 0;
        for (var i = 0; i < htmlStructureDesc.length; i++) {
            if (typeof htmlStructureDesc[i].tagName != 'undefined') {
                if (htmlStructureDesc[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 1) {
                if (htmlStructureDesc[i].tagName != 'h4') {
                    if (htmlStructureDesc[i].childNodes.length > 0) {
                        childNodes = htmlStructureDesc[i].childNodes;
                        for (const j in childNodes) {
                            if (childNodes[j].text != "" && childNodes[j].text != " ") {
                                itinerary.Itinerary[0].Itinerary[0].Itinerary[0].LongDescription.push(childNodes[j].text.trim());
                            }
                        }
                    }
                }
            }

            if (contaHeaders > 1) {
                break;
            }
        }

        // para crear todos los items faltantes en null
        itinerary.Itinerary = await this.addItemsItinerary(itinerary.Itinerary);

        return itinerary;
    }


    /**
     * 
     * @param {*} AttributeGroup 
     */
    async getRoomRules(AttributeGroup) {
        var RoomOccupateRuleList = [];

        const capacity = await this.AttributeGroupLibrary.getAttribute(global.constants.TOUR_TYPE_ACTIVITIES_TOUR, global.constants.ATTRIBUTE_CAPACITY, AttributeGroup);
        const minAge = await this.AttributeGroupLibrary.getAttribute(global.constants.TOUR_TYPE_ACTIVITIES_TOUR, global.constants.ATTRIBUTE_MINUM_AGE, AttributeGroup);

        var rooms = {
            NumberOfPersons: 0,
            AgeFrom: 0,
            AgeTo: 0,
        };

        if (capacity) {
            rooms.NumberOfPersons = capacity;
        }

        rooms.AgeFrom = minAge || global.constants.MIN_AGE;
        rooms.AgeTo = global.constants.MAX_AGE;

        RoomOccupateRuleList.push(rooms);


        return RoomOccupateRuleList;
    }

    async getAccomodationPickupIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_ACTIVITY_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_PICKUP_INCLUDED, atributeGroupList);

        return value;
    }

    async getAccomodationDropOffIncluded(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_ACTIVITY_DEPARTURE_AND_ARRIBAL_INFO, global.constants.ATTRIBUTE_DROPOFF_INCLUDED, atributeGroupList);

        return value;
    }

    /**
     * Retrieve the information of the units and the additionalservices
     * 
     * @param {string} Name 
     * @param {array} PackageUnit 
     * @param {array} AdditionalServicesList 
     * @returns {array}
     */
    async getAccomodationList(Name, PackageUnit, AdditionalServicesList) {
        var AccommodationObjectList = [];
        var itemAccomodationObject = null;
        var typeGroup = null;

        if (PackageUnit.length > 0) {
            for (const i in PackageUnit) {

                itemAccomodationObject = await CheckoutInterface.getAccomodationItemInterface();
                itemAccomodationObject.UnitID = PackageUnit[i].UnitID;
                itemAccomodationObject.ComponentName = Name;
                itemAccomodationObject.AvailabilityStatus =  PackageUnit[i].AvailabilityStatus;

                typeGroup = PackageUnit[i].Type.UnitTypeID;

                // sacar nombre
                if (PackageUnit[i].AttributeGroupList) {
                    if (Array.isArray(PackageUnit[i].AttributeGroupList.AttributeGroup)) {
                        if (PackageUnit[i].AttributeGroupList.AttributeGroup.length > 0) {
                            for (const j in PackageUnit[i].AttributeGroupList.AttributeGroup) {
                                if (PackageUnit[i].AttributeGroupList.AttributeGroup[j].GroupID == parseInt(typeGroup)) {
                                    if (PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList) {
                                        if (Array.isArray(PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute)) {
                                            for (const k in PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute) {

                                                if (PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute[k].AttributeID == global.constants.ATTRIBUTE_DESCRIPTION) {
                                                    itemAccomodationObject.UnitName = PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute[k].AttributeValue;
                                                    itemAccomodationObject.Description = PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute[k].AttributeValue;
                                                }

                                                if (PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute[k].AttributeID == global.constants.ATTRIBUTE_CAPACITY) {
                                                    itemAccomodationObject.RoomOccupateRuleList.push({
                                                        NumberOfPersons: PackageUnit[i].AttributeGroupList.AttributeGroup[j].AttributeList.Attribute[k].AttributeValue,
                                                        AgeFrom: global.constants.MIN_AGE,
                                                        AgeTo:  global.constants.MAX_AGE
                                                    });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (PackageUnit[i].ServiceList) {
                    if (Array.isArray(PackageUnit[i].ServiceList.Service)) {
                        if (PackageUnit[i].ServiceList.Service.length > 0) {
                            itemAccomodationObject.ServiceID = PackageUnit[i].ServiceList.Service[0].ServiceID;
                            itemAccomodationObject.ServiceName = PackageUnit[i].ServiceList.Service[0].ServiceName;
                        }
                    }

                    if (AdditionalServicesList) {
                        itemAccomodationObject.AdditionalServicesList = await this.getServicesList(PackageUnit[i]);
                    }
                }

                AccommodationObjectList.push(itemAccomodationObject);
            }
        }

        return AccommodationObjectList;
    }
}