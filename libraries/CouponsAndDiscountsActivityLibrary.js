import CuponsAndDiscountInterface from '../interface/cuponsAndDiscountInterface';
import GetSearchResults from '../services/lemax/GetSearchResults';
import TourLibrary from './TourLibrary';
import LangHelper from '../helpers/LanguageHelper';

export default class CouponsAndDiscountsActivityLibrary {

    constructor() {
        this.TourLibrary = new TourLibrary();
    }

    async getCuponAndDiscount(UnitID, params) {
        var tour = await CuponsAndDiscountInterface.getTourInterface();
        var searchResults = new GetSearchResults();

        searchResults.ShowFileds = [
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 4
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 4
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 100
            }
        ];

        searchResults.LanguageID = params.LanguageID || await LangHelper.getLang();
        searchResults.FilterType = [global.constants.FILTER_BY_ACTIVITIES];

        if (typeof params.TourID != "undefined") {
            searchResults.ObjectIDList = [params.TourID];
        }

        if (typeof params.InPriceType != "undefined") {
            searchResults.InPriceType = params.InPriceType;
        } else {
            searchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await searchResults.GetSearchResults();
        if (typeof response.GetSearchResultsResult != "undefined") {
            if (response.GetSearchResultsResult.Status.Code == 'OK') {

                if (response.GetSearchResultsResult) {
                    if (response.GetSearchResultsResult.AccommodationObjectList) {
                        var tourResult = response.GetSearchResultsResult.AccommodationObjectList.AccommodationObject[0];
                        var type = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeID : null;

                        var noteDiscount = await this.TourLibrary.getNoteDiscount(tourResult.NoteList);
                        var noteCoupon = await this.TourLibrary.getNoteCupon(tourResult.NoteList);
                        var specialOffer = null;
                        var packageUnit = await this.TourLibrary.getPackageUnitByID(UnitID, tourResult.UnitList.AccommodationUnit);

                        if(tourResult.UnitList) {
                           specialOffer = await this.TourLibrary.getSpecialOffer(type, tourResult.UnitList.AccommodationUnit[0]);
                        }

                        if(noteDiscount) {
                            tour.noteDiscount =  noteDiscount;
                        }

                        if(noteCoupon) {
                            tour.noteCoupon = noteCoupon; 
                        }

                        if(specialOffer) {
                            tour.specialOffer = specialOffer;
                        }

                        if (packageUnit) {
                            tour.availabilityStatus = packageUnit.AvailabilityStatus;
                        }
                    }
                }
            }
        }

        return tour;
    }
}