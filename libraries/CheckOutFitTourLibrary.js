import CheckoutInterface from '../interface/checkoutInterface';
import GetPackageDetailedDescription from '../services/lemax/GetPackageDetailedDescription';
import FitTourLibrary from '../libraries/FitTourLibrary';
import TravelAndExtrasFitTourLibrary from './TravelAndExtrasFitTourLibrary';
import ChildrenToursBamba from '../services/bamba/ChildrenToursBamba';
import LanguageHelper from '../helpers/LanguageHelper';
import InsertReservationBamba from '../services/bamba/InsertReservationBamba';

export default class CheckOutFitTourLibrary {

    constructor() {
        this.FitTourLibrary = new FitTourLibrary();
        this.TravelAndExtrasFitTourLibrary = new TravelAndExtrasFitTourLibrary();
        this.ChildrenToursBamba = new ChildrenToursBamba();
        this.InsertReservationBamba = new InsertReservationBamba();
    }

    /**
     * Generates an object for the reception section of the tab one of the fit tour
     * 
     * @param {object} params 
     * @returns {object}
     */
    async getCheckOut(params) {
        var tour = await CheckoutInterface.getCheckoutInterface();
        var packageDetailed = new GetPackageDetailedDescription();

        if (params.TourID) {
            packageDetailed.PackageTourID = params.TourID;
        }

        if (params.PackageTourCode) {
            packageDetailed.PackageTourCode = params.PackageTourCode;
        }

        if (params.LanguageID) {
            packageDetailed.LanguageID = params.LanguageID;
        } else {
            packageDetailed.LanguageID = await LanguageHelper.getLang();
        }

        if (params.CurrencyID) {
            packageDetailed.CurrencyID = params.CurrencyID;
        } else {
            packageDetailed.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (params.InPriceType) {
            packageDetailed.InPriceType = params.InPriceType;
        } else {
            packageDetailed.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await packageDetailed.GetPackageDetailedDescription();
        var GetPackageDetailedDescriptionResult = response.GetPackageDetailedDescriptionResult;

        if (GetPackageDetailedDescriptionResult.Status.Code == 'OK') {

            if (typeof GetPackageDetailedDescriptionResult.PackageTour != "undefined") {
                if (GetPackageDetailedDescriptionResult.PackageTour) {

                    var tourResult = GetPackageDetailedDescriptionResult.PackageTour;

                    tour.TourID = tourResult.ObjectID;
                    tour.Type = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeID : null;
                    tour.TypeName = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeName : null;
                    tour.TourTitle = tourResult.Name;

                    tour.Countries = (GetPackageDetailedDescriptionResult.CountryList) ? GetPackageDetailedDescriptionResult.CountryList.Country : [];
                    tour.NumberOfCountries = tour.Countries.length || 0;

                    tour.Style = await this.FitTourLibrary.getStyle(tourResult.CategoryList);

                    tour.AccommodationPickupIncluded = await this.FitTourLibrary.getAccomodationPickupIncluded(tourResult.AttributeGroupList);
                    tour.AccommodationDropOffIncluded = await this.FitTourLibrary.getAccomodationDropOffIncluded(tourResult.AttributeGroupList);

                    if (tourResult.PackageUnitList) {
                        if (Array.isArray(tourResult.PackageUnitList.PackageUnit)) {
                            if (tourResult.PackageUnitList.PackageUnit.length > 0) {
                                tour.PriceInfo = await this.FitTourLibrary.getPriceInfo(tour.Type, tourResult.PackageUnitList.PackageUnit[0], tourResult.NoteList, null);
                            }
                        }
                    }

                    if (tourResult.PackageUnitList) {
                        if (Array.isArray(tourResult.PackageUnitList.PackageUnit)) {
                            if (tourResult.PackageUnitList.PackageUnit.length > 0) {
                                tour.UnitID = await this.FitTourLibrary.getUnitIDBasic(tourResult.PackageUnitList.PackageUnit);
                                tour.ServiceID = await this.FitTourLibrary.getServiceIDBasic(tourResult.PackageUnitList.PackageUnit);

                                // para calcular la duracion
                                if (tour.UnitID && tour.ServiceID && typeof params.CalcularDurationDD != "undefined") {
                                    if (parseInt(params.CalcularDurationDD) == 1) {
                                        var components = await this.InsertReservationBamba.getComponents(tour.UnitID, tour.ServiceID, params.CurrencyID, global.constants.DEFAULT_PAYMENT_METHOD_ID, params.LanguageID);

                                        components = await this.FitTourLibrary.ordenarComponentes(components);
                                        tour.DurationDD = await this.FitTourLibrary.getDurationDD(components);
                                    }
                                }

                                tour.AccommodationObjectList = await this.FitTourLibrary.getAccomodationList(tour.TourID, tourResult.AttributeGroupList, tourResult.PackageUnitList.PackageUnit);
                            }
                        }
                    }
                }
            }
        }

        return tour;
    }

    
}