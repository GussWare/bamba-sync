import DestinationHelper from '../helpers/DestinationHelper';
import ShopInterface from '../interface/shopInterface';
import TourLibrary from './TourLibrary';
import ActivitiesTourLibrary from './ActivitiesTourLibrary';

export default class ShopActivitiesLibrary {

    constructor() {
        this.TourLibrary = new TourLibrary();
        this.ActivitiesTourLibrary = new ActivitiesTourLibrary();
    }

    /**
     * Form the array of the product listing for the store page
     * 
     * @param {PackageResult} PackageResult 
     */
    async getProductsList(PackageResult) {

        var productList = [];
        var result = await ShopInterface.getShopInterface();
        var packageTour = PackageResult.AccommodationObjectList.AccommodationObject;
        var atributeGroup = [];
        var atributeGroupListAtribute = [];
        var countriesList = [];
        var styleKeysotne = null;

        for (const i in packageTour) {
            var tourData = await ShopInterface.getShopItemInterface();

            atributeGroup = [];
            atributeGroupListAtribute = [];

            tourData.TourID = packageTour[i].ObjectID || '';
            tourData.TourTitle = packageTour[i].Name || '';

            tourData.Type = (packageTour[i].ObjectType) ? packageTour[i].ObjectType.ObjectTypeID : null;
            tourData.TypeName = (packageTour[i].ObjectType) ? packageTour[i].ObjectType.ObjectTypeName : null;

            tourData.Mapa = await this.TourLibrary.getMapa(packageTour[i].PhotoList);
            tourData.Galery = await this.TourLibrary.getGallery(packageTour[i].PhotoList);

            styleKeysotne = null;
            if(packageTour[i].CategoryList) {
                styleKeysotne = await this.ActivitiesTourLibrary.getStyle(packageTour[i].CategoryList);
                if(Array.isArray(styleKeysotne)) {
                    if(styleKeysotne.length > 0) {
                        tourData.Style.push(styleKeysotne[0]);
                    }
                }
            }

            if (packageTour[i].UnitList) {
                if (Array.isArray(packageTour[i].UnitList.AccommodationUnit)) {
                    if (packageTour[i].UnitList.AccommodationUnit.length > 0) {
                        tourData.PriceInfo = await this.TourLibrary.getPriceInfo(tourData.Type, packageTour[i].UnitList.AccommodationUnit[0], packageTour[i].NoteList, null);
                    }
                }
            }

            tourData.UnitID = (packageTour[i].UnitList) ? packageTour[i].UnitList.AccommodationUnit[0].UnitID : null;

            if (packageTour[i].DestinationID) {
                countriesList = await DestinationHelper.getCountriesDestination({Destination:[{DestinationID:packageTour[i].DestinationID}]});

                if (countriesList.length > 0) {
                    tourData.Countries.push(countriesList[0]);
                    tourData.NumberOfCountries = countriesList.length;
                }
            }

            if (packageTour[i].AttributeGroupList) {
                atributeGroup = packageTour[i].AttributeGroupList.AttributeGroup

                for (const j in atributeGroup) {
                    if (atributeGroup[j].GroupID == global.constants.GROUP_NAME_MAIN_THEME) {
                        atributeGroupListAtribute = atributeGroup[j].AttributeList.Attribute;

                        for (const k in atributeGroupListAtribute) {
                            tourData.MainTheme.push({
                                MainThemeID: atributeGroupListAtribute[k].AttributeID,
                                MainThemeName: atributeGroupListAtribute[k].AttributeName,
                                MainThemeValue: atributeGroupListAtribute[k].AttributeValue
                            });
                        }
                    }

                    if (atributeGroup[j].GroupID == global.constants.GROUP_NAME_ACTIVITY_DEPARTURE_AND_ARRIBAL_INFO) {
                        atributeGroupListAtribute = atributeGroup[j].AttributeList.Attribute;

                        for (const k in atributeGroupListAtribute) {
                            if (atributeGroupListAtribute[k].AttributeID == global.constants.ATTRIBUTE_DURATION_DD) {
                                tourData.DurationDD = atributeGroupListAtribute[k].AttributeValue;
                            }

                            if (atributeGroupListAtribute[k].AttributeID == global.constants.ATTRIBUTE_DURATION_HR) {
                                tourData.DurationHR = atributeGroupListAtribute[k].AttributeValue;
                            }
                        }
                    }

                    // agregado para las stars
                    if (atributeGroup[j].GroupID == tourData.Type) {
                        atributeGroupListAtribute = atributeGroup[j].AttributeList.Attribute;
                        for (const k in atributeGroupListAtribute) {
                            if (atributeGroupListAtribute[k].AttributeID == global.constants.ATTRIBUTE_ACCOMODATION_TYPE) { 
                                tourData.stars = atributeGroupListAtribute[k].AttributeValue;
                             }
                        }
                    }
                }
            }

            tourData.DurationDD = 1;

            productList.push(tourData);
        }

        result.PackageSearchResults = productList;
        result.Currency = (PackageResult.Currency) ? PackageResult.Currency : null;
        result.Pagination.CurrentPage = PackageResult.CurrentPage || '';
        result.Pagination.PageSize = PackageResult.PageSize || '';
        result.Pagination.TotalNumberOfResults = PackageResult.TotalNumberOfResults || '';

        return result;
    }
}