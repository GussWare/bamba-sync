
export default class AttributeGroupLibrary {

    constructor() {

    }

    /**
     * 
     */
    async getAttribute(GroupID, AttributeID, AttributeGroupList) {
        var value = null;

        if (AttributeGroupList) {
            if (Array.isArray(AttributeGroupList.AttributeGroup)) {
                if(AttributeGroupList.AttributeGroup.length > 0) {
                    for(const i in AttributeGroupList.AttributeGroup) {
                        if(AttributeGroupList.AttributeGroup[i].GroupID == GroupID) {
                            if(AttributeGroupList.AttributeGroup[i].AttributeList){
                                if(Array.isArray(AttributeGroupList.AttributeGroup[i].AttributeList.Attribute)){
                                    if(AttributeGroupList.AttributeGroup[i].AttributeList.Attribute.length > 0) {
                                        for(const j in AttributeGroupList.AttributeGroup[i].AttributeList.Attribute) {
                                            if(AttributeGroupList.AttributeGroup[i].AttributeList.Attribute[j].AttributeID == AttributeID) {
                                                value = AttributeGroupList.AttributeGroup[i].AttributeList.Attribute[j].AttributeValue;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            break;
                        }
                    }
                }
            }
        }

        return value;
    }
}