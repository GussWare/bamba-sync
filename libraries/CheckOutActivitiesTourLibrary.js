import CheckoutInterface from '../interface/checkoutInterface';
import GetSearchResults from '../services/lemax/GetSearchResults';
import ActivitiesTourLibrary from '../libraries/ActivitiesTourLibrary';
import TravelAndExtrasActivityTourLibrary from '../libraries/TravelAndExtrasActivityTourLibrary';

export default class CheckOutActivitiesTourLibrary {

    constructor() {

        this.ActivitiesTourLibrary = new ActivitiesTourLibrary();
        this.TravelAndExtrasActivityTourLibrary = new TravelAndExtrasActivityTourLibrary();
    }

    /**
     * Generates an object for the reception section of the tab one of the activity tour
     * 
     * @param {object} params 
     * @returns {object}
     */
    async getCheckOut(params) {
        var tour = await CheckoutInterface.getCheckoutInterface();
        var searchResults = new GetSearchResults();

        searchResults.ShowFileds = [
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 10
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 20
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 20
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 20
            },
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 4
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 4
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 20
            }
        ];

        searchResults.LanguageID = params.LanguageID || await LangHelper.getLang();
        searchResults.FilterType = [global.constants.FILTER_BY_ACTIVITIES];

        if (typeof params.TourID != "undefined") {
            searchResults.ObjectIDList = [params.TourID];
        }

        if (typeof params.InPriceType != "undefined") {
            searchResults.InPriceType = params.InPriceType;
        } else {
            searchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await searchResults.GetSearchResults();
        if (typeof response.GetSearchResultsResult != "undefined") {
            if (response.GetSearchResultsResult.Status.Code == 'OK') {

                tour = await CheckoutInterface.getCheckoutInterface();

                if (response.GetSearchResultsResult) {
                    if (response.GetSearchResultsResult.AccommodationObjectList) {
                        var tourResult = response.GetSearchResultsResult.AccommodationObjectList.AccommodationObject[0];

                        tour.TourID = tourResult.ObjectID;
                        tour.Type = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeID : null;
                        tour.TypeName = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeName : null;
                        tour.TourTitle = tourResult.Name;
                        tour.Countries = (response.GetSearchResultsResult.CountryList) ? response.GetSearchResultsResult.CountryList.Country : [];
                        tour.NumberOfCountries = tour.Countries.length || 0;
                        tour.Style = await this.ActivitiesTourLibrary.getStyle(tourResult.CategoryList);
                        tour.DurationDD = 1;
                        tour.UnitID = null;
                        tour.ServiceID = null;
                        tour.AccommodationObjectList = null;

                        tour.AccommodationPickupIncluded = await this.ActivitiesTourLibrary.getAccomodationPickupIncluded(tourResult.AttributeGroupList);
                        tour.AccommodationDropOffIncluded = await this.ActivitiesTourLibrary.getAccomodationDropOffIncluded(tourResult.AttributeGroupList);

                        if (tourResult.UnitList) {
                            if (Array.isArray(tourResult.UnitList.AccommodationUnit)) {
                                if (tourResult.UnitList.AccommodationUnit.length > 0) {
                                    tour.PriceInfo = await this.ActivitiesTourLibrary.getPriceInfo(tour.Type, tourResult.UnitList.AccommodationUnit[0], tourResult.NoteList, null);
                                }
                            }
                        }

                        if (tourResult.UnitList) {
                            if (Array.isArray(tourResult.UnitList.AccommodationUnit)) {
                                if (tourResult.UnitList.AccommodationUnit.length > 0) {
                                    tour.UnitID = await this.ActivitiesTourLibrary.getUnitIDBasic(tourResult.UnitList.AccommodationUnit);
                                    tour.ServiceID = await this.ActivitiesTourLibrary.getServiceIDBasic(tourResult.UnitList.AccommodationUnit);
                                    tour.AccommodationObjectList = await this.ActivitiesTourLibrary.getAccomodationList(tourResult.Name, tourResult.UnitList.AccommodationUnit, false);
                                }
                            }
                        }
                    }
                }
            }
        }

        return tour;
    }
}