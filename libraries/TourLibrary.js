import DestinationHelper from '../helpers/DestinationHelper';
import StyleHelper from '../helpers/StyleHelper';
import ThemesHelper from '../helpers/ThemesHelper';
import CheckoutInterface from '../interface/checkoutInterface';
import ShopInterface from '../interface/shopInterface';
import CuponAndDiscountInterface from '../interface/cuponsAndDiscountInterface';
import CouponAndDiscountBamba from '../services/bamba/CouponAndDiscountBamba';
import IconTravelStylesLandingHelper from '../helpers/IconTravelStylesLandingHelper';
import IconTravelsHelper from '../helpers/IconTravelsHelper';
import NumberHelper from '../helpers/NumberHelper';
import AttributeGroupLibrary from '../libraries/AttributeGroupLibrary';

export default class TourLibrary {


    constructor() {
        this.AttributeGroupLibrary = new AttributeGroupLibrary();
    }

    /**
     * 
     */
    async getPriceInfo(TourType, PackageUnit, NoteDiscount, SpecialOffer) {
        var priceInfo = null;

        if (PackageUnit) {

            var noteDiscount = null;
            var specialOffer = null;
            var price = null;
            var priceFormatted = null;
            var billingTypeName = null;
            var percent = null;

            priceInfo = await ShopInterface.getPriceInfoInterface();

            const couponAndDiscountBamba = new CouponAndDiscountBamba();

            price = PackageUnit.UnitMinimumPriceInfo.Price;
            priceFormatted = PackageUnit.UnitMinimumPriceInfo.PriceFormatted;
            billingTypeName = PackageUnit.UnitMinimumPriceInfo.BillingTypeName;

            priceInfo.Price = price;
            priceInfo.PriceFormatted = priceFormatted;
            priceInfo.BillingTypeName = billingTypeName;

            specialOffer = await this.getSpecialOffer(TourType, PackageUnit);

            if (specialOffer) {
                priceInfo.PriceOriginal = price;
                priceInfo.PriceOriginalFormatted = priceFormatted;

                priceInfo.SpecialOffer = await CuponAndDiscountInterface.getSpecialOfferInterface();
                priceInfo.SpecialOffer.Amount = priceInfo.PriceOriginal;
                priceInfo.SpecialOffer.Discount = specialOffer.Price;
                priceInfo.SpecialOffer.Total = await couponAndDiscountBamba.calculateSpecialOffer(priceInfo.PriceOriginal, specialOffer.Price);

                priceInfo.Price = priceInfo.SpecialOffer.Total;
                priceInfo.PriceFormatted = await NumberHelper.formatNumber(priceInfo.SpecialOffer.Total, 2);
            } else {
                if (NoteDiscount) {
                    noteDiscount = await this.getNoteDiscount(NoteDiscount);
                }

                if (noteDiscount) {
                    priceInfo.PriceOriginal = price;
                    priceInfo.PriceOriginalFormatted = priceFormatted;

                    percent = await couponAndDiscountBamba.getHtmlPercent(noteDiscount.NoteText);

                    if (percent) {
                        priceInfo.Discount = await CuponAndDiscountInterface.getDiscountInterface();
                        priceInfo.Discount.Amount = priceInfo.PriceOriginal;
                        priceInfo.Discount.Percent = percent;
                        priceInfo.Discount.Total = await couponAndDiscountBamba.calculateDiscount(priceInfo.Discount.Amount, priceInfo.Discount.Percent);

                        priceInfo.Price = priceInfo.Discount.Total;
                        priceInfo.PriceFormatted = await NumberHelper.formatNumber(priceInfo.Discount.Total, 2);
                    }
                }
            }
        }

        return priceInfo;
    }

    /**
     * Returns the data of the map image for this tour
     * 
     * @param {array} PhotoList List of photographs
     * @returns {object} Image of the map
     */
    async getMapa(PhotoList) {

        var mapa = null;

        if (PhotoList) {
            if (Array.isArray(PhotoList.Photo)) {
                if (PhotoList.Photo.length > 1) {
                    mapa = PhotoList.Photo[1];
                }
            }
        }

        return mapa;
    }

    /**
     * It is responsible for removing the map from the list of images and retrieving the rest of the information, 
     * it will be used for the tour gallery.
     * 
     * @param {array} PhotoList List of photographs
     * @returns {array} List of images without the map
     */
    async getGallery(PhotoList) {
        var gallery = [];

        if (PhotoList) {
            if (Array.isArray(PhotoList.Photo)) {
                PhotoList.Photo.splice(1, 1);
                gallery = PhotoList.Photo;
            }
        }

        return gallery;
    }

    /**
     * Recover the style of the trip in keystone based on the lemax id
     * 
     * @param {int} styleCategory 
     * @returns {object} Travel style information
     */
    async getStyle(styleCategory) {

        var style = [];
        var itemStyle = null;
        var icons = [];

        if (styleCategory) {
            if (typeof styleCategory.Category != "undefined") {
                if (Array.isArray(styleCategory.Category)) {
                    if (styleCategory.Category.length > 0) {
                        for (const i in styleCategory.Category) {

                            itemStyle = await StyleHelper.getByLemaxId(styleCategory.Category[i].CategoryID);

                            if(itemStyle.icons) {
                                if (itemStyle.icons.length > 0) {
                                    for (const j in itemStyle.icons) {
                                        icons.push(
                                            await IconTravelsHelper.getIconTravel(itemStyle.icons[j])
                                        );
                                    }
    
                                    itemStyle.icons = icons;
                                }
                            }

                            if(itemStyle.iconsLanding) {
                                if (itemStyle.iconsLanding.length > 0) {
                                    icons = [];
    
                                    for (const j in itemStyle.iconsLanding) {
                                        icons.push(
                                            await IconTravelStylesLandingHelper.getIconTravel(itemStyle.iconsLanding[j])
                                        );
                                    }
    
                                    itemStyle.iconsLanding = icons;
                                }
                            }

                            style.push(itemStyle);
                        }

                    }
                }
            }
        }

        return style;
    }

    /**
     * Retrieve the list of topics associated with the tour
     * 
     * @param {*} atributeGroup 
     */
    async getThemes(atributeGroupList) {
        var themes = [];
        var ThemeMain = await ThemesHelper.getThemeCategoriesIDList();
        var atributeGroupListAtribute = [];

        if(atributeGroupList) {

            if (Array.isArray(atributeGroupList.AttributeGroup)) {
                for (const i in atributeGroupList.AttributeGroup) {
                    if (ThemeMain.indexOf(atributeGroupList.AttributeGroup[i].GroupID) > -1) {
                        if (atributeGroupList.AttributeGroup[i].AttributeList) {
                            atributeGroupListAtribute = atributeGroupList.AttributeGroup[i].AttributeList.Attribute;
    
                            for (const j in atributeGroupListAtribute) {
                                themes.push({
                                    ThemeID: atributeGroupListAtribute[j].AttributeID,
                                    ThemeName: atributeGroupListAtribute[j].AttributeName,
                                    ThemeValue: atributeGroupListAtribute[j].AttributeValue
                                });
                            }
                        }
                    }
                }
            }
        }

        return themes;
    }

    /**
     * Retrieve the main theme 
     * 
     * @param {*} atributeGroup 
     */
    async getMainTheme(atributeGroupList) {
        var mainTheme = [];
        var atributeGroupListAtribute = [];

        if(atributeGroupList) {
            if (Array.isArray(atributeGroupList.AttributeGroup)) {
                for (const i in atributeGroupList.AttributeGroup) {
                    if (atributeGroupList.AttributeGroup[i].GroupID == global.constants.GROUP_NAME_MAIN_THEME) {
                        atributeGroupListAtribute = atributeGroupList.AttributeGroup[i].AttributeList.Attribute;
    
                        for (const j in atributeGroupListAtribute) {
                            mainTheme.push({
                                MainThemeID: atributeGroupListAtribute[j].AttributeID,
                                MainThemeName: atributeGroupListAtribute[j].AttributeName,
                                MainThemeValue: atributeGroupListAtribute[j].AttributeValue
                            });
                        }
    
                        break;
                    }
                }
            }
        }

        return mainTheme;
    }

    /**
     * 
     * @param {*} PackageUnit 
     */
    async getRoomRules(PackageUnit) {
        var RoomOccupateRuleList = [];
        if (PackageUnit.RoomOccupantCombination) {
            if (PackageUnit.RoomOccupantCombination.RoomOccupantCaseList) {
                if (Array.isArray(PackageUnit.RoomOccupantCombination.RoomOccupantCaseList.RoomOccupantCase)) {
                    if (PackageUnit.RoomOccupantCombination.RoomOccupantCaseList.RoomOccupantCase.length > 0) {
                        if (PackageUnit.RoomOccupantCombination.RoomOccupantCaseList.RoomOccupantCase[0].RoomOccupantRuleList) {
                            if (PackageUnit.RoomOccupantCombination.RoomOccupantCaseList.RoomOccupantCase[0].RoomOccupantRuleList) {
                                RoomOccupateRuleList = PackageUnit.RoomOccupantCombination.RoomOccupantCaseList.RoomOccupantCase[0].RoomOccupantRuleList.RoomOccupantRule;
                            }
                        }
                    }
                }
            }
        }
        return RoomOccupateRuleList;
    }

    /**
     * 
     * @param {array} destinationList 
     * @returns {string} 
     */
    async getStartDestination(destinationList) {
        var destination = null;
        var destinationStart = null;
        var contryStart = {};

        if (typeof destinationList.Destination != "undefined") {
            if (Array.isArray(destinationList.Destination)) {
                destination = destinationList.Destination[0];
                contryStart = await DestinationHelper.getCountryDestination(destination.DestinationID);

                if (contryStart) {
                    destinationStart = destination.DestinationName.concat(', ', contryStart.name);
                }
            }
        }

        return destinationStart;
    }

    async getStartDestinationID(DestinationList) {
        var DestinationID = null;

        if (DestinationList) {
            if (Array.isArray(DestinationList)) {
                if (DestinationList.length > 0) {
                    DestinationID = DestinationList[0].DestinationID;
                }
            }
        }

        return DestinationID;
    }

    /**
     * Retrieve the final city from the list of attributes
     * 
     * @param {int} type Tour Type
     * @param {array} attrbuteList List of destinations
     * @returns {string} Information about the final destination
     */
    async getEndDestination(type, attributeGroup) {
        var endCity = null;
        if (Array.isArray(attributeGroup)) {
            if (attributeGroup.length > 0) {
                for (const i in attributeGroup) {
                    if (attributeGroup[i].GroupID == type) {
                        if (attributeGroup[i].AttributeList) {
                            if (typeof attributeGroup[i].AttributeList.Attribute != "undefined") {
                                if (Array.isArray(attributeGroup[i].AttributeList.Attribute)) {
                                    if (attributeGroup[i].AttributeList.Attribute.length > 0) {
                                        for (const j in attributeGroup[i].AttributeList.Attribute) {
                                            if (attributeGroup[i].AttributeList.Attribute[j].AttributeID == global.constants.ATTRIBUTE_END_CITY_COUNTRY) {
                                                endCity = attributeGroup[i].AttributeList.Attribute[j].AttributeValue;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return endCity;
    }

    async getEndDestinationID(EndDestination) {

        var DestinationID = null;

        if (EndDestination != "") {
            var DestinationSplit = EndDestination.split(',');

            if (Array.isArray(DestinationSplit)) {
                if (DestinationSplit.length > 0) {
                    var DestinationName = DestinationSplit[0].trim();
                    var Destination = await DestinationHelper.getDestinationByName(DestinationName);

                    if (Destination) {
                        DestinationID = Destination.lemaxId;
                    }
                }
            }
        }

        return DestinationID;
    }

    /**
     * Retrieve the initial description of the tour
     * 
     * @param {*} htmlStructure 
     */
    async getOverview(HtmlStructure) {
        var overview = [];
        var contaHeaders = 0;
        var totalElements = HtmlStructure.length;
        var childNodes = [];

        for (var i = 0; i < totalElements; i++) {

            if (typeof HtmlStructure[i].tagName != 'undefined') {
                if (HtmlStructure[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 1) {
                if (HtmlStructure[i].tagName != 'h4') {
                    if (HtmlStructure[i].childNodes.length > 0) {
                        childNodes = HtmlStructure[i].childNodes;
                        for (const j in childNodes) {
                            if (childNodes[j].text != "" && childNodes[j].text != " ") {
                                overview.push(childNodes[j].text);
                            }
                        }
                    }
                }
            }

            if (contaHeaders == 2) {
                break;
            }
        }

        return overview;
    }

    async getHighlights(HtmlStructure) {
        var highlights = [];
        var contaHeaders = 0;
        var totalElements = HtmlStructure.length;
        var childNodes = [];

        for (var i = 0; i < totalElements; i++) {

            if (typeof HtmlStructure[i].tagName != 'undefined') {
                if (HtmlStructure[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 2) {
                if (HtmlStructure[i].tagName != 'h4') {
                    if (HtmlStructure[i].childNodes.length > 0) {
                        childNodes = HtmlStructure[i].childNodes;
                        for (const j in childNodes) {
                            if (childNodes[j].text != "" && childNodes[j].text != " ") {
                                highlights.push(childNodes[j].text);
                            }
                        }
                    }
                }
            }

            if (contaHeaders > 2) {
                break;
            }
        }

        return highlights;
    }

    async getWhatYouGet(HtmlStructure) {
        var whatYouGet = [];
        var totalElements = HtmlStructure.length;
        var contaHeaders = 0;
        var bSubtitle = false;
        var tituloH5 = '';
        var posParentesis = null;
        var posCierraParentesis = null;
        var posNumeroParentesis = null;
        var tituloSPlit = [];
        var contaItems = 0;

        for (var i = 0; i < totalElements; i++) {
            if (typeof HtmlStructure[i].tagName != 'undefined') {
                if (HtmlStructure[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 2) {
                if (HtmlStructure[i].tagName == 'h5') {

                    whatYouGet[contaItems] = {
                        number: null,
                        title: null,
                        subtitle: null,
                        list: []
                    };

                    // agregamos el subtitulo si es que tiene
                    tituloH5 = HtmlStructure[i].text;
                    posParentesis = tituloH5.indexOf('(');
                    posCierraParentesis = tituloH5.indexOf(')');

                    bSubtitle = false;
                    if (posParentesis > -1 && posCierraParentesis > -1) {
                        bSubtitle = true;
                        whatYouGet[contaItems].subtitle = tituloH5.trim().substring(posParentesis + 1, posCierraParentesis);
                    }

                    // verificamos si hay :
                    tituloSPlit = HtmlStructure[i].text.split(":");
                    if (tituloSPlit.length > 1) {
                        // si hay : separamos el titulo y los numeros
                        whatYouGet[contaItems].title = tituloSPlit[0].substring(1).trim();
                        if (tituloSPlit[1].trim()) {
                            whatYouGet[contaItems].number = tituloSPlit[1].trim();
                        }

                        // si hay parentesis modificamos para recuperar el numero nada mas
                        if (bSubtitle) {
                            posNumeroParentesis = whatYouGet[contaItems].number.indexOf('(');
                            whatYouGet[contaItems].number = whatYouGet[contaItems].number.substring(0, posNumeroParentesis - 1);
                        }

                    } else {
                        // si no hay : ponemos todo el titulo y el numero lo dejamos como null
                        whatYouGet[contaItems].title = HtmlStructure[i].text.substring(1).trim();
                    }

                    contaItems++;

                } else {
                    whatYouGet[contaItems - 1].list.push(HtmlStructure[i].text);
                }
            }
        }

        for (const k in whatYouGet) {
            if (!whatYouGet[k].subtitle) {
                if (whatYouGet[k].list.length > 0) {
                    whatYouGet[k].subtitle = whatYouGet[k].list[0].trim().substring(1);
                }
            }
        }

        whatYouGet = await this.getOrderWhatYouGet(whatYouGet);

        return whatYouGet;
    }

    async getWhatIsNotIncluded(HtmlStructure) {
        var WhatIsNotIncluded = [];
        var totalElements = HtmlStructure.length;
        var contaHeaders = 0;

        for (var i = 0; i < totalElements; i++) {

            if (typeof HtmlStructure[i].tagName != 'undefined') {
                if (HtmlStructure[i].tagName == 'h4') {
                    contaHeaders++;
                    continue;
                }
            }

            if (contaHeaders == 3) {
                if (HtmlStructure[i].tagName != 'h4') {
                    if (HtmlStructure[i].childNodes.length > 0) {

                        for (const j in HtmlStructure[i].childNodes) {

                            if (Array.isArray(HtmlStructure[i].childNodes[j].childNodes)) {
                                if (HtmlStructure[i].childNodes[j].childNodes.length > 0) {
                                    for (const k in HtmlStructure[i].childNodes[j].childNodes) {

                                        if (HtmlStructure[i].childNodes[j].childNodes[k].text) {
                                            if (HtmlStructure[i].childNodes[j].childNodes[k].text != "<br>" &&
                                                HtmlStructure[i].childNodes[j].childNodes[k].text != "<br/>" &&
                                                HtmlStructure[i].childNodes[j].childNodes[k].text != "&nbsp;"
                                            ) {
                                                WhatIsNotIncluded.push(HtmlStructure[i].childNodes[j].childNodes[k].text);
                                            }
                                        }
                                    }
                                } else {

                                    if (HtmlStructure[i].childNodes[j].text) {
                                        if (HtmlStructure[i].childNodes[j].text != "<br>" &&
                                            HtmlStructure[i].childNodes[j].text != "<br/>" &&
                                            HtmlStructure[i].childNodes[j].text != "&nbsp;"
                                        ) {
                                            WhatIsNotIncluded.push(HtmlStructure[i].childNodes[j].text);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return WhatIsNotIncluded;
    }

    async getOrderWhatYouGet(whatYouGet) {
        var orderWhatYouGet = [];
        var posisiones = {
            "0": 2,
            "1": 1,
            "2": 3,
            "3": 0,
            "4": 4,
            "5": 5
        };

        for (const i in posisiones) {
            if (typeof whatYouGet[posisiones[i]] != "undefined") {
                orderWhatYouGet[i] = whatYouGet[posisiones[i]];
            }
        }

        return orderWhatYouGet;
    }

    /**
     * 
     * @param {array} itinerary 
     * @return {array}
     */
    async addItemsItinerary(itinerary) {

        var items = [
            'Title', 'LongDescription', 'ShortDescription', 'Activities', 'Transportation', 'SimpleTour', 'Accomodation'
        ];

        if (Array.isArray(itinerary)) {
            // pais
            if (itinerary.length > 0) {
                for (const i in itinerary) {
                    // destinos
                    if (Array.isArray(itinerary[i].Itinerary)) {
                        if (itinerary[i].Itinerary.length > 0) {
                            for (const j in itinerary[i].Itinerary) {

                                // items
                                if (Array.isArray(itinerary[i].Itinerary[j].Itinerary)) {
                                    if (itinerary[i].Itinerary[j].Itinerary.length > 0) {
                                        for (const k in itinerary[i].Itinerary[j].Itinerary) {
                                            for (const l in items) {
                                                if (!itinerary[i].Itinerary[j].Itinerary[k].hasOwnProperty(items[l])) {
                                                    itinerary[i].Itinerary[j].Itinerary[k][items[l]] = [];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return itinerary;
    }

    /** 
     * 
     */
    async getServicesList(PackageUnit) {
        var AdditionalServicesList = [];
        var itemAccomodationServiceList = {};

        if (PackageUnit.ServiceList) {
            if (Array.isArray(PackageUnit.ServiceList.Service)) {
                if (PackageUnit.ServiceList.Service.length > 0) {
                    for (const j in PackageUnit.ServiceList.Service) {
                        if (PackageUnit.ServiceList.Service[j].ServiceType == global.constants.SERVICE_TYPE_ADDITIONAL_SERVICE) {
                            itemAccomodationServiceList = await CheckoutInterface.getServiceListInterface();
                            itemAccomodationServiceList.ServiceID = PackageUnit.ServiceList.Service[j].ServiceID;
                            itemAccomodationServiceList.ServiceName = PackageUnit.ServiceList.Service[j].ServiceName
                            itemAccomodationServiceList.ServiceType = PackageUnit.ServiceList.Service[j].ServiceType;
                            AdditionalServicesList.push(itemAccomodationServiceList);
                        }
                    }
                }
            }
        }

        return AdditionalServicesList;
    }

    /**
     * Retrieve the list of tour notes
     * 
     * @param {array} NoteList 
     * @returns {array}
     */
    async getNotes(NoteList) {

        var notes = [];

        if (NoteList) {
            if (Array.isArray(NoteList.Note)) {
                if (NoteList.Note.length > 0) {
                    notes = NoteList.Note;
                }
            }
        }

        return notes;
    }

    /**
     * 
     * @param {*} NoteList 
     */
    async getNoteCupon(NoteList) {
        var cupon = null;
        var notes = await this.getNotes(NoteList);

        if (notes.length > 0) {
            for (const i in notes) {
                if (notes[i].NoteTitle == global.constants.NOTE_TYPE_CUPON) {
                    cupon = notes[i];
                }
            }
        }

        return cupon;
    }

    async getNoteDiscount(NoteList) {
        var note = null;
        var notes = await this.getNotes(NoteList);

        if (notes.length > 0) {
            for (const i in notes) {
                if (notes[i].NoteTitle == global.constants.NOTE_TYPE_DISCOUNT) {
                    note = notes[i];
                }
            }
        }

        return note;
    }

    async getNoteAdditionalInformation(NoteList) {
        var note = null;
        var notes = await this.getNotes(NoteList);

        if (notes.length > 0) {
            for (const i in notes) {
                if (notes[i].NoteTitle == global.constants.NOTE_TYPE_ADDITIONAL_INFORMATION) {
                    note = notes[i];
                }
            }
        }

        return note;
    }

    async getNoteImportantInformation(NoteList) {

        var note = null;
        var notes = await this.getNotes(NoteList);

        if (notes.length > 0) {
            for (const i in notes) {
                if (notes[i].NoteTitle == global.constants.NOTE_TYPE_IMPORTANT_INFORMATION) {
                    note = notes[i];
                }
            }
        }

        return note;
    }

    async getSpecialOffer(TourType, PackageUnit) {
        var specialOffer = null;

        switch (parseInt(TourType)) {
            case global.constants.TOUR_TYPE_ACTIVITIES_TOUR:
                if (PackageUnit) {
                    if (PackageUnit.CalculatedPriceInfo) {
                        if (PackageUnit.CalculatedPriceInfo.ServiceList) {
                            if (Array.isArray(PackageUnit.CalculatedPriceInfo.ServiceList.Service)) {
                                for (const i in PackageUnit.CalculatedPriceInfo.ServiceList.Service) {
                                    if (PackageUnit.CalculatedPriceInfo.ServiceList.Service[i].ServiceType == global.constants.SERVICE_TYPE_SPECIAL_OFFER) {
                                        specialOffer = PackageUnit.CalculatedPriceInfo.ServiceList.Service[i];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case global.constants.TOUR_TYPE_SIMPLE_TOUR:
                if (typeof PackageUnit.PackagePeriodList != "undefined") {
                    if (Array.isArray(PackageUnit.PackagePeriodList.PackagePeriod)) {
                        if (PackageUnit.PackagePeriodList.PackagePeriod.length > 0) {
                            var PackagePeriod = PackageUnit.PackagePeriodList.PackagePeriod[0];
                            if (PackagePeriod.CalculatedPriceInfo) {
                                if (PackagePeriod.CalculatedPriceInfo.ServiceList) {
                                    if (Array.isArray(PackagePeriod.CalculatedPriceInfo.ServiceList.Service)) {
                                        for (const i in PackagePeriod.CalculatedPriceInfo.ServiceList.Service) {
                                            if (PackagePeriod.CalculatedPriceInfo.ServiceList.Service[i].ServiceType == global.constants.SERVICE_TYPE_SPECIAL_OFFER) {
                                                specialOffer = PackagePeriod.CalculatedPriceInfo.ServiceList.Service[i];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }

        return specialOffer;
    }

    async getPackageUnitByID(UnitID, PackageUnitList) {

        var packageUnit = null;

        if (Array.isArray(PackageUnitList)) {
            if (PackageUnitList.length > 0) {
                for (const i in PackageUnitList) {
                    if (parseInt(UnitID) == parseInt(PackageUnitList[i].UnitID)) {
                        packageUnit = PackageUnitList[i];
                        break;
                    }
                }
            }
        }

        return packageUnit;
    }

    async getLocalPayment(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_LOCAL_PAYMENT, global.constants.ATTRIBUTE_LOCAL_PAYMENT, atributeGroupList);

        return value;
    }

    /**
     * 
     * @param {*} atributeGroupList 
     */
    async getEasy(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_PHYSICAL_DEMAND, global.constants.ATTRIBUTE_EASY, atributeGroupList);

        return value;
    }

    async getModerate(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_PHYSICAL_DEMAND, global.constants.ATTRIBUTE_MODERATE, atributeGroupList);

        return value;
    }

    async getMildlyChallenging(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_PHYSICAL_DEMAND, global.constants.ATTRIBUTE_MILDLY_CHALLENGING, atributeGroupList);

        return value;
    }

    async getChallenging(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_PHYSICAL_DEMAND, global.constants.ATTRIBUTE_CHALLENGING, atributeGroupList);

        return value;
    }

    async getVeryChallenging(atributeGroupList) {

        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_PHYSICAL_DEMAND, global.constants.ATTRIBUTE_VERY_CHALLENGING, atributeGroupList);

        return value;
    }

    async getBambaRecommended(atributeGroupList) {
        var value = await this.AttributeGroupLibrary.getAttribute(global.constants.GROUP_NAME_BAMBA_RECOMMENDED, global.constants.ATTRIBUTE_AUTHENTIC_BAMBA, atributeGroupList);

        return value;
    }

    async getUnitBasic(packageUnit) {
        var Unit = null;

        if (packageUnit) {
            if (Array.isArray(packageUnit)) {
                if (packageUnit.length > 0) {
                    Unit = packageUnit[0];
                }
            }
        }

        return Unit;
    }

    async getServiceBasic(packageUnit) {
        var Service = null;

        if (packageUnit) {
            if (Array.isArray(packageUnit)) {
                if (packageUnit.length > 0) {
                    if (packageUnit[0].ServiceList) {
                        if (Array.isArray(packageUnit[0].ServiceList.Service)) {
                            if (packageUnit[0].ServiceList.Service.length > 0) {
                                Service = packageUnit[0].ServiceList.Service[0];
                            }
                        }
                    }
                }
            }
        }

        return Service;
    }

    async getUnitIDBasic(packageUnit) {
        var UnitID = null;
        var Unit = await this.getUnitBasic(packageUnit);

        if(Unit) {
            UnitID = Unit.UnitID;
        }

        return UnitID;
    }

    async getServiceIDBasic(packageUnit) {
        var ServiceID = null;
        var Service = await this.getServiceBasic(packageUnit);

        if(Service) {
            ServiceID = Service.ServiceID;
        }

        return ServiceID;
    }

    async getPriceRowList(packageUnit) {
        var service = await this.getServiceBasic(packageUnit);
        var priceRowList = [];

        if(service) {
            if(service.PriceRowList) {
                if(Array.isArray(service.PriceRowList.PriceRow)) {
                    if(service.PriceRowList.PriceRow.length > 0) {
                        priceRowList = service.PriceRowList.PriceRow;
                    }
                }
            }
        }

        return priceRowList;
    }
}