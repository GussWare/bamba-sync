import SimpleTourLibrary from '../libraries/SimpleTourLibrary';
import TravelAndExtrasInterface from '../interface/travelAndExtrasInterface';
import GetPackageDetailedDescription from '../services/lemax/GetPackageDetailedDescription';
import CheckoutInterface from '../interface/checkoutInterface';
import PriceForServiceListLibrary from '../libraries/PriceForServiceListLibrary';
import AttributeGroupLibrary from '../libraries/AttributeGroupLibrary';
import LanguageHelper from '../helpers/LanguageHelper';

export default class TravelAndExtrasSimpleTourLibrary {

    constructor() {
        this.SimpleTourLibrary = new SimpleTourLibrary();
        this.PriceForServiceListLibrary = new PriceForServiceListLibrary();
        this.AttributeGroupLibrary = new AttributeGroupLibrary();
    }

    /**
     * Generate an object with the necessary information to fill the information of the travels and extras tab for when it is a simple tour
     * 
     * @param {object} params 
     * @returns {object}
     */
    async getTravelAndExtras(params) {
        const tour = await TravelAndExtrasInterface.getTravelAndExtrasInterface();
        var packageDetailed = new GetPackageDetailedDescription();

        if (params.TourID) {
            packageDetailed.PackageTourID = params.TourID;
        }

        if (params.PackageTourCode) {
            packageDetailed.PackageTourCode = params.PackageTourCode;
        }

        if (params.LanguageID) {
            packageDetailed.LanguageID = params.LanguageID;
        } else {
            packageDetailed.LanguageID = await LanguageHelper.getLang();
        }

        if (params.CurrencyID) {
            packageDetailed.CurrencyID = params.CurrencyID;
        } else {
            packageDetailed.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (params.InPriceType) {
            packageDetailed.InPriceType = params.InPriceType;
        } else {
            packageDetailed.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await packageDetailed.GetPackageDetailedDescription();
        var GetPackageDetailedDescriptionResult = response.GetPackageDetailedDescriptionResult;

        if (GetPackageDetailedDescriptionResult.Status.Code == 'OK') {
            if (typeof GetPackageDetailedDescriptionResult.PackageTour != "undefined") {
                if (GetPackageDetailedDescriptionResult.PackageTour) {

                    var tourResult = GetPackageDetailedDescriptionResult.PackageTour;
                    var Type = (tourResult.ObjectType) ? tourResult.ObjectType.ObjectTypeID : null;
                    var TourID = tourResult.ObjectID;
                    var UnitID = null;
                    var ServiceID = null;

                    tour.AccommodationObjectList = [];

                    if (tourResult.PackageUnitList) {
                        if (Array.isArray(tourResult.PackageUnitList.PackageUnit)) {
                            if (tourResult.PackageUnitList.PackageUnit.length > 0) {
                                UnitID = tourResult.PackageUnitList.PackageUnit[0].UnitID;

                                if (tourResult.PackageUnitList.PackageUnit[0].ServiceList) {
                                    if (Array.isArray(tourResult.PackageUnitList.PackageUnit[0].ServiceList.Service)) {
                                        if (tourResult.PackageUnitList.PackageUnit[0].ServiceList.Service.length > 0) {
                                            ServiceID = tourResult.PackageUnitList.PackageUnit[0].ServiceList.Service[0].ServiceID;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    tour.StartDestination = await this.SimpleTourLibrary.getStartDestination(GetPackageDetailedDescriptionResult.DestinationList);
                    tour.EndDestination = await this.SimpleTourLibrary.getEndDestination(Type, tourResult.AttributeGroupList.AttributeGroup);

                    tour.StartDestinationID = await this.SimpleTourLibrary.getStartDestinationID(GetPackageDetailedDescriptionResult.DestinationList.Destination);
                    tour.EndDestinationID = await this.SimpleTourLibrary.getEndDestinationID(tour.EndDestination);

                    if (tourResult.PackageUnitList) {
                        if (Array.isArray(tourResult.PackageUnitList.PackageUnit)) {
                            tour.AccommodationObjectList = await this.SimpleTourLibrary.getAccomodationList(tourResult.Name,tourResult.PackageUnitList.PackageUnit, true);
                            tour.AccommodationObjectList = await this.PriceForServiceListLibrary.getPriceForServiceList(tour.AccommodationObjectList, params.StartDate, params.EndDate, params.CurrencyID, global.constants.DEFAULT_PAYMENT_METHOD_ID, params.LanguageID);
                        }
                    }

                    tour.AirportPickup = await this.SimpleTourLibrary.getAirportPickupIncluded(tourResult.AttributeGroupList);
                }
            }
        }

        return tour;
    }
}