
import TourDetailModel from '../models/TourDetailModel';
import TourInterface from '../interface/tourInterface';

export default class TourDetailEntity {

    constructor() {

    }

    async getTour(params) {

        var filter = {};

        if (params.TourID) {
            filter.TourID = params.TourID;
        }

        var tour = await TourDetailModel.findOne(filter);

        return tour;
    }

    async save(tour) {

        var tourDB = await TourDetailModel.findOne({ TourID: tour.TourID });
        var tourInterface = await TourInterface.getTourInterface();

        tour = Object.assign(tourInterface, tour);

        if (tourDB) {

            tourDB.TourID = tour.TourID;
            tourDB.TourTitle = tour.TourTitle;
            tourDB.Type = tour.Type;
            tourDB.TypeName = tour.TypeName;
            tourDB.PriceInfo = tour.PriceInfo;
            tourDB.Style = tour.Style;
            tourDB.Mapa = tour.Mapa;
            tourDB.Galery = tour.Galery;
            tourDB.MainTheme = tour.MainTheme;
            tourDB.DurationDD = tour.DurationDD;
            tourDB.DurationHR = tour.DurationHR;
            tourDB.DeparturePoint = tour.DeparturePoint;
            tourDB.DepartureTimes = tour.DepartureTimes;
            tourDB.ArrivalPoint = tour.ArrivalPoint;
            tourDB.Countries = tour.Countries;
            tourDB.NumberOfCountries = tour.NumberOfCountries;
            tourDB.StartDestination = tour.StartDestination;
            tourDB.EndDestination = tour.EndDestination;
            tourDB.Themes = tour.Themes;
            tourDB.Overview = tour.Overview;
            tourDB.Highlights = tour.Highlights;
            tourDB.WhatYouGet = tour.WhatYouGet;
            tourDB.WhatIsNotIncluded = tour.WhatIsNotIncluded;
            tourDB.Itinerary = tour.Itinerary;
            tourDB.AccommodationPickupIncluded = tour.AccommodationPickupIncluded;
            tourDB.AccomodationDropOffIncluded = tour.AccomodationDropOffIncluded;
            tourDB.LocalPayment = tour.LocalPayment;
            tourDB.UnitID = tour.UnitID;
            tourDB.ServiceID = tour.ServiceID;
            tourDB.AccommodationObjectList = tour.AccommodationObjectList;
            tourDB.ListNotStartDays = tour.ListNotStartDays;
            tourDB.Easy = tour.Easy;
            tourDB.Moderate = tour.Moderate;
            tourDB.MildlyChallenging = tour.MildlyChallenging;
            tourDB.Challenging = tour.Challenging;
            tourDB.VeryChallenging = tour.VeryChallenging;
            tourDB.NoteAdditionalInformation = tour.NoteAdditionalInformation;
            tourDB.NoteImportantInformation = tour.NoteImportantInformation;
            tourDB.NoteCoupon = tour.NoteCoupon;
            tourDB.NoteDiscount = tour.NoteDiscount;
            tourDB.LanguageID = tour.LanguageID;

            return await tourDB.save();

        } else {

            tourDB = new TourDetailModel();
            tourDB.TourID = tour.TourID;
            tourDB.TourTitle = tour.TourTitle;
            tourDB.Type = tour.Type;
            tourDB.TypeName = tour.TypeName;
            tourDB.PriceInfo = tour.PriceInfo;
            tourDB.Style = tour.Style;
            tourDB.Mapa = tour.Mapa;
            tourDB.Galery = tour.Galery;
            tourDB.MainTheme = tour.MainTheme;
            tourDB.DurationDD = tour.DurationDD;
            tourDB.DurationHR = tour.DurationHR;
            tourDB.DeparturePoint = tour.DeparturePoint;
            tourDB.DepartureTimes = tour.DepartureTimes;
            tourDB.ArrivalPoint = tour.ArrivalPoint;
            tourDB.Countries = tour.Countries;
            tourDB.NumberOfCountries = tour.NumberOfCountries;
            tourDB.StartDestination = tour.StartDestination;
            tourDB.EndDestination = tour.EndDestination;
            tourDB.Themes = tour.Themes;
            tourDB.Overview = tour.Overview;
            tourDB.Highlights = tour.Highlights;
            tourDB.WhatYouGet = tour.WhatYouGet;
            tourDB.WhatIsNotIncluded = tour.WhatIsNotIncluded;
            tourDB.Itinerary = tour.Itinerary;
            tourDB.AccommodationPickupIncluded = tour.AccommodationPickupIncluded;
            tourDB.AccomodationDropOffIncluded = tour.AccomodationDropOffIncluded;
            tourDB.LocalPayment = tour.LocalPayment;
            tourDB.UnitID = tour.UnitID;
            tourDB.ServiceID = tour.ServiceID;
            tourDB.AccommodationObjectList = tour.AccommodationObjectList;
            tourDB.ListNotStartDays = tour.ListNotStartDays;
            tourDB.Easy = tour.Easy;
            tourDB.Moderate = tour.Moderate;
            tourDB.MildlyChallenging = tour.MildlyChallenging;
            tourDB.Challenging = tour.Challenging;
            tourDB.VeryChallenging = tour.VeryChallenging;
            tourDB.NoteAdditionalInformation = tour.NoteAdditionalInformation;
            tourDB.NoteImportantInformation = tour.NoteImportantInformation;
            tourDB.NoteCoupon = tour.NoteCoupon;
            tourDB.NoteDiscount = tour.NoteDiscount;
            tourDB.LanguageID = tour.LanguageID;

            return await tourDB.save();
        }
    }
}