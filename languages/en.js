'use strict'

module.exports = {

    ERROR_ID_NOT_VALID: "IDENTIFIER NOT VALID",
    ERROR_CONNECTION_KEYSTONE: "Keystone connection error",
    ERROR_FILE_NOT_FOUND: "Error file not found",

    ERROR_MESSAGE_INSERT_COUNTRIES: 'Error registering countries',
    ERROR_MESSAGE_GENERIC: 'There was an error processing the request, please try again, if the problem persists notify the administrator.',

    VALIDATOR_MESSAGE_REQUIRED: "The '{field}' field is required!",
    VALIDATOR_MESSAGE_STRING: "The '{field}' field must be a string!",
    VALIDATOR_MESSAGE_STRING_EMPTY: "The '{field}' field must not be empty!",
    VALIDATOR_MESSAGE_STRING_MIN: "The '{field}' field length must be larger than or equal to {expected} characters long!",
    VALIDATOR_MESSAGE_STRING_MAX: "The '{field}' field length must be less than or equal to {expected} characters long!",
    VALIDATOR_MESSAGE_STRING_LENGTH: "The '{field}' field length must be {expected} characters long!",
    VALIDATOR_MESSAGE_STRING_PATTERN: "The '{field}' field fails to match the required pattern!",
    VALIDATOR_MESSAGE_STRING_CONTAIN: "The '{field}' field must contain the '{expected}' text!",
    VALIDATOR_MESSAGE_STRING_ENUM: "The '{field}' field does not match  any of the allowed values!",
    VALIDATOR_MESSAGE_NUMBER: "The '{field}' field must be a number!",
    VALIDATOR_MESSAGE_NUMBER_MIN: "The '{field}' field must be larger than or equal to {expected}!",
    VALIDATOR_MESSAGE_NUMBER_MAX: "The '{field}' field must be less than or equal to {expected}!",
    VALIDATOR_MESSAGE_NUMBER_EQUAL: "The '{field}' field must be equal with {expected}!",
    VALIDATOR_MESSAGE_NUMBER_NOT_EQUAL: "The '{field}' field can't be equal with {expected}!",
    VALIDATOR_MESSAGE_NUMBER_INTEGER: "The '{field}' field must be an integer!",
    VALIDATOR_MESSAGE_NUMBER_POSITIVE: "The '{field}' field must be a positive number!",
    VALIDATOR_MESSAGE_NUMBER_NEGATIVE: "The '{field}' field must be a negative number!",
    VALIDATOR_MESSAGE_ARRAY: "The '{field}' field must be an array!",
    VALIDATOR_MESSAGE_ARRAY_EMPTY: "The '{field}' field must not be an empty array!",
    VALIDATOR_MESSAGE_ARRAY_MIN: "The '{field}' field must contain at least {expected} items!",
    VALIDATOR_MESSAGE_ARRAY_MAX: "The '{field}' field must contain less than or equal to {expected} items!",
    VALIDATOR_MESSAGE_ARRAY_LENGTH: "The '{field}' field must contain {expected} items!",
    VALIDATOR_MESSAGE_ARRAY_CONTAINS: "The '{field}' field must contain the '{expected}' item!",
    VALIDATOR_MESSAGE_ARRAY_ENUM: "The '{field} field value '{expected}' does not match any of the allowed values!",
    VALIDATOR_MESSAGE_BOOLEAN: "The '{field}' field must be a boolean!",
    VALIDATOR_MESSAGE_FUNCTION: "The '{field}' field must be a function!",
    VALIDATOR_MESSAGE_DATE: "The '{field}' field must be a Date!",
    VALIDATOR_MESSAGE_DATE_MIN: "The '{field}' field must be larger than or equal to {expected}!",
    VALIDATOR_MESSAGE_DATE_MAX: "The '{field}' field must be less than or equal to {expected}!",
    VALIDATOR_MESSAGE_FORBIDDEN: "The '{field}' field is forbidden!",
    VALIDATOR_MESSAGE_EMAIL: "The '{field}' field must be a valid e-mail!",


    VALIDATOR_MESSAGE_ARRAY_NUMBER_INTIGER: 'Identifiers not valid',
    COUNTRIES_ADDED_SUCCESSFULLY: 'The countries were added successfully',
    SEO_ADDED_SUCCESSFULLY: 'The data were added successfully',

    TXT_GUIDE_SUBTITLE_DEFAULT: 'Some of the activities',
    TXT_ACCOMODATION_SUBTITLE_DEFAULT: 'Choose from Hostel, 3 Star or 4 Star Hotels',
    TXT_ACTIVITIES_SUBTITLE_DEFAULT: 'Texto por definir',


    CHECKOUT_ERROR_PASSENGER_REQUIRED: "The field 'Passengers' is required",
    CHECKOUT_ERROR_RESERVATIONS_REQUIRED: "The field 'Reservations' is required",
    CHECKOUT_ERROR_ROOM_OCUPATE_RULE_LIST_REQUIRED: "The field 'RoomOccupateRuleList' is required",
    CHECKOUT_ERROR_NUMBER_OF_PERSON_EXCEEDS: "The number of people exceeds the allowed",

    COUPON_AND_DISCOUNT_INVALID_COUPON: "The coupon was not found",
    COUPON_AND_DISCOUNT_INVALID_DATES_PURSHASE:"Invalid purchase dates",
    COUPON_AND_DISCOUNT_INVALID_DATES_TRAVEL:"Invalid travel dates. Please try again",
    COUPON_AND_DISCOUNT_INVALID_COUPON_NAME:"Invalid coupon name. Please try again",

    STATIC_DATA_NO_TOUR: "Tour no encontrado",
    BAMBA_SYNC_BAMBA_NO_TOURS: "No hay tours",
};