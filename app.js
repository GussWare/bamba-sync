import express from 'express';
import bodyParser from 'body-parser';
import TestController from './controllers/v1/TestController';
import StaticDataController from './controllers/v1/StaticDataController';
import TransactionController from './controllers/v1/TransactionController';
import TourDetailController from './controllers/v1/TourDetailController';
import path from 'path';

// parser data with bodyparser
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// config headers http
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Autorization, X-API-KEY, Origin, X-Request-Width, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
});

// routes   
app.use('/api', TestController);
app.use('/api', StaticDataController);
app.use('/api', TransactionController);
app.use('/api', TourDetailController);

module.exports = app;