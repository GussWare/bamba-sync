import sys
import os
import requests
import constants
import time

from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime, date, time, timedelta

from TransactionModel import TransactionModel
from TransactionLogModel import TransactionLogModel


def getToday():
    today = datetime.today()

    return today 


if __name__ == "__main__":

    # conexion con la base de datos
    BambaSyncDB = MongoClient('mongodb://127.0.0.1:27017/bamba-sync')
    db = BambaSyncDB["bamba-sync"]

    # Collecciones a trabajar
    transactionCollection = db.transactions
    transactionLogCollection = db.logs

    # Iniciamos la transaccion, agregamos el primer log
    today = getToday()

    transactionData = TransactionModel(TransactionID=None, StartDate=today, EndDate=None, Message='Inicia el proceso de Transacción',
                                       Estatus=constants.TRANSACTION_STATUS_INIT, NoTour=0, NoTourSuccess=0, NoTourError=0)

    print(transactionData.toDBCollection())

    transactionID = transactionCollection.insert_one(
        transactionData.toDBCollection()).inserted_id

    # Realizamos la peticion a la api SeoData
    urlSeoData = "http://localhost:3980/api/v1/get-seo-data"
    response = requests.get(urlSeoData)

    if response.status_code == 200:
        tourList = response.json()
        if tourList:

            # Actualizamos el estatus de la transaccion a en proceso
            transactionData = TransactionModel(TransactionID=transactionID,
                                               Message='Seo Data si retorna tours y se procede a guardar', Estatus=constants.TRANSACTION_STATUS_PROCESS)

            transactionCollection.update_one({"_id": transactionID}, {
                                             "$set": transactionData.toDBCollection()})

            urlSyncTourDetail = "http://localhost:3990/api/v1/sync-tour-detail"

            # Procedemos a sync cada tour
            totalTours = len(tourList)
            toursErrors = 0
            toursSuccess = 0
            contador = 0

            for tour in tourList:
                todayStart = getToday()

                args = {
                    "TourList": [{
                        "TourID": tour["lemaxId"],
                        "Type": tour["typeId"]
                    }],
                    "LanguageID": "en",
                    "CurrencyID": "840"
                }

                response = requests.post(urlSyncTourDetail, json=args)

                todayEnd = getToday()

                if response.status_code == 200:
                    tourResponse = response.json()

                    if tourResponse:
                        transactionLog = TransactionLogModel(TransactionID=transactionID, Peticion=args, Respuesta=response.status_code,
                                                             StartDate=todayStart, EndDate=todayEnd, TourID=tour["lemaxId"], Type=tour["typeId"], LanguageID=tour["languageId"], TourTitle=tour["title"], Estatus=constants.TRANSACTION_STATUS_SUCCESS, Message='Se guardo con exito')

                        transactionLogCollection.insert_one(
                            transactionLog.toDBCollection()).inserted_id

                        toursSuccess += 1
                    else:
                        transactionLog = TransactionLogModel(TransactionID=transactionID, Peticion=args, Respuesta=response.status_code,
                                                             StartDate=todayStart, EndDate=todayEnd, TourID=tour["lemaxId"], Type=tour["typeId"], LanguageID=tour["languageId"], TourTitle=tour["title"], Estatus=constants.TRANSACTION_STATUS_WARNING, Message='No hay error en la petición pero api no regreso datos')

                        transactionLogCollection.insert_one(
                            transactionLog.toDBCollection()).inserted_id

                        toursErrors += 1
                else:
                    transactionLog = TransactionLogModel(TransactionID=transactionID, Peticion=args, Respuesta=response.status_code,
                                                         StartDate=todayStart, EndDate=todayEnd, TourID=tour["lemaxId"], Type=tour["typeId"], LanguageID=tour["languageId"], TourTitle=tour["title"], Estatus=constants.TRANSACTION_STATUS_ERROR, Message='Api retorno una exception', Exceptionn=response.json())
                    transactionLogCollection.insert_one(
                        transactionLog.toDBCollection()).inserted_id

                    toursErrors += 1

                contador += 1

            # Verificamos si se terminaron de procesar todos los tours
            if contador >= totalTours:
                if toursErrors > 0:
                    transactionData = TransactionModel(
                        TransactionID=transactionID, EndDate=getToday(), Message='Se completo la transacción pero con errores', Estatus=constants.TRANSACTION_STATUS_ERROR, NoTour=totalTours, NoTourSuccess=toursSuccess, NoTourError=toursErrors)
                elif toursSuccess == totalTours:
                    transactionData = TransactionModel(
                        TransactionID=transactionID, EndDate=getToday(), Message='Se completo la transacción, todos los tours se sincronizaron correctamente', Estatus=constants.TRANSACTION_STATUS_SUCCESS, NoTour=totalTours, NoTourSuccess=toursSuccess, NoTourError=toursErrors)
                else:
                    transactionData = TransactionModel(
                        TransactionID=transactionID, EndDate=getToday(), Message='Se completo la transacción, Verificar Log', Estatus=constants.TRANSACTION_STATUS_COMPLETED, NoTour=totalTours, NoTourSuccess=toursSuccess, NoTourError=toursErrors)

                transactionCollection.update_one({"_id": transactionID}, {
                                                 "$set": transactionData.toDBCollection()})

                # Realizamos la peticion a la api para sincronizar los datos de la base de datos bamba-sync a bamba 
                urlSeoData = "http://localhost:3980/api/v1/tour-details-bamba-sync"
                response = requests.get(urlSeoData)
        else:
            transactionData = TransactionModel(TransactionID=transactionID, EndDate=getToday(),
                                               Message='Se realizo la peticion a seo data pero retorno lista vacia', Estatus=constants.TRANSACTION_STATUS_SUCCESS)

            transactionCollection.update_one({"_id": transactionID}, {
                                             "$set": transactionData.toDBCollection()})
        pass
    else:
        transactionData = TransactionModel(TransactionID=transactionID, EndDate=getToday(),
                                           Message='Error al llamar al api SeoData', Estatus=constants.TRANSACTION_STATUS_ERROR)

        transactionCollection.update_one({"_id": transactionID}, {
                                         "$set": transactionData.toDBCollection()})
    pass
