
class TransactionModel:

    def __init__(self, TransactionID=None, StartDate=None, EndDate=None, Message=None, Estatus=None, NoTour=None, NoTourSuccess=None, NoTourWarning=None, NoTourError=None):
        self.TransactionID = TransactionID
        self.StartDate = StartDate
        self.EndDate = EndDate
        self.Message = Message
        self.Estatus = Estatus
        self.NoTour = NoTour
        self.NoTourSuccess = NoTourSuccess
        self.NoTourWarning = NoTourWarning
        self.NoTourError = NoTourError

    def toDBCollection(self):

        data = {}

        if self.TransactionID is not None:
            if self.StartDate is not None:
                data["StartDate"] = self.StartDate
            
            if self.EndDate is not None:
                data["EndDate"] = self.EndDate
                
            if self.Message is not None:
                data["Message"] = self.Message
                
            if self.Estatus is not None:
                data["Estatus"] = self.Estatus
                
            if self.NoTour is not None:
                data["NoTour"] = self.NoTour
                
            if self.NoTourSuccess is not None:
                data["NoTourSuccess"] = self.NoTourSuccess

            if self.NoTourWarning is not None:
                data["NoTourWarning"] = self.NoTourWarning
                
            if self.NoTourError is not None:
                data["NoTourError"] = self.NoTourError            
        else:
             data["StartDate"] = self.StartDate
             data["EndDate"] = self.EndDate
             data["Message"] = self.Message
             data["Estatus"] = self.Estatus
             data["NoTour"] = self.NoTour
             data["NoTourSuccess"] = self.NoTourSuccess
             data["NoTourWarning"] = self.NoTourWarning
             data["NoTourError"] = self.NoTourError

        return data

    def toString(self):
        print("Debde entrar aqui")
        print(self.StartDate)
        print(self.EndDate)
        print(self.Message)
        print(self.Estatus)
        print(self.NoTour)
        print(self.NoTourSuccess)
        print(self.NoTourWarning)
        print(self.NoTourError)
        print("Y hasta aca")
        pass
