'use strict'

exports.getCheckoutInterface = async () => {

    var customizeInterface = {
        TourID: null,
        TourTitle: null,
        Type: null,
        TypeName: null,
        PriceInfo: null,
        DurationDD: null,
        Style: null,
        Countries: null,
        NumberOfCountries: null,
        UnitID: null,
        ServiceID: null,
        AccommodationObjectList: [],
        ListNotStartDays: null,
        AccommodationPickupIncluded:null,
        AccommodationDropOffIncluded:null
    };

    return customizeInterface;
}


exports.getAccomodationItemInterface = async () => {
    var itemAccomodationInterface = {
        UnitID: null,
        UnitName: null,
        AvailabilityStatus:null,
        ServiceID:null,
        ServiceName:null,
        ComponentName:null,
        Description: null,
        RoomOccupateRuleList:[],
        AdditionalServicesList: []
    };

    return itemAccomodationInterface;
}

exports.getServiceListInterface = async () => {

    var serviceListInterface = {
        ServiceID:null,
        ServiceName: null,
        ServiceType: null,
        Price: null,
        PriceFormated: null,
        PriceType: null
    };

    return serviceListInterface;
} 