'use strict'

exports.geReservationInterface = async () => {

    var reservationItem = {
        ReservationID:null,
        Status: null,
        SellingPrice: null,
        EndReservationURL:null,
        Customer:null,
        ReservationItems: {
            InsertReservationItemRS:[]
        }
    };

    return reservationItem;
};

exports.geReservationItemInterface = async () => {
    var reservationItem = {
        ReservationItemOrder: null,
        SellingPrice: null,
        NetPrice: null,
        Commission:null,
        ReservationItemDetailsList:null,
        PriceCalculationStatus:null,
        CancellationData:null,
        StartDate:null,
        EndDate:null,
        StartDateTime:null,
        EndDateTime:null
    };

    return reservationItem;
}

exports.geDocumentsInterface = async () => {

    var documents = {
        Invoice:[],
        Itinerary:[]
    }

    return documents;
}