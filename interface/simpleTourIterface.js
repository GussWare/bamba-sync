'use strict'

exports.getSimpleTourInterface = async () => {

    var simpleTour = {
        ID: null,
        TourID:null,
        TourTitle:null,
        Type:null,
        StartDate:null,
        EndDate:null,
        Duration:null,
        StartDay: null,
        EndDay: null,
        Day:null,
        Itinerary: []
    };

    return simpleTour;
};

exports.getSimpletourItineraryInterface = async () => {

    var simleToutInterface = {
        title:null,
        shortDescription:[],
        longDescription:[]
    };

    return simleToutInterface;
}