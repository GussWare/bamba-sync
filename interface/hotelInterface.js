'use strict'

exports.getHotelItemInterface = async () => {

    var hotelItemInterface = {
        ID: null,
        Name: null,
        Destination: null,
        Stars:null,
        Priority:null,
        UnitMinimumPriceInfo:null,
        UnitList: []
    };


    return hotelItemInterface;
};

exports.getServiceItemInterface = async () => {
    var ServiceItem = {
        UnitID:null,
        ServiceID:null,
        BillingType:null,
        Capacity:null,
        Description:null,
        Price:null
    };

    return ServiceItem;
}