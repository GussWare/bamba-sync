'use strict'

exports.getAccomodationInterface = async () => {

    var accmonodation = {
        ID: null,
        ServiceType: null,
        StartDate:null,
        EndDate:null,
        StartDay: null,
        EndDay:null,
        Duration:null,
        Day:null,
        Name: null,
        LongDescription: null,
        Country: null,
        DestinationID:null,
        Destination: null,
        NumberOfStars: null,
        listStars:null
    };

    return accmonodation;
};