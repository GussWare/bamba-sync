'use strict'

exports.getTransportListInterface = async () => {

    var tranportList = {
        TransportID: null,
        Name: null,
        UnitID:null,
        ServiceID:null,
        AvailabilityStatus:null,
        Capacity:null,
        CalculatePriceInfo:null
    };

    return tranportList;
};