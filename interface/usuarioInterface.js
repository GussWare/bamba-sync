'use strict'
exports.getLoginInterface = async () => {
    var login = {
        Status:null,
        Token:null
    }

    return login;
};

exports.getUsuarioInterface = async () => {

    var usuarioInterface = {
        sub:null,
        iat:null,
        exp:null,

        UserID: null,
        Email: null,
        Name:null,
        Surname:null,
        Telephone:null,
        Fax:null,
        LanguageID: null,
        UserRolesIDsList: {
            unsignedByte: []
        },
        Customer: {
            CustomerID: null,
            IsCustomer: null,
            IsSupplier: null,
            IsPartner: null,
            IsLoyaltyMember: null,
            LanguageID: null,
            CustomerType: null,
            TelephoneNumber2:null,
            Email: null,
            CompanyName:null,
            TaxPayerType: 0,
            BirthDate: null,
            CountryID: null,
            CitizenshipID: null,
            Sex: null,
            ContractType: null,
            LogEntries: null,
            PointPerCurrency: null,
            CreatedDate: null,
            ModifiedDate: null,
            OtherSystemID: null,
            listCustomField: null
        }
    };

    return usuarioInterface;
};