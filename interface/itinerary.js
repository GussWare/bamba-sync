'use strict'

/**
 * 
 */
exports.getLevelItineraryInterface = async () => {
    var itinerary = {
        TotalDestination: 0,
        Itinerary: []
    };

    return itinerary;
}

/**
 * 
 */
exports.getLevelContryInterfeace = async () => {

    var itinerary = {
        Country: null,
        StartDay: null,
        EndDay: null,
        Duration: null,
        Itinerary: []
    }

    return itinerary;
};

/**
 * 
 */
exports.getLevelDestinationInterface = async () => {

    var itinerary = {
        Destination: null,
        StartDay: null,
        EndDay: null,
        Duration: null,
        Itinerary: []
    }

    return itinerary;
};