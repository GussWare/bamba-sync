'use strict'

exports.getTourInterface = async () => {
        var tour = {
                TourID: null,
                TourTitle: null,
                Type: null,
                TypeName: null,
                PriceInfo: null,
                Style: null,
                Mapa: null,
                Galery: null,
                MainTheme: null,
                DurationDD: null,
                DurationHR: null,
                DeparturePoint: null,
                DepartureTimes: null,
                ArrivalPoint: null,
                Countries: null,
                NumberOfCountries: null,
                StartDestination: null,
                EndDestination: null,
                Themes: null,
                Overview: null,
                Highlights: null,
                WhatYouGet: null,
                WhatIsNotIncluded: null,
                Itinerary: {
                        TotalDestination: null,
                        Itinerary: []
                },
                AccommodationPickupIncluded:null,
                AccommodationDropOffIncluded:null,
                LocalPayment:null,
                UnitID:null,
                ServiceID:null,
                AccommodationObjectList:null,
                ListNotStartDays:[],
                Easy:null,
                Moderate:null,
                MildlyChallenging:null,
                Challenging:null,
                VeryChallenging:null,

                NoteAdditionalInformation:null,
                NoteImportantInformation:null,
                NoteCoupon:null,
                NoteDiscount:null,
                LanguageID:null,
                BambaRecommendedTrip:null
        }

        return tour;
}

