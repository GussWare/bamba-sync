'use strict'

exports.getTransportInterface = async () => {

    var transport = {
        ID: null,
        StartDate:null,
        EndDate:null,
        StartDay: null,
        EndDay: null,
        Duration: null,
        Day:null,
        TypeOfService:null,
        TypeOfServiceName:null,
        Name: null,
        Country:null,
        DestinationID:null,
        Destination:null,
        DeparturePoint: null,
        ArrivalPoint: null,
        PickupIncluded:null,
        DropoffIncluded:null,
        UserSelectDeapertureTime: null,
        ShortDescription:null,
        DepartureTimes: null,
        DurationHR:null
        
    };

    return transport;
};