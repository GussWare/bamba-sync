'use strict'

/**
 * 
 */
exports.getSuccessInterface = async () => {
    var success = {
        success: false,
        data: {},
        messages: []
    };

    return success;
}