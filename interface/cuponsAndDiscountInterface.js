'use strict'

exports.getDiscountInterface = async () => {

    var discount = {
        Amount:null,
        Percent:null,
        Total:null
    };

    return discount;
};

exports.getTourInterface = async () => {
    var discount = {
        noteDiscount: null,
        noteCoupon:null,
        availabilityStatus:null
    };

    return discount;
};

exports.getSpecialOfferInterface = async () => {
    var discount = {
        Amount: null,
        Discount:null,
        Total:null
    };

    return discount;
};
