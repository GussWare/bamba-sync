'use strict'

exports.getShopInterface = async () => {

    var shop = {
        PackageSearchResults: [],
        Currency: {},
        Pagination: {
            CurrentPage: '',
            PageSize: '',
            TotalNumberOfResults: ''
        }
    };

    return shop;
};

exports.getShopItemInterface = async () => {

    var shop = {
        TourID: '',
        TourTitle: '',
        Type: '',
        TypeName: '',
        UnitID: null,
        MainTheme: [],
        Countries: [],
        NumberOfCountries: 0,
        DurationDD: null,
        DurationHR: null,
        Style: [],
        PriceInfo: {},
        stars: null
    };

    return shop;
};

exports.getPriceInfoInterface = async () => {

    var PriceInfo = {
        Discount:null,
        SpecialOffer:null,
        PriceOriginal:null,
        PriceOriginalFormatted:null,
        Price: null,
        PriceFormatted: null,
        BillingTypeName: ""
    };

    return PriceInfo;
};