'use strict'

exports.getTravelAndExtrasInterface = async () => {

    var travelAndExtras = {
        StartDestination:null,
        EndDestination:null,
        StartDestinationID:null,
        EndDestinationID:null,
        AccommodationObjectList:null,
        AirportPickup:null,
        TransportationList:null
    };

    return travelAndExtras;
};