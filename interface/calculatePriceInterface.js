'use strict'

exports.getCalculatePriceInterface = async () => {

    var calculatePrice = {
        AvailabilityStatus:null,
        PriceOriginal:null,
        PriceOriginalFormated:null,
        TotalPrice:null,
        PriceFormated:null,
        NetPrice:null,
        Discount:null,
        Coupon:null,
        SpecialOffer:null,
        Messages:[]
    };

    return calculatePrice;
};