'use strict'

exports.getActivityInterface = async () => {

    var activity = {
        ID: null,
        Type:null,
        StartDate:null,
        EndDate:null,
        StartDay: null,
        EndDay: null,
        Duration:null,
        Day:null,
        Name: null,
        ShortDescription: null,
        Country: null,
        DestinationID:null,
        Destination: null
    };

    return activity;
};

exports.getActivityFakeInterface = async () => {

    var activity = {
        ID: null,
        Type:null,
        StartDate:null,
        EndDate:null,
        StartDay: null,
        EndDay: null,
        Duration:null,
        Day:null,
        Name: null,
        ShortDescription: null,
        Country: null,
        DestinationID:null,
        Destination: null
    };

    return activity;
};