'use strict'

const langHelper = require('../helpers/LanguageHelper');

exports.load = async (req, res, next) => {

    var params = req.body,
        lang = global.config.lang.default
        ;

    global.lang = lang;
    global.translation = await langHelper.load(lang, global.config.lang.path);

    next();
};