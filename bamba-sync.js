import mongoose from 'mongoose';
import app from './app';
import Config from './config/config';
import Constants from './config/constants';

const port = process.env.port || 3990;

// config global
global.config = Config;
global.constants = Constants;

//const mongoDB = 'mongodb://' + global.config.database.user + ':' + global.config.database.password + '@' + global.config.database.host + ':' + global.config.database.port + '/' + global.config.database.name + '?authSource=admin';
const mongoDB = 'mongodb://' + global.config.database.host + ':' + global.config.database.port + '/' + global.config.database.name;

console.log(mongoDB);

mongoose.connect(mongoDB, { useNewUrlParser: true }, (err, res) => {
    if (err) {
        throw err;
    } else {
        app.listen(port, () => {
            console.log('Servidor escuchando en ' + global.config.base_url + ':' + port);
        });
    }
});

mongoose.set('useCreateIndex', true);