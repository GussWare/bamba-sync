import soap from 'soap';
import LangHelper from '../../helpers/LanguageHelper';

export default class GetPackageDetailedDescription {

    constructor() {
        this.PackageTourID = null;
        this.PackageTourCode = null;
        this.NumberOfPersons = null;
        this.ChildrenAgeList = null;
        this.InPriceType = null;
        this.LanguageID = null;
        this.CurrencyID = null;
        this.OutParameterList = null;
    }

    async getInt(arr) {

        return {
            int: arr
        }
    }

    GetPackageDetailedDescription() {

        return new Promise((resolve, reject) => {

            var params = {};

            if (this.PackageTourID) {
                params.PackageTourID = this.PackageTourID;
            }

            if (this.PackageTourCode) {
                params.PackageTourCode = this.PackageTourCode;
            }

            if (this.NumberOfPersons) {
                params.NumberOfPersons = this.NumberOfPersons;
            }

            if (this.ChildrenAgeList) {
                params.ChildrenAgeList = this.getInt(this.ChildrenAgeList);
            }

            if(this.CurrencyID) {
                params.CurrencyID = this.CurrencyID;
            }

            if (this.InPriceType) {
                params.InPriceType = this.InPriceType;
            }

            var args = {
                getPackageDetailedDescriptionParameters: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetPackageDetailedDescription(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}