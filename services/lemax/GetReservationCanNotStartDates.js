import soap from 'soap';

export default class GetReservationCanNotStartDates {

    constructor() {
        this.UnitID = null;
        this.StartDate = null;
        this.EndDate = null;
    }

    GetReservationCanNotStartDates() {
        return new Promise((resolve, reject) => {

            var params = {};

            if(this.UnitID) {
                params.UnitID = this.UnitID;
            }

            if(this.StartDate) {
                params.StartDate = this.StartDate;
            }

            if(this.EndDate) {
                params.EndDate = this.EndDate;
            }

            var args = {
                request: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetReservationCanNotStartDates(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}