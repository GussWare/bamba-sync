import soap from 'soap';

export default class GetAllSEOData {
    
    constructor() {

    }

    GetAllSEOData() {
        return new Promise((resolve, reject) => {

            var params = {};

            var args = {
                getAllSeoDataParameters: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetAllSeoData(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}