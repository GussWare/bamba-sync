import soap from 'soap';
import LangHelper from '../../helpers/LanguageHelper';

export default class GetSearchFields {

    constructor() {
        this.LanguageID = null;
    }

    GetSearchFields() {

        return new Promise((resolve, reject) => {

            var params = {};

            if(this.LanguageID) {
                params.LanguageID = this.LanguageID;
            }

            var args = {
                getSearchFieldsParameters: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetSearchFields (args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}