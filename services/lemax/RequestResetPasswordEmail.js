import soap from 'soap';

export default class RequestResetPasswordEmail {

    constructor() {
        this.Username = null;
    }

    RequestResetPasswordEmail() {
        return new Promise((resolve, reject) => {
            var params = {};

            if(this.Username) {
                params.Username = this.Username;
            }

            var args = {
                request: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.RequestResetPasswordEmail(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}