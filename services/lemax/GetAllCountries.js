import soap from 'soap';

export default class GetAllCountries {
    
    constructor() {

    }

    GetAllCountries() {
        return new Promise((resolve, reject) => {

            var params = {};

            if (this.LanguageID) {
                params.LanguageID = this.LanguageID;
            }

            if (this.SelectedCountryID) {
                params.SelectedCountryID = this.SelectedCountryID;
            }

            var args = {
                getAllCountriesParameter: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetAllCountries(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}