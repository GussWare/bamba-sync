import soap from 'soap';
import LangHelper from '../../helpers/LanguageHelper';

export default class GetAllDestinations {

    constructor() {
        this.LanguageID = null;
        this.SelectedCountryID = null;
    }

    GetAllDestinations() {

        return new Promise((resolve, reject) => {

            var params = {};

            if (this.LanguageID) {
                params.LanguageID = this.LanguageID;
            }

            if (this.SelectedCountryID) {
                params.SelectedCountryID = this.SelectedCountryID;
            }

            var args = {
                getAllDestinationsParameter: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetAllDestinations(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}