import soap from 'soap';

export default class GetReservation {

    constructor() {
        this.ReservationID = null;
        this.LanguageID = null;
    }

    GetReservation() {
        return new Promise((resolve, reject) => {
            var params = {};

            if(this.ReservationID) {
                params.ReservationID = this.ReservationID;
            }

            if(this.LanguageID) {
                params.LanguageID = this.LanguageID;
            }

            var args = {
                getReservationParameters: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetReservation(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}