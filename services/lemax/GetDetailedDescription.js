import soap from 'soap';
import LangHelper from '../../helpers/LanguageHelper';

export default class GetDetailedDescription {

    constructor() {
        this.ObjectID = null;
        this.InPriceType = null;
        this.LanguageID = null;
        this.CurrencyID = null;
    }

    GetDetailedDescription() {

        return new Promise((resolve, reject) => {

            var params = {};

            if (this.ObjectID) {
                params.ObjectID = this.ObjectID;
            }

            if(this.CurrencyID) {
                params.CurrencyID = this.CurrencyID;
            }

            if (this.InPriceType) {
                params.InPriceType = this.InPriceType;
            }

            var args = {
                getDetailedDescriptionParameters: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetDetailedDescription(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}