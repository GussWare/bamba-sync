import soap from 'soap';

export default class SetUserResetPassword {

    constructor() {
        this.ResetPasswordToken = null;
        this.NewPassword = null;
    }

    SetUserResetPassword() {
        return new Promise((resolve, reject) => {
            var params = {};

            if(this.ResetPasswordToken) {
                params.ResetPasswordToken = this.ResetPasswordToken;
            }

            if(this.NewPassword) {
                params.NewPassword = this.NewPassword;
            }

            var args = {
                request: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.SetUserResetPassword(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}