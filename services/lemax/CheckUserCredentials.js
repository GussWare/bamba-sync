import soap from 'soap';

export default class CheckUserCredentials {

    constructor() {
        this.Email = null;
        this.Password = null;
    }

    CheckUserCredentials() {
        return new Promise((resolve, reject) => {
            var params = {};

            if(this.Email) {
                params.Email = this.Email;
            }

            if(this.Password) {
                params.Password = this.Password;
            }

            var args = {
                checkUserCredentialsRequest: params
            };

            console.log(JSON.stringify(params));

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.CheckUserCredentials(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}