import soap from 'soap';
import XMLElementsApi from '../lemax/XMLElementsApi';

export default class GetSearchResults {

    constructor() {
        this.ObjectIDList = null;
        this.CategoryIDListIntersection = null;
        this.PriceFrom = null;
        this.PriceTo = null;
        this.StartDate = null;
        this.EndDate = null;
        this.LanguageID = null;
        this.FilterType = null;
        this.InPriceType = null;
        this.ThemeIDList = [];
        this.StyleIDList = [];
        this.PassengerConfigurationFilterList = null;
        this.DestinationIDList = null;
        this.CountryIDList = [];
        this.SortBy = null;
        this.SortOrder = null;
        this.PageSize = null;
        this.CurrentPage = null;
        this.CurrencyID = null;
        this.BambaRecommendedTrip = null;
        this.OnlyOnSpecialOffer = null;
        this.IgnorePriceAndAvailability = null;

        this.ShowFileds = [
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "GetObjectInfoForAllResults",
                NumberOfResults: 100
            }
        ];

        this.XMLElementsApi = new XMLElementsApi();
    }

    GetSearchResults() {
        return new Promise(async (resolve, reject) => {
            var params = {};

            // filtros
            if (this.ObjectIDList) {
                params.ObjectIDList = await this.XMLElementsApi.getInt(this.ObjectIDList);
            }

            // Filtro por precio
            if (this.PriceFrom > 0 && this.PriceTo > 0) {
                params.PriceFrom = this.PriceFrom;
                params.PriceTo = this.PriceTo;
            }

            if (this.StyleIDList.length > 0) {
                params.CategoryIDListIntersection = await this.XMLElementsApi.getInt(this.StyleIDList);
            }

            // Filtro por themas
            if (this.ThemeIDList.length > 0) {
                if (typeof params.ObjectAttributeFilterList == "undefined") {
                    params.ObjectAttributeFilterList = [];
                }

                for (const i in this.ThemeIDList) {
                    params.ObjectAttributeFilterList.push(await this.XMLElementsApi.getAttrFilterByValue(this.ThemeIDList[i], 1, global.constants.FILTER_COMPARASION_TYPE_EQUALS));
                }
            }

            if (this.BambaRecommendedTrip) {
                if (typeof params.ObjectAttributeFilterList == "undefined") {
                    params.ObjectAttributeFilterList = [];
                }

                params.ObjectAttributeFilterList.push(await this.XMLElementsApi.getAttrFilterByValue(global.constants.ATTRIBUTE_AUTHENTIC_BAMBA, 1, global.constants.FILTER_COMPARASION_TYPE_EQUALS));
            }

            if (this.OnlyOnSpecialOffer) {
                params.OnlyOnSpecialOffer = this.OnlyOnSpecialOffer;
            }

            if(this.IgnorePriceAndAvailability) {
                params.IgnorePriceAndAvailability = this.IgnorePriceAndAvailability;
            }

            if (this.FilterType) {
                if (typeof params.ObjectTypeIDList == "undefined") {
                    params.ObjectTypeIDList = [];
                }

                params.ObjectTypeIDList.push(await this.XMLElementsApi.getUnsignedByte(this.FilterType));
            }

            if (this.PassengerConfigurationFilterList) {
                if (typeof params.PassengerConfigurationFilterList == "undefined") {
                    params.PassengerConfigurationFilterList = {};
                }

                params.PassengerConfigurationFilterList = await this.XMLElementsApi.getPassengerConfigurationFilter(this.PassengerConfigurationFilterList);
            }

            if (this.ShowFileds) {
                if (typeof params.OutParameterList == "undefined") {
                    params.OutParameterList = [];
                }

                params.OutParameterList = await this.XMLElementsApi.getAttrOutParameter(this.ShowFileds, global.constants.FILTER_NUMBER_OF_RESULTS);
            }

            if (this.StartDate && this.EndDate) {
                params.StartDate = this.StartDate;
                params.EndDate = this.EndDate;
            }

            if (this.InPriceType) {
                params.InPriceType = this.InPriceType;
            }

            if (this.CurrencyID) {
                params.CurrencyID = this.CurrencyID;
            }

            // Filtro por pais OK
            if (this.CountryIDList.length > 0) {
                params.CountryIDList = await this.XMLElementsApi.getLong(this.CountryIDList);
            }

            if (this.DestinationIDList) {
                if (typeof params.DestinationIDList != "undefined") {
                    params.DestinationIDList = [];
                }

                params.DestinationIDList = await this.XMLElementsApi.getLong(this.DestinationIDList);
            }

            if (this.LanguageID) {
                params.LanguageID = this.LanguageID;
            }

            if (this.CurrentPage) {
                params.CurrentPage = this.CurrentPage;
            }

            if (this.PageSize) {
                params.PageSize = this.PageSize;
            }
            

            if (this.SortBy && this.SortOrder) {
                params.SortParameterList = [];

                switch(this.SortBy.trim()) {
                    case global.constants.SORT_BY_DURATION:
                            params.SortParameterList.push(await this.XMLElementsApi.getSortCustomParameter(global.constants.FILTER_CUSTOM_ATRIBUTE, this.SortOrder.trim(), global.constants.ATTRIBUTE_DURATION_HR, global.constants.ATTRIBUTE_TYPE_NUMERIC));
                        break;
                    case global.constants.SORT_BY_PRIORITY:
                            params.SortParameterList.push(await this.XMLElementsApi.getSortCustomParameter(global.constants.FILTER_CUSTOM_ATRIBUTE, this.SortOrder.trim(), global.constants.ATTRIBUTE_PRIORITY, global.constants.ATTRIBUTE_TYPE_NUMERIC));
                        break;
                    default:
                            params.SortParameterList.push(await this.XMLElementsApi.getSortParameter(this.SortBy.trim(), this.SortOrder.trim()));
                        break;
                    break;
                }
            }

            var args = {
                getSearchResultsParameters: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetSearchResults(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}