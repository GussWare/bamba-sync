import soap from 'soap';
import XMLElementsApi from './XMLElementsApi';

export default class GetTransportationSearchResults {

    constructor() {
        this.StartDate = null;
        this.EndDate = null;
        this.InPriceType = null;
        this.ChildrenAgeList = null;
        this.NumberOfPersons = null;
        this.PageSize = null;
        this.CurrentPage = null;
        this.CurrencyID = null;
        this.PickupDestinationIDList = null;
        this.DropoffDestinationIDList = null;
        this.CategoryIDListUnion = null;
        this.ObjectAttributeFilterList = null;
        this.UnitAttributeFilterList = null;
        this.OutParameterList = null;
        this.OutParameter = null;
    }

    GetTransportationSearchResults() {

        var xmlElementsApi = new XMLElementsApi();

        return new Promise(async (resolve, reject) => {

            var params = {};


            if (this.StartDate) {
                params.StartDate = this.StartDate;
            }

            if (this.EndDate) {
                params.EndDate = this.EndDate;
            }

            if (this.CategoryIDListUnion) {
                params.CategoryIDListUnion = await xmlElementsApi.getInt(this.CategoryIDListUnion);
            }

            if (this.PickupDestinationIDList) {
                params.PickupDestinationIDList = await xmlElementsApi.getLong(this.PickupDestinationIDList);
            }

            if (this.DropoffDestinationIDList) {
                params.DropoffDestinationIDList = await xmlElementsApi.getLong(this.DropoffDestinationIDList);
            }

            if (this.ChildrenAgeList) {
                params.ChildrenAgeList = await xmlElementsApi.getInt(this.ChildrenAgeList);
            }

            if (this.NumberOfPersons) {
                if (!params.UnitAttributeFilterList) {
                    console.log("si tiene que entrar aqui entonces si es array");
                    params.UnitAttributeFilterList = [];
                }

                params.UnitAttributeFilterList.push(await xmlElementsApi.getAttrFilterByValue(global.constants.ATTRIBUTE_CAPACITY, this.NumberOfPersons, global.constants.FILTER_COMPARASION_TYPE_GREATER_OR_EQUAL_THAN));
            }

            if (this.CurrencyID) {
                params.CurrencyID = this.CurrencyID;
            }

            if (this.InPriceType) {
                params.InPriceType = this.InPriceType;
            }

            if (this.PageSize) {
                params.PageSize = this.PageSize;
            }

            if (this.CurrentPage) {
                params.CurrentPage = this.CurrentPage;
            }

            if (this.OutParameterList) {
                params.OutParameterList = this.OutParameterList;
            } else {
                params.OutParameterList = await await xmlElementsApi.getOutParameterFilterList([
                    {
                        ResponseDetail: "ObjectDetailedAttributes",
                        NumberOfResults: 100
                    },
                    {
                        ResponseDetail: "UnitDetailedAttributes",
                        NumberOfResults: 100
                    },
                    {
                        ResponseDetail: "CalculatedPriceInfo",
                        NumberOfResults: 100
                    },
                    {
                        ResponseDetail: "ObjectDescription",
                        NumberOfResults: 100
                    }
                ]);
            }

            var args = {
                getTransportationSearchResultsParameters: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetTransportationSearchResults(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}