import soap from 'soap';

export default class GetPaymentMethods {

    constructor() {
        this.LanguageID = null;
    }

    GetPaymentMethods() {
        return new Promise((resolve, reject) => {

            var params = {};

            if (this.LanguageID) {
                params.LanguageID = this.LanguageID;
            }


            var args = {
                getPaymentMethodRequest: params
            };

            var headers = {
                "AuthHeader": {
                    "Username": global.config.lemax.user,
                    "Password": global.config.lemax.pass
                }
            };

            soap.createClient(global.config.lemax.wsdl, { forceSoap12Headers: true, envelopeKey: 'soap12' }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    client.addSoapHeader(headers);
                    client.GetPaymentMethods(args, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}