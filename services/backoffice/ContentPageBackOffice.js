import request from 'request';
import LangHelper from '../../helpers/LanguageHelper';


export default class ContentPageBackOffice {

    constructor() {

    }

    getAll(params) {

        return new Promise((resolve, reject) => {
            var url = global.config.backoffice.api + '/contentPage/?page=' + params.page + '&perPage=' + params.perPage;

            if (params.filter) {
                url += "&filter=" + params.filter;
            }

            if (params.listShow) {
                url += "&listShow=" + params.listShow;
            }

            var options = {
                method: 'GET',
                url: url,
                json: true
            };

            request(options, (error, response, body) => {
                if (error) {
                    reject({
                        status: global.constants.STATUS_500_INTERNAL_SERVER_ERROR,
                        body: {
                            error: LangHelper.lang('ERROR_CONNECTION_KEYSTONE')
                        }
                    });
                } else {
                    resolve({
                        status: response.statusCode,
                        body: body
                    });
                }
            });
        });
    }

    get(params) {

        return new Promise((resolve, reject) => {

            if (params.id) {
                var url = global.config.backoffice.api + '/contentPage/' + (params.id);

                if (params.retrieveShow) {
                    url += "/?retrieveShow=" + params.retrieveShow;
                }

                var options = {
                    method: 'GET',
                    url: url,
                    json: true
                };

                request(options, (error, response, body) => {
                    if (error) {
                        reject({
                            status: global.constants.STATUS_500_INTERNAL_SERVER_ERROR,
                            body: {
                                error: LangHelper.lang('ERROR_CONNECTION_KEYSTONE')
                            }
                        });
                    } else {
                        resolve({
                            status: response.statusCode,
                            body: body
                        });
                    }
                });
            } else {
                reject({
                    status: global.constants.STATUS_500_INTERNAL_SERVER_ERROR,
                    body: { error: LangHelper.lang('ERROR_ID_NOT_VALID') }
                });
            }
        });
    }
}