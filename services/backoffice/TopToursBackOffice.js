import request from 'request';
import LangHelper from '../../helpers/LanguageHelper';


export default class TopToursService {

    constructor() {

    }

    /**
     * recover all the categories of the themes
     */
    getAll(params) {

        return new Promise((resolve, reject) => {
            var url = global.config.backoffice.api + '/toptours/?page=' + params.page + '&perPage=' + params.perPage;

            if (params.filter) {
                url += "&filter=" + params.filter;
            }

            if (params.listShow) {
                url += "&listShow=" + params.listShow;
            }

            var options = {
                method: 'GET',
                url: url,
                json: true
            };

            request(options, (error, response, body) => {
                if (error) {
                    reject({
                        status: global.constants.STATUS_500_INTERNAL_SERVER_ERROR,
                        body: {
                            error: LangHelper.lang('ERROR_CONNECTION_KEYSTONE')
                        }
                    });
                } else {
                    resolve({
                        status: response.statusCode,
                        body: body
                    });
                }
            });
        });
    }

    /**
     * retrieves the theme of the category based on its identifier
     */
    get(params) {

        return new Promise((resolve, reject) => {

            if (params.id) {
                var url = global.config.backoffice.api + '/toptours/' + (params.id);

                if (params.retrieveShow) {
                    url += "/?retrieveShow=" + params.retrieveShow;
                }

                var options = {
                    method: 'GET',
                    url: url,
                    json: true
                };

                request(options, (error, response, body) => {
                    if (error) {
                        reject({
                            status: global.constants.STATUS_500_INTERNAL_SERVER_ERROR,
                            body: {
                                error: LangHelper.lang('ERROR_CONNECTION_KEYSTONE')
                            }
                        });
                    } else {
                        resolve({
                            status: response.statusCode,
                            body: body
                        });
                    }
                });
            } else {
                reject({
                    status: global.constants.STATUS_500_INTERNAL_SERVER_ERROR,
                    body: { error: LangHelper.lang('ERROR_ID_NOT_VALID') }
                });
            }
        });
    }
}