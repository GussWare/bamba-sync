var stripe = require("stripe")("sk_test_AZiFLubLZDo6kSDucpvKaZL5");

export default class StripeService {

    constructor() {

    }

    async verificarTransaccion(params) {

        // se convierte en integer 
        // https://stripe.com/docs/recipes/variable-amount-checkout

        var charge = await stripe.charges.create({
            amount: Math.round(parseFloat(params.amount) * 100),
            currency: params.currency,
            description: params.description,
            source: params.token
        });

        console.log(charge);

        return charge;
    }
}