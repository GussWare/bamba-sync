import RegionModel from '../../models/RegionModel';
import TourDetailModel from '../../models/TourDetailModel';
import ShopInterface from '../../interface/shopInterface';
import ShopHelper from '../../helpers/ShopHelper';

export default class ShopBamba {

    constructor() {
    }

    async getProducts(params) {

        var filter = {};
        var sortby = {};
        var currentPage = 1;
        var pageSize = 12;
        var response = await ShopInterface.getShopInterface();

        //ok
        if (params.PackageTourIDList) {
            var toirsIDs = params.PackageTourIDList.split(',');
            if (Array.isArray(toirsIDs)) {
                filter.TourID = { $in: toirsIDs };
            }
        }

        //ok
        if (params.StyleIDList) {
            var stylesArr = params.StyleIDList.split(',');
            if (Array.isArray(stylesArr)) {
                var stylesint = [];
                for (const i in stylesArr) {
                    stylesint.push(parseInt(stylesArr[i]));
                }
                filter.Style = { $elemMatch: { lemaxId: { $in: stylesint } } };
            }
        }

        //ok
        if (params.CountryIDList) {
            var countryArr = params.CountryIDList.split(',');
            if (Array.isArray(countryArr)) {
                var contriesInt = [];
                for (const i in countryArr) {
                    contriesInt.push(parseInt(countryArr[i]));
                }
                filter.Countries = { $elemMatch: { lemaxId: { $in: contriesInt } } };
            }
        } else {
            if (params.RegionsIDList) {
                var regionsIDList = params.RegionsIDList.split(',');
                var regions = await RegionModel.find({ _id: { $in: regionsIDList } }).populate('allCountry');

                if (Array.isArray(regions)) {
                    for (const i in regions) {
                        if (Array.isArray(regions[i].allCountry)) {

                            var contriesInt = [];

                            for (const j in regions[i].allCountry) {
                                if (typeof regions[i].allCountry[j].lemaxId != "undefined") {
                                    if (contriesInt.indexOf(regions[i].allCountry[j].lemaxId) < 0) {
                                        contriesInt.push(regions[i].allCountry[j].lemaxId);
                                    }
                                }
                            }

                            if (contriesInt.length > 0) {
                                filter.Countries = { $elemMatch: { lemaxId: { $in: contriesInt } } };
                            }
                        }
                    }
                }
            }
        }

        //ok
        if (params.ThemeIDList) {
            var themeArr = params.ThemeIDList.split(',');
            if (Array.isArray(themeArr)) {
                var themeInt = [];
                for (const i in themeArr) {
                    themeInt.push(parseInt(themeArr[i]));
                }
                filter.Themes = { $elemMatch: { ThemeID: { $in: themeInt } } };
            }
        }

        //Ok
        if (params.StartDate && params.EndDate) {
            filter.DurationDD = { $gt: parseInt(params.StartDate), $lt: parseInt(params.EndDate) };
        }

        //ok
        if (params.PriceFrom && params.PriceTo) {
            filter["PriceInfo.Price"] = { $gt: parseFloat(params.PriceFrom), $lt: parseFloat(params.PriceTo) };
        }

        if (params.BambaRecommendedTrip) {
            filter.BambaRecommendedTrip = 1;
        }

        // ok 
        if(params.OnlyOnSpecialOffer) {
           // filter["PriceInfo.SpecialOffer"] = {$ne:null};
        }

        //ok 
        if (params.SortBy && params.SortOrder) {
            sortby[params.SortBy] = await ShopHelper.sortby(params.SortOrder);
        }

        if (params.CurrentPage) {
            console.log("debe entrar aqui");
            currentPage = parseInt(params.CurrentPage);
        }

        if(params.PageSize) {
            console.log("debe entrar aqui");
            pageSize = parseInt(params.PageSize);
        }

        console.log("Filter", JSON.stringify(filter));
        console.log("pageSize", pageSize);
        console.log("currentPage", currentPage);

        var totalPaginas = await TourDetailModel.countDocuments(filter)
        var tourList = await TourDetailModel.find(filter).skip(currentPage > 0 ? ( ( currentPage - 1 ) * pageSize ) : 0).limit(pageSize).sort(sortby);

        for (const i in tourList) {
            tourList[i].Galery = await ShopHelper.quitarFotosGallery(tourList[i].Galery);
        }

        response.PackageSearchResults = tourList;
        response.Pagination.TotalNumberOfResults = totalPaginas;
        response.Pagination.CurrentPage = params.CurrentPage;
        response.Pagination.PageSize = params.PageSize;

        return response;
    }
}