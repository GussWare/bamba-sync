import GetSearchResults from '../lemax/GetSearchResults';
import DestinationHelper from '../../helpers/DestinationHelper';
import HotelInterface from '../../interface/hotelInterface';
import FakeHelper from '../../helpers/FakeHelper';
import CalculatePriceInterface from '../../interface/calculatePriceInterface';
import AttributeGroupLibrary from '../../libraries/AttributeGroupLibrary';

export default class HotelesBamba {

    constructor() {
        this.AttributeGroupLibrary = new AttributeGroupLibrary();
    }

    /**
     * 
     * @param {*} params 
     */
    async getHoteles(params) {
        var hoteles = [];
        var getSearchResults = new GetSearchResults();
        
        getSearchResults.ShowFileds = [
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 4
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 4
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 100
            }
        ];

        getSearchResults.LanguageID = params.LanguageID || await LangHelper.getLang();

        if (typeof params.Rooms != "undefined") {
            getSearchResults.PassengerConfigurationFilterList = [];

            if (Array.isArray(params.Rooms)) {
                if (params.Rooms.length > 0) {
                    var numberConsta = 1;
                    for (const i in params.Rooms) {
                        getSearchResults.PassengerConfigurationFilterList.push(
                            {
                                RoomsNumber: numberConsta,
                                AdultsNumber: params.Rooms[i].Adults,
                                ChildrenAges: await FakeHelper.childrenAge(params.Rooms[i].Childrens)
                            }
                        );
                        numberConsta++;
                    }
                }
            }
        }

        if (typeof params.StartDate != "undefined" && typeof params.EndDate != "undefined") {
            getSearchResults.StartDate = params.StartDate;
            getSearchResults.EndDate = params.EndDate;
        }

        if (typeof params.DestinationIDList != "undefined") {
            getSearchResults.DestinationIDList = params.DestinationIDList;
        }

        if (typeof params.InPriceType != "undefined") {
            getSearchResults.InPriceType = params.InPriceType;
        } else {
            getSearchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        if (typeof params.CurrencyID != "undefined") {
            getSearchResults.CurrencyID = params.CurrencyID;
        }

        if (typeof params.ShowFileds != "undefined") {
            getSearchResults.ShowFileds = params.ShowFileds.split(',');
        }

        if (typeof params.PageSize != "undefined") {
            getSearchResults.PageSize = params.PageSize;
        } else {
            getSearchResults.PageSize = 1;
        }

        if (typeof params.CurrentPage != "undefined") {
            getSearchResults.CurrentPage = params.CurrentPage;
        } else {
            getSearchResults.CurrentPage = 1;
        }

        if (typeof params.SortBy != "undefined" && typeof params.SortOrder != "undefined") {
            getSearchResults.SortBy = params.SortBy.trim();
            getSearchResults.SortOrder = params.SortOrder.trim();
        }

        var response = await getSearchResults.GetSearchResults();
        if (typeof response.GetSearchResultsResult != "undefined") {
            if (response.GetSearchResultsResult.Status.Code == 'OK') {
                if (response.GetSearchResultsResult.AccommodationObjectList) {
                    if (Array.isArray(response.GetSearchResultsResult.AccommodationObjectList.AccommodationObject)) {
                        if (response.GetSearchResultsResult.AccommodationObjectList.AccommodationObject.length > 0) {
                            var AccommodationObject = response.GetSearchResultsResult.AccommodationObjectList.AccommodationObject;
                            var hotelItem = null;
                            var type = null;

                            for (const i in AccommodationObject) {
                                type = AccommodationObject[i].ObjectType.ObjectTypeID;

                                hotelItem = await HotelInterface.getHotelItemInterface();
                                hotelItem.ID = AccommodationObject[i].ObjectID;
                                hotelItem.Name = AccommodationObject[i].Name;
                                hotelItem.Stars = await this.getStars(type, AccommodationObject[i].AttributeGroupList);
                                hotelItem.Priority = await this.getPriority(type, AccommodationObject[i].AttributeGroupList);
                                hotelItem.Destination = await DestinationHelper.getDestination(AccommodationObject[i].DestinationID);
                                hotelItem.UnitMinimumPriceInfo = await this.getPriceInfo(AccommodationObject[i].UnitList);
                                hotelItem.UnitList = await this.getUnitList(AccommodationObject[i].UnitList);

                                hoteles.push(hotelItem);
                            }
                        }
                    }
                }
            }
        }

        hoteles = await this.orderStars(hoteles);

        return hoteles;
    }

    async getUnitList(UnitList) {
        var ServicesList = [];
        var ServiceItem = null;
        var type = null;

        if (UnitList) {
            if (Array.isArray(UnitList.AccommodationUnit)) {
                if (UnitList.AccommodationUnit.length > 0) {
                    for (const i in UnitList.AccommodationUnit) {
                        type = UnitList.AccommodationUnit[i].Type.UnitTypeID;

                        ServiceItem = await HotelInterface.getServiceItemInterface();
                        ServiceItem.UnitID = UnitList.AccommodationUnit[i].UnitID;
                        ServiceItem.ServiceID = await this.getServiceDefault(UnitList.AccommodationUnit[i]);
                        ServiceItem.BillingType = await this.getBillingTypeName(UnitList.AccommodationUnit[i]);
                        ServiceItem.Description = await this.getDescriptionService(type, UnitList.AccommodationUnit[i].AttributeGroupList);
                        ServiceItem.Capacity = await this.getCapacityService(type, UnitList.AccommodationUnit[i].AttributeGroupList);
                        ServiceItem.Price = await this.getCalculatePriceService(UnitList.AccommodationUnit[i]);

                        ServicesList.push(ServiceItem);
                    }
                }
            }
        }

        return ServicesList;
    }

    async getPriceInfo(UnitList) {

        var priceInfo = null;

        if (UnitList) {
            if (typeof UnitList.AccommodationUnit != "undefined") {
                if (Array.isArray(UnitList.AccommodationUnit)) {
                    priceInfo = (UnitList.AccommodationUnit.length > 0) ? UnitList.AccommodationUnit[0].UnitMinimumPriceInfo : null;
                }
            }
        }

        return priceInfo;
    }

    async getStars(type, AttributeGroupList) {
        var Stars = null;
        Stars = await this.AttributeGroupLibrary.getAttribute(type, global.constants.ATTRIBUTE_NUMBER_OF_STARTS, AttributeGroupList);

        return Stars;
    }

    async getPriority(type, AttributeGroupList) {
        var Stars = null;
        Stars = await this.AttributeGroupLibrary.getAttribute(type, global.constants.ATTRIBUTE_PRIORITY, AttributeGroupList);

        return Stars;
    }

    async getCapacityService(type, AttributeGroupList) {
        var Capacity = null;
        Capacity = await this.AttributeGroupLibrary.getAttribute(type, global.constants.ATTRIBUTE_CAPACITY, AttributeGroupList);

        return Capacity;
    }

    async getCapacityService(type, AttributeGroupList) {
        var Capacity = null;
        Capacity = await this.AttributeGroupLibrary.getAttribute(type,  global.constants.ATTRIBUTE_CAPACITY, AttributeGroupList);

        return Capacity;
    }

    async getDescriptionService(type, AttributeGroupList) {
        var Description = null;
        Description = await this.AttributeGroupLibrary.getAttribute(type, global.constants.ATTRIBUTE_DESCRIPTION, AttributeGroupList);

        return Description;
    }

    async getCalculatePriceService(AccommodationUnit) {
        var calculatePrice = null;

        if (AccommodationUnit) {
            if (typeof AccommodationUnit.CalculatedPriceInfo != "undefined") {
                calculatePrice = await CalculatePriceInterface.getCalculatePriceInterface();
                calculatePrice.TotalPrice = AccommodationUnit.CalculatedPriceInfo.CalculatedPrice;
                calculatePrice.PriceFormated = AccommodationUnit.CalculatedPriceInfo.CalculatedPriceFormated;
            }
        }

        return calculatePrice;
    }

    async getServiceDefault(AccommodationUnit) {
        var ServiceID = null;

        if(AccommodationUnit.ServiceList) {
            if(Array.isArray(AccommodationUnit.ServiceList.Service)) {
                if(AccommodationUnit.ServiceList.Service.length > 0) {    
                    ServiceID = AccommodationUnit.ServiceList.Service[0].ServiceID;
                }
            }
        }

        return ServiceID;
    }

    async getBillingTypeName(AccommodationUnit) {
        var BillingTypeName = null;

        if(AccommodationUnit.ServiceList) {
            if(Array.isArray(AccommodationUnit.ServiceList.Service)) {
                if(AccommodationUnit.ServiceList.Service.length > 0) {    
                    BillingTypeName = AccommodationUnit.ServiceList.Service[0].BillingType;
                }
            }
        }

        return BillingTypeName;
    }

    async orderStars(hoteles) {

        hoteles.sort(function(a, b){
            return parseInt(a.Stars) - parseInt(b.Stars);
        });

        return hoteles;
    }
}