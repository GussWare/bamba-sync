import PackageSearchResults from '../lemax/GetPackageSearchResults';
import GetSearchResults from '../lemax/GetSearchResults';
import SimpleTourLibrary from '../../libraries/SimpleTourLibrary';
import FitTourLibrary from '../../libraries/FitTourLibrary';
import ActivitiesTourLibrary from '../../libraries/ActivitiesTourLibrary';
import LangHelper from '../../helpers/LanguageHelper';
import TourDetailEntity from '../../entity/TourDetailEntity';

export default class StaticTourDetailBamba {

    constructor() {

    }

    async syncTourDetail(params) {

        var tourList = [];
        var activityList = [];
        var packageList = [];
        var tours = [];
        var tour = {};

        for (const i in params.TourList) {
            switch (parseInt(params.TourList[i].Type)) {
                case global.constants.TOUR_TYPE_SIMPLE_TOUR:
                case global.constants.TOUR_TYPE_FIT_TOUR:
                    tourList.push(params.TourList[i].TourID);
                    break;
                case global.constants.TOUR_TYPE_ACTIVITIES_TOUR:
                    activityList.push(params.TourList[i].TourID);
                    break;
                default:
                    break;
            }
        }

        if (tourList.length > 0) {
            tours = await this.getTour({
                PackageTourIDList: tourList,
                LanguageID: params.LanguageID,
                CurrencyID: params.CurrencyID,
                InPriceType: params.InPriceType
            });

            if (Array.isArray(tours)) {
                if (tours.length > 0) {
                    for (const i in tours) {
                        const tourDeatilEntity = new TourDetailEntity();
                        await tourDeatilEntity.save(tours[i]);

                        packageList.push(tours[i]);
                    }
                }
            }
        }


        if (activityList.length > 0) {

            for (const i in activityList) {
                tour = await this.getActivity({
                    TourID: activityList[i],
                    LanguageID: params.LanguageID,
                    CurrencyID: params.CurrencyID,
                    InPriceType: params.InPriceType
                });

                if (tour.TourID) {
                    const tourDeatilEntity = new TourDetailEntity();
                    await tourDeatilEntity.save(tour);

                    packageList.push(tour);
                }
            }
        }

        return packageList;
    }

    async getTour(params) {
        var tourResult = {};
        var searchResults = new PackageSearchResults();
        var packageTours = [];

        if (params.PackageTourIDList) {
            searchResults.PackageTourIDList = params.PackageTourIDList;
        }

        if (params.LanguageID) {
            searchResults.LanguageID = params.LanguageID;
        } else {
            searchResults.LanguageID = await LangHelper.getLang();
        }

        if (params.CurrencyID) {
            searchResults.CurrencyID = params.CurrencyID;
        } else {
            searchResults.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (params.InPriceType) {
            searchResults.InPriceType = params.InPriceType;
        } else {
            searchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        if (typeof params.PageSize != "undefined") {
            searchResults.PageSize = params.PageSize;
        } else {
            searchResults.PageSize = 100;
        }

        searchResults.ShowFileds = [
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 11
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 11
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 100
            },

            {
                ResponseDetail: "GetHotels",
                NumberOfResults: 100
            },

            /*
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 10
            },
            
            {
                ResponseDetail: "ObjectTitle",
                NumberOfResults: 10
            },
            
            {
                ResponseDetail: "MapCoordinates",
                NumberOfResults: 10
            },
            
            {
                ResponseDetail: "ObjectSEODescription",
                NumberOfResults: 10
            },
            {
                ResponseDetail: "GetObjectInfoForAllResults",
                NumberOfResults: 10
            },
            {
                ResponseDetail: "GetPricesInAllActiveCurrencies",
                NumberOfResults: 10
            }*/
        ]

        var response = await searchResults.GetPackageSearchResults();
        var GetPackageSearchResultsResult = response.GetPackageSearchResultsResult;

        if (GetPackageSearchResultsResult.Status.Code == 'OK') {

            if (typeof GetPackageSearchResultsResult.PackageTourList != "undefined") {
                if (GetPackageSearchResultsResult.PackageTourList) {

                    if (Array.isArray(GetPackageSearchResultsResult.PackageTourList.PackageTour)) {
                        var tourResult = GetPackageSearchResultsResult.PackageTourList.PackageTour;
                        var tourObject = null;

                        for (const i in tourResult) {
                            switch (tourResult[i].ObjectType.ObjectTypeID) {
                                case global.constants.TOUR_TYPE_SIMPLE_TOUR:
                                    tourObject = new SimpleTourLibrary();
                                    packageTours.push(await tourObject.getPackage(tourResult[i]));
                                    break;
                                case global.constants.TOUR_TYPE_FIT_TOUR:
                                    tourObject = new FitTourLibrary();
                                    packageTours.push(await tourObject.getPackage(tourResult[i]));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return packageTours;
    }

    async getActivity(params) {
        var tour = {};
        var searchResults = new GetSearchResults();

        searchResults.ShowFileds = [
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 11
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 11
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 100
            }
        ];

        searchResults.LanguageID = params.LanguageID || await LangHelper.getLang();
        searchResults.FilterType = [global.constants.FILTER_BY_ACTIVITIES];

        if (typeof params.TourID != "undefined") {
            searchResults.ObjectIDList = [params.TourID];
        }

        if (typeof params.PageSize != "undefined") {
            searchResults.PageSize = params.PageSize;
        } else {
            searchResults.PageSize = 1;
        }

        if (typeof params.CurrentPage != "undefined") {
            searchResults.CurrentPage = params.CurrentPage;
        } else {
            searchResults.CurrentPage = 1;
        }

        if (typeof params.CurrencyID != "undefined") {
            searchResults.CurrencyID = params.CurrencyID;
        } else {
            searchResults.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (typeof params.InPriceType != "undefined") {
            searchResults.InPriceType = params.InPriceType;
        } else {
            searchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await searchResults.GetSearchResults();
        if (typeof response.GetSearchResultsResult != "undefined") {
            if (response.GetSearchResultsResult.Status.Code == 'OK') {
                if (response.GetSearchResultsResult.AccommodationObjectList) {
                    var activityTourLibrary = new ActivitiesTourLibrary();
                    tour = await activityTourLibrary.getPackage(response.GetSearchResultsResult);
                }
            }
        }

        return tour;
    }
}