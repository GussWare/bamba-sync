import InsertReservation from '../lemax/InsertReservation';
import ReservationInterface from '../../interface/reservationInterface';
import UserInterface from '../../interface/usuarioInterface';
import JwtHelper from '../../helpers/JwtHelper';

export default class ConfirmAndPayBamba {

    constructor() {
        this.ExecuteInsert = null;
        this.CurrencyID = null;
        this.PaymentMethodID = null;
        this.LanguageID = null;
        this.Customer = null;
        this.ReservationItems = [];
        this.AdHocItems = [];
    }

    /**
     * If True then the reservation will be entered into the system, if False reservation won’t be entered 
     * (used for gettings list of components on FIT tour or Final price check)
     * 
     * @param {boolean} ExecuteInsert 
     * @returns {void}
     */
    async setExecuteInsert(ExecuteInsert) {
        this.ExecuteInsert = ExecuteInsert;
    }

    /**
     * Choose currency in which the response will be
     * 
     * @param {int} CurrencyID 
     * @returns {void}
     */
    async setCurrencyID(CurrencyID) {
        this.CurrencyID = CurrencyID;
    }

    /**
     * Method of payment
     * 
     * @param {int}
     * @returns {void}
     */
    async setPaymentMethodID(PaymentMethodID) {
        this.PaymentMethodID = PaymentMethodID;
    }

    /**
     * Language in which response is returned
     * 
     * @param {string} LanguageID 
     * @returns {void}
     */
    async setLanguageID(LanguageID) {
        this.LanguageID = LanguageID;
    }

    /**
     * Customer data
     * 
     * @param {*} Customer 
     * @returns {void}
     */
    async setCustomer(Customer) {
        this.Customer = Customer;
    }

    /**
     * 
     * @param {*} ReservationItems 
     * @returns {void}
     */
    async setReservationItems(ReservationItems) {
        this.ReservationItems = ReservationItems;
    }

    /**
     * Each item represents different product
     * 
     * @param {*} AdHocItems 
     * @returns {void}
     */
    async setAdHocItems(AdHocItems) {
        this.AdHocItems = AdHocItems;
    }

    /**
     * It is responsible for confirming the reservation
     * 
     * @returns {void}
     */
    async confirmAndPay() {

        const insertReservation = new InsertReservation();

        if (this.ExecuteInsert) {
            insertReservation.ExecuteInsert = this.ExecuteInsert;
        }

        if (this.CurrencyID) {
            insertReservation.CurrencyID = this.CurrencyID;
        }

        if (this.PaymentMethodID) {
            insertReservation.PaymentMethodID = this.PaymentMethodID;
        }

        if (this.LanguageID) {
            insertReservation.LanguageID = this.LanguageID;
        }

        if (this.Customer) {
            insertReservation.CustomerObject = await this.getPrepareCustomer(this.Customer);
        }

        if (this.ReservationItems) {
            insertReservation.ReservationItems = await this.getPrepareReservation(this.ReservationItems);
        }

        if (this.AdHocItems) {
            insertReservation.AdHocReservationItems = await this.getPrepareReservation(this.AdHocItems);
        }

        const response = await insertReservation.InsertReservation();


        if (response.InsertReservationResult) {
            var reservation = await ReservationInterface.geReservationInterface();
            reservation.ReservationID = response.InsertReservationResult.ReservationID;
            reservation.Status = response.InsertReservationResult.Status;
            reservation.SellingPrice = response.InsertReservationResult.SellingPrice;
            reservation.EndReservationURL = response.InsertReservationResult.EndReservationURL;
            reservation.Customer = response.InsertReservationResult.Customer;

            if (response.InsertReservationResult.ReservationItems) {
                if (Array.isArray(response.InsertReservationResult.ReservationItems.InsertReservationItemRS)) {
                    if (response.InsertReservationResult.ReservationItems.InsertReservationItemRS.length > 0) {

                        for (const i in response.InsertReservationResult.ReservationItems.InsertReservationItemRS) {

                            var reservationItem = await ReservationInterface.geReservationItemInterface();
                            reservationItem.ReservationItemOrder = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].ReservationItemOrder;
                            reservationItem.SellingPrice = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].SellingPrice;
                            reservationItem.NetPrice = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].NetPrice;
                            reservationItem.Commission = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].Commission;
                            reservationItem.ReservationItemDetailsList = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].ReservationItemDetailsList;
                            reservationItem.PriceCalculationStatus = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].PriceCalculationStatus;
                            reservationItem.CancellationData = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].CancellationData;
                            reservationItem.StartDate = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].StartDate;
                            reservationItem.EndDate = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].EndDate;
                            reservationItem.StartDateTime = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].StartDateTime;
                            reservationItem.EndDateTime = response.InsertReservationResult.ReservationItems.InsertReservationItemRS[i].EndDateTime;

                            reservation.ReservationItems.InsertReservationItemRS.push(reservationItem);
                        }
                    }
                }
            }
        }

        return reservation;
    }

    /**
     * Prepare the items to make the reservation
     * 
     * @param {*} ReservationItems 
     * @returns {array}
     */
    async getPrepareReservation(ReservationItems) {

        var reservationItems = [];
        var reservationItem = {};
        var passegnerItem = {};

        if (Array.isArray(ReservationItems)) {
            if (ReservationItems.length > 0) {
                for (const i in ReservationItems) {

                    reservationItem = {};

                    if (ReservationItems[i].UnitID) {
                        reservationItem.UnitID = ReservationItems[i].UnitID;
                    }

                    if(ReservationItems[i].ClientComment) {
                        reservationItem.ClientComment = ReservationItems[i].ClientComment;
                    }

                    reservationItem.StartDate = ReservationItems[i].StartDate;
                    reservationItem.EndDate = ReservationItems[i].EndDate;
                    reservationItem.SelectedServices = ReservationItems[i].Services;

                    if (typeof ReservationItems[i].Passengers != "undefined") {
                        if (Array.isArray(ReservationItems[i].Passengers)) {
                            if (ReservationItems[i].Passengers.length > 0) {

                                reservationItem.Passengers = [];

                                for (const j in ReservationItems[i].Passengers) {
                                    passegnerItem = {};

                                    if (typeof ReservationItems[i].Passengers[j].FullName != "undefined") {
                                        passegnerItem.FullName = ReservationItems[i].Passengers[j].FullName;
                                        passegnerItem.Name = await this.getPassengerName(ReservationItems[i].Passengers[j].FullName);
                                        passegnerItem.Surname = await this.getPassengerSurname(ReservationItems[i].Passengers[j].FullName);
                                    }

                                    passegnerItem.DateOfBirth = ReservationItems[i].Passengers[j].DateBirth;
                                    passegnerItem.Gender = await this.getGenderPassenger(ReservationItems[i].Passengers[j].Gender);
                                    passegnerItem.CountryID = ReservationItems[i].Passengers[j].Nationality;

                                    reservationItem.Passengers.push(passegnerItem);
                                }
                            }
                        }
                    }

                    reservationItems.push(reservationItem);
                }
            }
        }

        return reservationItems;
    }

    /**
     * Retrieve the name of the full name attribute
     * 
     * @param {*} FullName 
     * @returns {void}
     */
    async getPassengerName(FullName) {
        var nameSplit = FullName.split(' ');
        var name = '';

        if (nameSplit.length > 0) {
            name = nameSplit[0];
        }

        return name;
    }

    /**
     * Retrieve the last name of the full name attribute
     * 
     * @param {string} FullName 
     * @returns {string}
     */
    async getPassengerSurname(FullName) {
        var nameSplit = FullName.split(' ');
        var totalName = nameSplit.length;
        var surName = '';

        if (totalName > 1) {
            for (var i = 1; i <= totalName - 1; i++) {
                surName += nameSplit[i] + ' ';
            }
        }

        return surName;
    }

    /**
     * Prepare the gender for the passenger
     * 
     * @param {*} Sex 
     * @returns {char}
     */
    async getGenderPassenger(Sex) {
        var gender = ''

        if (Sex = 'Male') {
            gender = 'M';
        }

        if (Sex = 'Female') {
            gender = 'F';
        }

        return gender;
    }

    /**
     * It is responsible for generating the gender for the customer's purpose
     * 
     * @param {*} Sex 
     * @returns {int}
     */
    async getGenderCustomer(Sex) {
        var gender = 1;

        if (Sex = 'Male') {
            gender = 1;
        }

        if (Sex = 'Female') {
            gender = 0;
        }

        return gender;
    }

    /**
     * Prepares the data to generate the customer's object
     * 
     * @param {*} customer 
     * @returns {object}
     */
    async getPrepareCustomer(customer) {

        var newCustumer = {};

        if (typeof customer.FullName != "undefined") {
            newCustumer.FullName = customer.FullName;
            newCustumer.Name = await this.getPassengerName(customer.FullName);
            newCustumer.Surname = await this.getPassengerSurname(customer.FullName);
        }

        if (typeof customer.Email != "undefined") {
            newCustumer.Email = customer.Email;
        }

        if (typeof customer.DateBirth != "undefined") {
            newCustumer.BirthDate = customer.DateBirth;
        }

        if (typeof customer.Gender != "undefined") {
            newCustumer.Gender = await this.getGenderCustomer(customer.Gender);
        }

        if (typeof customer.CountryID != "undefined") {
            newCustumer.CountryID = customer.CountryID;
        }

        console.log("newCUsumer", newCustumer);

        return newCustumer;
    }
}