import GetTransportationSearchResults from '../lemax/GetTransportationSearchResults'
import TransportListInterface from '../../interface/transportListInterface';
import AttributeGroupLibrary from '../../libraries/AttributeGroupLibrary';

export default class TransportationListBamba {

    constructor() {
        this.StartDate = null;
        this.EndDate = null;
        this.ChildrenAgeList = [];
        this.PickupDestinationIDList = [];
        this.DropoffDestinationIDList = [];
        this.CategoryIDListUnion = [];
        this.InPriceType = null;
        this.PageSize = null;
        this.CurrentPage = null;
        this.CurrencyID = null;
        this.LanguageID = null;
        this.NumberOfPersons = null;


        this.PriceFormat = null;
        this.TransportationList = [];
        this.Status = null;
        this.TotalNumberOfResults = null;
        this.Language = null;
        this.Currency = null;
        this.DestinationList = null;
        this.RegionIDList = null;
        this.CountryList = null;

        this.GetTransportationSearchResultsResult = null;
        this.TransportationUnitList = null;

        this.AttributeGroupLibrary = new AttributeGroupLibrary();
    }

    async setStartDate(StartDate) {
        this.StartDate = StartDate;
    }

    async setEndDate(EndDate) {
        this.EndDate = EndDate;
    }

    async setChildrenAgeList(ChildrenAgeList) {
        if (ChildrenAgeList) {
            if (Array.isArray(ChildrenAgeList)) {
                for (const i in ChildrenAgeList) {
                    this.ChildrenAgeList.push(ChildrenAgeList[i]);
                }
            } else {
                this.ChildrenAgeList.push(ChildrenAgeList);
            }
        }
    }

    async setNumberOfPersons(NumberOfPersons) {
        this.NumberOfPersons = NumberOfPersons;
    }

    async setPickupDestinationIDList(PickupDestinationIDList) {
        if (PickupDestinationIDList) {
            if (Array.isArray(PickupDestinationIDList)) {
                for (const i in PickupDestinationIDList) {
                    this.PickupDestinationIDList.push(PickupDestinationIDList[i]);
                }
            } else {
                this.PickupDestinationIDList.push(PickupDestinationIDList);
            }
        }
    }

    async setDropoffDestinationIDList(DropoffDestinationIDList) {
        if (DropoffDestinationIDList) {
            if (Array.isArray(DropoffDestinationIDList)) {
                for (const i in DropoffDestinationIDList) {
                    this.DropoffDestinationIDList.push(DropoffDestinationIDList[i]);
                }
            } else {
                this.DropoffDestinationIDList.push(DropoffDestinationIDList);
            }
        }
    }

    async setCategoryIDListUnion(CategoryIDListUnion) {
        if (Array.isArray(CategoryIDListUnion)) {
            for (const i in CategoryIDListUnion) {
                this.CategoryIDListUnion.push(CategoryIDListUnion[i]);
            }
        } else {
            this.CategoryIDListUnion.push(CategoryIDListUnion);
        }
    }

    async setInPriceType(InPriceType) {
        this.InPriceType = InPriceType;
    }

    async setPageSize(PageSize) {
        this.PageSize = PageSize;
    }

    async setCurrentPage(CurrentPage) {
        this.CurrentPage = CurrentPage;
    }

    async setCurrencyID(CurrencyID) {
        this.CurrencyID = CurrencyID;
    }

    async setLanguageID(LanguageID) {
        this.LanguageID = LanguageID;
    }

    async callApi() {
        const getTransportsSearchResults = new GetTransportationSearchResults();

        if (this.StartDate) {
            getTransportsSearchResults.StartDate = this.StartDate;
        }

        if (this.EndDate) {
            getTransportsSearchResults.EndDate = this.EndDate;
        }

        if (this.CategoryIDListUnion.length > 0) {
            getTransportsSearchResults.CategoryIDListUnion = this.CategoryIDListUnion;
        }

        if (this.PickupDestinationIDList.length > 0) {
            getTransportsSearchResults.PickupDestinationIDList = this.PickupDestinationIDList;
        }

        if (this.DropoffDestinationIDList.length > 0) {
            getTransportsSearchResults.DropoffDestinationIDList = this.DropoffDestinationIDList;
        }

        if (this.NumberOfPersons) {
            getTransportsSearchResults.NumberOfPersons = this.NumberOfPersons;
        }

        if (this.ChildrenAgeList.length > 0) {
            getTransportsSearchResults.ChildrenAgeList = this.ChildrenAgeList;
        }

        if (this.CurrencyID) {
            getTransportsSearchResults.CurrencyID = this.CurrencyID;
        }

        if (this.InPriceType) {
            getTransportsSearchResults.InPriceType = this.InPriceType;
        }

        if (this.PageSize) {
            getTransportsSearchResults.PageSize = this.PageSize;
        }

        if (this.CurrentPage) {
            getTransportsSearchResults.CurrentPage = this.CurrentPage;
        }

        var response = await getTransportsSearchResults.GetTransportationSearchResults();
        this.GetTransportationSearchResultsResult = response.GetTransportationSearchResultsResult;

        if (this.GetTransportationSearchResultsResult) {

            this.PriceFormat = this.GetTransportationSearchResultsResult.PriceFormat;
            this.Status = this.GetTransportationSearchResultsResult.Status;
            this.TotalNumberOfResults = this.GetTransportationSearchResultsResult.TotalNumberOfResults;

            if (this.GetTransportationSearchResultsResult.TransportationList) {
                this.TransportationList = this.GetTransportationSearchResultsResult.TransportationList.Transportation;
            }

            this.CurrentPage = this.GetTransportationSearchResultsResult.CurrentPage;
            this.PageSize = this.GetTransportationSearchResultsResult.PageSize;
        }
    }

    async getTransportationList() {

        await this.callApi();

        var transportList = [];
        var transportItem = null;

        if (Array.isArray(this.TransportationList)) {
            if (this.TransportationList.length > 0) {
                for (const i in this.TransportationList) {
                    transportItem = await TransportListInterface.getTransportListInterface();

                    this.TransportationUnitList = null;

                    if (this.TransportationList[i].TransportationUnitList) {
                        if (Array.isArray(this.TransportationList[i].TransportationUnitList.TransportationUnit)) {
                            this.TransportationUnitList = this.TransportationList[i].TransportationUnitList.TransportationUnit;
                        }
                    }

                    transportItem.TransportID = this.TransportationList[i].ObjectID;
                    transportItem.Name = this.TransportationList[i].Name;
                    transportItem.CalculatePriceInfo = await this.getBasicPrecie();
                    transportItem.UnitID = await this.getUnitID();
                    transportItem.Capacity = await this.getCapacity();
                    transportItem.ServiceID = await this.getServiceDefault();
                    transportItem.AvailabilityStatus = await this.getStatus();

                    transportList.push(transportItem);
                }
            }
        }

        this.TransportationUnitList = null;

        return transportList;
    }

    /**
     * 
     */
    async getFirstTransport() {

        var transportItem = null;
        var tranposrtList = await this.getTransportationList();

        if (tranposrtList.length > 0) {
            transportItem = tranposrtList[0];
        }

        return transportItem;
    }

    /**
     * 
     */
    async getBasicPrecie() {
        var CalculatePrice = {
            Price: null,
            PriceFormated: null,
            NumberOfPersons: null
        }

        if (this.TransportationUnitList) {
            if (Array.isArray(this.TransportationUnitList)) {
                CalculatePrice.Price = this.TransportationUnitList[0].CalculatedPriceInfo.CalculatedPrice;
                CalculatePrice.PriceFormated = this.TransportationUnitList[0].CalculatedPriceInfo.CalculatedPriceFormated;
                CalculatePrice.NumberOfPersons = this.TransportationUnitList[0].CalculatedPriceInfo.NumberOfPersons;
            }
        }

        return CalculatePrice;
    }

    async getUnitID() {
        var UnitID = null;

        if (this.TransportationUnitList) {
            if (Array.isArray(this.TransportationUnitList)) {
                UnitID = this.TransportationUnitList[0].UnitID;
            }
        }

        return UnitID;
    }

    async getServiceDefault() {
        var ServiceID = null;

        if (this.TransportationUnitList[0].ServiceList) {
            if (Array.isArray(this.TransportationUnitList[0].ServiceList.Service)) {
                if (this.TransportationUnitList[0].ServiceList.Service.length > 0) {
                    ServiceID = this.TransportationUnitList[0].ServiceList.Service[0].ServiceID;
                }
            }
        }

        return ServiceID;
    }

    async getCapacity() {
        var capacity = null;
        var type = null;

        if (this.TransportationUnitList.length > 0) {
            type = this.TransportationUnitList[0].Type.UnitTypeID;
            capacity = await this.AttributeGroupLibrary.getAttribute(type, global.constants.ATTRIBUTE_CAPACITY, this.TransportationUnitList[0].AttributeGroupList);
        }

        return capacity;
    }

    async getStatus() {
            
        var Status = null;

        if (this.TransportationUnitList) {
            if (Array.isArray(this.TransportationUnitList)) {
                Status = this.TransportationUnitList[0].AvailabilityStatus;
            }
        }

        return Status;
    }
}