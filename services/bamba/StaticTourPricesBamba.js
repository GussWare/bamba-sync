import GetSearchResults from '../lemax/GetSearchResults';
import GetDetailedDescription from '../lemax/GetDetailedDescription';
import LangHelper from '../../helpers/LanguageHelper';

export default class StaticTourPricesBamba {

    constructor() {

    }

    async syncTourPrices(params) {
        return await this.getActivitiesPrices(params);
    }

    async getActivitiesPrices(params) {
        var searchResults = new GetSearchResults();

        searchResults.ShowFileds = [
            {
                ResponseDetail: "GetObjectInfoForAllResults",
            }
        ];

        searchResults.FilterType = [global.constants.FILTER_BY_ACTIVITIES];
        searchResults.PageSize = 1;
        searchResults.CurrentPage = 1;
        searchResults.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        searchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        searchResults.IgnorePriceAndAvailability = true;

        var response = await searchResults.GetSearchResults();
        var accommodationObjectInfo = [];
        var responseDetailed = null;

        if (typeof response.GetSearchResultsResult != "undefined") {
            if (response.GetSearchResultsResult.Status.Code == 'OK') {
                if (response.GetSearchResultsResult.AllResultsAdditionalData) {
                    if (response.GetSearchResultsResult.AllResultsAdditionalData.ObjectInfoList) {
                        if (Array.isArray(response.GetSearchResultsResult.AllResultsAdditionalData.ObjectInfoList.AccommodationObjectInfo)) {
                            if(response.GetSearchResultsResult.AllResultsAdditionalData.ObjectInfoList.AccommodationObjectInfo.length > 0) {
                                accommodationObjectInfo = response.GetSearchResultsResult.AllResultsAdditionalData.ObjectInfoList.AccommodationObjectInfo;

                                if(Array.isArray(accommodationObjectInfo)) {
                                    if(accommodationObjectInfo.length > 0) {
                                        for(const i in accommodationObjectInfo) {
                                            var getDetailedDescription = new GetDetailedDescription();
                                            getDetailedDescription.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
                                            getDetailedDescription.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
                                            getDetailedDescription.ObjectID = accommodationObjectInfo[i].ItravelObjectID;

                                            responseDetailed = await getDetailedDescription.GetDetailedDescription();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}