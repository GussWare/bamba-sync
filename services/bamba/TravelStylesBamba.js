import IconTravelsHelper from '../../helpers/IconTravelsHelper';
import IconTravelStylesLandingHelper from '../../helpers/IconTravelStylesLandingHelper';
import TravelStyleBackOffice from '../backoffice/TravelStyleBackOffice';

export default class TravelStylesBamba {

    constructor() {

    }

    async getAll(params) {
        var travelStyleServ = new TravelStyleBackOffice();
        var response = await travelStyleServ.getAll(params);
        var icons = [];


        if (response.body["travel-styles"]) {
            if (Array.isArray(response.body["travel-styles"].results)) {
                if (response.body["travel-styles"].results.length > 0) {
                    for (const i in response.body["travel-styles"].results) {

                        if (Array.isArray(response.body["travel-styles"].results[i].icons)) {
                            if (response.body["travel-styles"].results[i].icons.length > 0) {
                                icons = [];

                                for (const j in response.body["travel-styles"].results[i].icons) {
                                    icons.push(
                                        await IconTravelsHelper.getIconTravel(response.body["travel-styles"].results[i].icons[j])
                                    );
                                }

                                response.body["travel-styles"].results[i].icons = icons;
                            }
                        }

                        if (Array.isArray(response.body["travel-styles"].results[i].iconsLanding)) {
                            if (response.body["travel-styles"].results[i].iconsLanding.length > 0) {
                                icons = [];

                                for (const j in response.body["travel-styles"].results[i].iconsLanding) {
                                    icons.push(
                                        await IconTravelStylesLandingHelper.getIconTravel(response.body["travel-styles"].results[i].iconsLanding[j])
                                    );
                                }

                                response.body["travel-styles"].results[i].iconsLanding = icons;
                            }
                        }
                    }
                }
            }
        }

        return response;
    }

    async get(params) {
        var travelStyleServ = new TravelStyleBackOffice();
        var response = await travelStyleServ.get(params);
        var icons = [];

        if (response.body["travel style"]) {
            if (Array.isArray(response.body["travel style"].icons)) {
                if (response.body["travel style"].icons.length > 0) {

                    icons = [];

                    for (const j in response.body["travel style"].icons) {
                        icons.push(
                            await IconTravelsHelper.getIconTravel(response.body["travel style"].icons[j])
                        );
                    }

                    response.body["travel style"].icons = icons;
                }
            }
        }

        return response;
    }
}