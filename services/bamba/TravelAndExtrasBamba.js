import InsertReservationBamba from './InsertReservationBamba';
import TransportationListBamba from '../bamba/TransportationListBamba';
import FakeHelper from '../../helpers/FakeHelper';
import TravelAndExtrasSimpleTourLibrary from '../../libraries/TravelAndExtrasSimpleTourLibrary';
import TravelAndExtrasFitTourLibrary from '../../libraries/TravelAndExtrasFitTourLibrary';
import TravelAndExtrasActivityTourLibrary from '../../libraries/TravelAndExtrasActivityTourLibrary';

export default class TravelAndExtrasBamba {

    constructor() {

    }

    /**
     * 
     * @param {*} params 
     */
    async getTravelAndExtras(params) {

        const travelAndExtras = await this.getTravelAndExtrasForTour(params);

        if (!travelAndExtras.AirportPickup) {
            const transportationListBamba = new TransportationListBamba();
            transportationListBamba.setStartDate(params.StartDate);
            transportationListBamba.setEndDate(params.EndDate);
            transportationListBamba.setChildrenAgeList(await FakeHelper.childrenAge(params.Childrens));
            transportationListBamba.setNumberOfPersons(params.NumberOfPersons);
            transportationListBamba.setPickupDestinationIDList(travelAndExtras.StartDestinationID);
            transportationListBamba.setDropoffDestinationIDList(travelAndExtras.StartDestinationID);
            transportationListBamba.setCategoryIDListUnion(global.constants.CATEGORY_UNION_AIRPORT);
            transportationListBamba.setInPriceType(global.constants.DEFAULT_INPRICE_TYPE);
            transportationListBamba.setCurrencyID(params.CurrencyID);
            transportationListBamba.setPageSize(1);
            transportationListBamba.setCurrentPage(1);
            transportationListBamba.setLanguageID(params.LanguageID);
            travelAndExtras.TransportationList = await transportationListBamba.getFirstTransport();
        }

        return travelAndExtras;
    }

    /**
     * 
     * @param {*} tourID 
     * @param {*} type 
     */
    async getTravelAndExtrasForTour(params) {
        var tour = {};
        var travelAndExtras = null;

        switch (parseInt(params.Type)) {
            case global.constants.TOUR_TYPE_SIMPLE_TOUR:
                travelAndExtras = new TravelAndExtrasSimpleTourLibrary();
                tour = await travelAndExtras.getTravelAndExtras(params);
                break;
            case global.constants.TOUR_TYPE_FIT_TOUR:
                travelAndExtras = new TravelAndExtrasFitTourLibrary();
                tour = await travelAndExtras.getTravelAndExtras(params);
                break;
            case global.constants.TOUR_TYPE_ACTIVITIES_TOUR:
                travelAndExtras = new TravelAndExtrasActivityTourLibrary();
                tour = await travelAndExtras.getTravelAndExtras(params);
                break;
            default:
                break;
        }

        return tour;
    }
}