import CouponsAndDiscountsActivityLibrary from '../../libraries/CouponsAndDiscountsActivityLibrary';
import CouponsAndDiscountsTourLibrary from '../../libraries/CouponsAndDiscountsTourLibrary';
import HtmlHelper from '../../helpers/HtmlHelper';
import CuponAndDiscountInterface from '../../interface/cuponsAndDiscountInterface';
import StringHelper from '../../helpers/StringHelper';
import LangHelper from '../../helpers/LanguageHelper';
import moment from 'moment';

export default class CouponAndDiscountBamba {

    constructor() {

        this.NoteDiscount = null;
        this.NoteCoupon = null;

        this.Coupon = null;
        this.PercentCoupon = null;
        this.SpecialOffer = null;
        this.AvailabilityStatus = null;

        this.DiscountPercent = null;

        this.CouponsAndDiscountsActivityLibrary = new CouponsAndDiscountsActivityLibrary();
        this.CouponsAndDiscountsTourLibrary = new CouponsAndDiscountsTourLibrary();
    }

    async getCouponAndDiscount(UnitID, params) {
        await this.getPackage(UnitID, params);
    }

    /**
     * Recover tour information to recover coupons and discounts
     * 
     * @param {int} tourID 
     * @param {int} type 
     * @returns {object}
     */
    async getPackage(UnitID, params) {
        var tourPackage = null;

        switch (parseInt(params.Type)) {
            case global.constants.TOUR_TYPE_SIMPLE_TOUR:
            case global.constants.TOUR_TYPE_FIT_TOUR:
                tourPackage = await this.CouponsAndDiscountsTourLibrary.getCuponAndDiscount(UnitID, params);
                break;
            case global.constants.TOUR_TYPE_ACTIVITIES_TOUR:
                tourPackage = await this.CouponsAndDiscountsActivityLibrary.getCuponAndDiscount(UnitID, params);
                break;
            default:
                break;
        }

        if (tourPackage) {
            this.NoteCoupon = tourPackage.noteCoupon;
            this.NoteDiscount = tourPackage.noteDiscount;
            this.SpecialOffer = tourPackage.specialOffer;
            this.AvailabilityStatus = tourPackage.availabilityStatus;
        }

        return tourPackage;
    }

    async getAvailabilityStatus() {
        return this.AvailabilityStatus;
    }

    async applyCouponDiscount(amount, params) {

        var response = {
            status: false,
            error: await LangHelper.lang('COUPON_AND_DISCOUNT_INVALID_COUPON'),
            data: await CuponAndDiscountInterface.getDiscountInterface()
        };

        if (this.NoteCoupon) {

            var codigoCoupon = await this.getHtmlCoupon(this.NoteCoupon.NoteText);
            var percent = await this.getHtmlPercent(this.NoteCoupon.NoteText);

            if (codigoCoupon && percent) {
                if (await this.validateDatesNow(this.NoteCoupon.ValidDateFrom, this.NoteCoupon.ValidDateTo)) {
                    if (await this.validateDatesTravel(params.StartDate, params.EndDate, this.NoteCoupon.TravelDateFrom, this.NoteCoupon.TravelDateTo)) {
                        if (params.Coupon.trim() == codigoCoupon.trim()) {
                            response.status = true;
                            response.error = null;
                            response.data.Amount = amount;
                            response.data.Percent = percent;
                            response.data.Total = await this.calculateDiscount(amount, percent);
                        } else {
                            response.status = false;
                            response.error = await LangHelper.lang('COUPON_AND_DISCOUNT_INVALID_COUPON_NAME');
                        }
                    } else {
                        response.status = false;
                        response.error = await LangHelper.lang('COUPON_AND_DISCOUNT_INVALID_DATES_TRAVEL');
                    }
                } else {
                    response.status = false;
                    response.error = await LangHelper.lang('COUPON_AND_DISCOUNT_INVALID_DATES_PURSHASE');
                }
            }
        }

        return response;
    }

    async applyDiscount(amount, params) {

        var response = {
            status: false,
            error: await LangHelper.lang('COUPON_AND_DISCOUNT_INVALID_COUPON'),
            data: await CuponAndDiscountInterface.getDiscountInterface()
        };

        if (this.NoteDiscount) {

            var percent = await this.getHtmlPercent(this.NoteDiscount.NoteText);

            if (percent) {
                if (await this.validateDatesNow(this.NoteDiscount.ValidDateFrom, this.NoteDiscount.ValidDateTo)) {
                    if (await this.validateDatesTravel(params.StartDate, params.EndDate, this.NoteDiscount.TravelDateFrom, this.NoteDiscount.TravelDateTo)) {
                        response.status = true;
                        response.error = null;
                        response.data.Amount = amount;
                        response.data.Percent = percent;
                        response.data.Total = await this.calculateDiscount(amount, percent);
                    } else {
                        response.status = false;
                        response.error = await LangHelper.lang('COUPON_AND_DISCOUNT_INVALID_DATES_TRAVEL');
                    }
                } else {
                    response.status = false;
                    response.error = await LangHelper.lang('COUPON_AND_DISCOUNT_INVALID_DATES_PURSHASE');
                }
            }
        }

        return response;
    }

    async applySpecialOffer(amount) {
        var response = {
            status: false,
            error: await LangHelper.lang('COUPON_AND_DISCOUNT_INVALID_COUPON'),
            data: await CuponAndDiscountInterface.getSpecialOfferInterface()
        };

        if (this.SpecialOffer) {
            var discount = this.SpecialOffer.Price;
            if (discount) {
                response.status = true;
                response.error = null;
                response.data.Amount = amount;
                response.data.Discount = discount;
                response.data.Total = await this.calculateSpecialOffer(amount, discount);
            }
        }

        return response;
    }

    async getHtmlCoupon(NoteText) {

        var coupon = null;
        var htmlParser = await HtmlHelper.getHtmlParser(NoteText);

        if (Array.isArray(htmlParser)) {
            if (htmlParser.length > 0) {
                for (const i in htmlParser) {
                    if (htmlParser[i].tagName == "h4") {

                        if (Array.isArray(htmlParser[i].childNodes)) {
                            if (htmlParser[i].childNodes.length > 0) {
                                coupon = htmlParser[i].childNodes[0].rawText;
                            }
                        }

                        break;
                    }
                }
            }
        }

        return coupon;
    }


    async getHtmlPercent(NoteText) {
        var percent = null;
        var htmlParser = await HtmlHelper.getHtmlParser(NoteText);

        if (Array.isArray(htmlParser)) {
            if (htmlParser.length > 0) {
                for (const i in htmlParser) {
                    if (htmlParser[i].tagName == "p") {

                        if (Array.isArray(htmlParser[i].childNodes)) {
                            if (htmlParser[i].childNodes.length > 0) {
                                percent = htmlParser[i].childNodes[0].rawText;
                                percent = await StringHelper.quitarUltimoCaracter(percent);
                            }
                        }

                        break;
                    }
                }
            }
        }

        return percent;
    }

    async validateDatesNow(dateFrom, dateTo) {

        var hoy = moment().format('YYYY-MM-DD');
        var dateFromMoment = moment(dateFrom).format('YYYY-MM-DD');
        var dateToMoment = moment(dateTo).format('YYYY-MM-DD');

        if (hoy >= dateFromMoment && hoy <= dateToMoment) {
            return true;
        } else {
            return false;
        }
    }

    async validateDatesTravel(dateStartCheckout, dateEndCheckout, dateTravelDateFrom, dateTravelDateTo) {

        var dateStartCheckoutMoment = moment(dateStartCheckout).format('YYYY-MM-DD');
        var dateEndCheckoutMomento = moment(dateEndCheckout).format('YYYY-MM-DD');
        var dateTravelDateFromMoment = moment(dateTravelDateFrom).format('YYYY-MM-DD');
        var dateTravelDateToMoment = moment(dateTravelDateTo).format('YYYY-MM-DD');

        if ((dateStartCheckoutMoment >= dateTravelDateFromMoment && dateStartCheckoutMoment <= dateTravelDateToMoment)
            && (dateEndCheckoutMomento >= dateTravelDateFromMoment && dateEndCheckoutMomento <= dateTravelDateToMoment)
            && (dateStartCheckoutMoment <= dateEndCheckoutMomento)) {
            return true;
        } else {
            return false;
        }
    }

    async calculateDiscount(amount, percent) {

        var total = (parseFloat(amount) - (parseFloat(amount) * (parseFloat(percent) / 100)));

        return total;
    }

    async calculateSpecialOffer(amount, discount) {

        var amount = parseFloat(amount);
        var discount = parseFloat(discount);

        if (discount < 0) {
            discount = (discount * -1);
        }

        var total = amount - discount;

        return total;
    }
}
