import HomeBackOffice from '../backoffice/HomeBackOffice';
import ArrayHelper from '../../helpers/ArrayHelper';
import TourDetailModel from '../../models/TourDetailModel';
import ShopHelper from '../../helpers/ShopHelper';

export default class HomeBamba {

    constructor() {

    }

    /**
     * Form an array with the necessary data for the home page
     * 
     * @param {*} params 
     * @returns {array}
     */
    async getHome(params) {
        const homeBackOffice = new HomeBackOffice();
        const response = await homeBackOffice.getAll(params);
        const homeResult = response.body.homes.results;

        var toursIDList = [];
        var toursData = [];

        for (const i in homeResult) {
            homeResult[i].search = await ArrayHelper.returnPositionRandom(homeResult[i].search);

            // recommendedForYou
            if (typeof homeResult[i].recommendedForYou != "undefined") {
                if (Array.isArray(homeResult[i].recommendedForYou)) {
                    if (homeResult[i].recommendedForYou.length > 0) {
                        for (const j in homeResult[i].recommendedForYou) {
                            if (homeResult[i].recommendedForYou[j].lemaxTourId) {
                                toursIDList.push(homeResult[i].recommendedForYou[j].lemaxTourId);
                            }
                        }
                    }
                }
            }

            // travelDeals
            if (typeof homeResult[i].travelDeals != "undefined") {
                if (Array.isArray(homeResult[i].travelDeals)) {
                    if (homeResult[i].travelDeals.length > 0) {
                        for (const j in homeResult[i].travelDeals) {
                            if (homeResult[i].travelDeals[j].lemaxTourId) {
                                toursIDList.push(homeResult[i].travelDeals[j].lemaxTourId);
                            }
                        }
                    }
                }
            }

            if (toursIDList.length > 0) {
                toursData = await TourDetailModel.find({ TourID: { $in: toursIDList } });
                for (const l in toursData) {

                    // quitamos fotos que no son necesarias
                    toursData[l].Galery = await ShopHelper.quitarFotosGallery(toursData[l].Galery);


                    // recommendedForYou
                    if (typeof homeResult[i].recommendedForYou != "undefined") {
                        if (Array.isArray(homeResult[i].recommendedForYou)) {
                            if (homeResult[i].recommendedForYou.length > 0) {
                                for (const j in homeResult[i].recommendedForYou) {
                                    if (homeResult[i].recommendedForYou[j].lemaxTourId == toursData[l].TourID) {
                                        homeResult[i].recommendedForYou[j].tour = toursData[l];
                                    }
                                }
                            }
                        }
                    }

                    // travelDeals
                    if (typeof homeResult[i].travelDeals != "undefined") {
                        if (Array.isArray(homeResult[i].travelDeals)) {
                            if (homeResult[i].travelDeals.length > 0) {
                                for (const j in homeResult[i].travelDeals) {
                                    if (homeResult[i].travelDeals[j].lemaxTourId == toursData[l].TourID) {
                                        homeResult[i].travelDeals[j].tour = toursData[l];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return homeResult;
    }
}