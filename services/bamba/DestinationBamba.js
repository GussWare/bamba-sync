import GetAllDestination from '../lemax/GetAllDestinations';
import LanguageHelper from '../../helpers/LanguageHelper';
import CountryModel from '../../models/CountryModel';
import CountryDestinationModel from '../../models/CountryDestinationModel';
import DestinationModel from '../../models/DestinationModel';

export default class DestinationBamba {

    constructor() {
        this.LanguageID = null;
    }

    /**
     * Save the information of the destinations as well as the relation of the destination and country
     * 
     * @returns {boolean}
     */
    async InsertRelationShip() {

        if (!this.LanguageID) {
            this.LanguageID = await LanguageHelper.getLang();
        }

        var getAllDestiation = null;
        var countryDestination = null;
        var destination = null;
        var response = null;
        var destinationList = [];
        var contaDestination = 0;
        var contaContryDestination = 0;
        var countriesList = await CountryModel.find();

        for (const i in countriesList) {
            if (countriesList[i].lemaxId) {
                getAllDestiation = new GetAllDestination();
                getAllDestiation.LanguageID = this.LanguageID;
                getAllDestiation.SelectedCountryID = countriesList[i].lemaxId;

                response = await getAllDestiation.GetAllDestinations();

                if (response.GetAllDestinationsResult.DestinationList) {
                    if (Array.isArray(response.GetAllDestinationsResult.DestinationList.Destination)) {
                        if (response.GetAllDestinationsResult.DestinationList.Destination) {
                            destinationList = response.GetAllDestinationsResult.DestinationList.Destination;
                            if (destinationList.length > 0) {
                                for (const j in destinationList) {
                                    contaDestination = await CountryDestinationModel.countDocuments({
                                        lemaxId: destinationList[j].DestinationID
                                    });

                                    if (contaDestination == 0) {
                                        destination = new DestinationModel();
                                        destination.lemaxId = destinationList[j].DestinationID;
                                        destination.name = destinationList[j].DestinationName;
                                        await destination.save();
                                    }

                                    contaContryDestination = await CountryDestinationModel.countDocuments({
                                        destinationLemaxId: destinationList[j].DestinationID
                                    });

                                    if (contaDestination == 0) {
                                        countryDestination = new CountryDestinationModel();
                                        countryDestination.countryLemaxId = countriesList[i].lemaxId;
                                        countryDestination.destinationLemaxId = destinationList[j].DestinationID;
                                        await countryDestination.save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return true;
    }
}