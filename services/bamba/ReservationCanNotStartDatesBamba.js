import GetReservationCanNotStartDates from '../lemax/GetReservationCanNotStartDates';
import DateHelper from '../../helpers/DateHelper';

export default class ReservationCanNotStartDatesBamba {

    constructor() {

    }

    /**
     * 
     * @param {*} params 
     * @returns {}
     */
    async getCanNotDates(params) {
        var listNotStartDays = [];

        const getReservationCanNotStartDates = new GetReservationCanNotStartDates();

        // para recuperar la lista de dias
        getReservationCanNotStartDates.UnitID = params.UnitID;

        if (typeof params.StartDate != "undefined" && typeof params.EndDate != "undefined") {
            if (params.StartDate && params.EndDate) {
                getReservationCanNotStartDates.StartDate = params.StartDate;
                getReservationCanNotStartDates.EndDate = params.EndDate;
            } else {
                getReservationCanNotStartDates.StartDate = await DateHelper.now();
                getReservationCanNotStartDates.EndDate = await DateHelper.addMounts(getReservationCanNotStartDates.StartDate, 12);
            }
        } else {
            getReservationCanNotStartDates.StartDate = await DateHelper.now();
            getReservationCanNotStartDates.EndDate = await DateHelper.addMounts(getReservationCanNotStartDates.StartDate, 12);
        }

        const response = await getReservationCanNotStartDates.GetReservationCanNotStartDates();
        if (response.GetReservationCanNotStartDatesResult) {
            if (response.GetReservationCanNotStartDatesResult.Status.Code == 'OK') {
                if (response.GetReservationCanNotStartDatesResult.ListDates) {
                    if (Array.isArray(response.GetReservationCanNotStartDatesResult.ListDates.dateTime)) {
                        listNotStartDays = response.GetReservationCanNotStartDatesResult.ListDates.dateTime;
                    }
                }
            }
        }

        return listNotStartDays;
    }
}