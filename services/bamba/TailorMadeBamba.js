import InsertReservationBamba from '../bamba/InsertReservationBamba';
import LanguageHelper from '../../helpers/LanguageHelper';
import FakeHelper from '../../helpers/FakeHelper';

export default class TailorMadeBamba {

    constructor() {

    }

    async insertReservation(params) {

        const insertReservationBamba = new InsertReservationBamba();

        insertReservationBamba.ExecuteInsert = true;

        if (params.CurrencyID) {
            insertReservationBamba.CurrencyID = params.CurrencyID;
        } else {
            insertReservationBamba.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (params.PaymentMethodID) {
            insertReservationBamba.setPaymentMethodID(params.PaymentMethodID);
        } else {
            insertReservationBamba.setPaymentMethodID(global.constants.DEFAULT_PAYMENT_METHOD_ID);
        }

        if (params.LanguageID) {
            insertReservationBamba.setLanguageID(params.LanguageID);
        } else {
            insertReservationBamba.setLanguageID(await LanguageHelper.getLang());
        }

        if (params.Customer) {
            insertReservationBamba.CustomerObject = params.Customer;
        }

        if (params.ReservationCustomFields) {
            insertReservationBamba.ReservationCustomFields = await this.prepareDataReservationCustomFilds(params.ReservationCustomFields);
        }

        insertReservationBamba.setAdHocItem(await FakeHelper.adhocFake(params.Customer));

        return await insertReservationBamba.InsertReservation();
    }

    async prepareDataReservationCustomFilds(reservationCustomFields) {

        var stringCustomFilds = '';
        var reservationCustomFiledsArray = [];

        if (Array.isArray(reservationCustomFields)) {
            for (const i in reservationCustomFields) {
                stringCustomFilds += reservationCustomFields[i].Etiqueta + '<br />';
                stringCustomFilds += reservationCustomFields[i].Valor + '<br />';
            }
        }

        reservationCustomFiledsArray.push({
            ID: global.constants.TAILOR_MADE_CUSTOM_FIELD_94,
            Value: stringCustomFilds
        });

        return reservationCustomFiledsArray;
    }
}