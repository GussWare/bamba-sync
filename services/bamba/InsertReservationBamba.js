import InsertReservation from '../lemax/InsertReservation';
import DateHelper from '../../helpers/DateHelper';

export default class InsertReservationBamba {

    constructor() {
        this.ExecuteInsert = null;
        this.PaymentMethodID = null;
        this.CurrencyID = null;
        this.LanguageID = null;
        this.Customer = null;
        this.CustomerObject = null;
        this.CustomerType = null;
        this.ReservationItems = [];
        this.AdHocReservationItems = [];
        this.ReservationCustomFields = [];
    }

    async setExecuteInsert(ExecuteInsert) {
        this.ExecuteInsert = ExecuteInsert;
    }

    async setPaymentMethodID(PaymentMethodID) {
        this.PaymentMethodID = PaymentMethodID;
    }

    async setCurrencyID(CurrencyID) {
        this.CurrencyID = CurrencyID;
    }

    async setLanguageID(LanguageID) {
        this.LanguageID = LanguageID;
    }

    async setCustomer(Customer) {
        this.Customer = Customer;
    }

    async setCustomerType(CustomerType) {
        this.CustomerType = CustomerType;
    }

    async setCustomerObject(CustomerObject){
        this.CustomerObject = CustomerObject;
    }

    async setReservationItems(ReservationItems) {
        if (ReservationItems) {
            if (Array.isArray(ReservationItems)) {
                for (const i in ReservationItems) {
                    this.ReservationItems.push(ReservationItems[i]);
                }
            } else {
                this.ReservationItems.push(ReservationItems);
            }
        }
    }

    async setAdHocItem(AdHocService) {
        if (AdHocService) {
            if (Array.isArray(AdHocService)) {
                for (const i in AdHocService) {
                    this.AdHocReservationItems.push(AdHocService[i]);
                }
            } else {
                this.AdHocReservationItems.push(AdHocService);
            }
        }
    }

    async setReservationCustomFields(ReservationCustomFields) {
        if(ReservationCustomFields) {
            if (Array.isArray(ReservationCustomFields)) {
                for (const i in ReservationCustomFields) {
                    this.ReservationCustomFields.push(ReservationCustomFields[i]);
                }
            } else {
                this.ReservationCustomFields.push(ReservationCustomFields);
            }
        }
    }

    /**
     * Method used to enter a new reservation in lemax
     * 
     * @param {boolean} ExecuteInsert
     * @param {int} PaymentMethodID
     * @param {int} CurrencyID Currency identifier
     * @param {string} LanguageID language identifier
     * @param {string} Customer
     * @param {array} ReservationItems
     * @returns {array}
     */
    async InsertReservation() {
        var insertReservation = new InsertReservation();

        if (this.ExecuteInsert) {
            insertReservation.ExecuteInsert = this.ExecuteInsert;
        }

        if (this.PaymentMethodID) {
            insertReservation.PaymentMethodID = this.PaymentMethodID;
        }

        if (this.CurrencyID) {
            insertReservation.CurrencyID = this.CurrencyID;
        }

        if (this.LanguageID) {
            insertReservation.LanguageID = this.LanguageID;
        }

        if (this.CustomerType) {
            insertReservation.CustomerType = this.CustomerType;
        }

        if (this.Customer) {
            insertReservation.Customer = this.Customer;
        }

        if(this.CustomerObject) {
            insertReservation.CustomerObject = this.CustomerObject; 
        }

        if (Array.isArray(this.ReservationItems)) {
            if (this.ReservationItems.length > 0) {
                insertReservation.ReservationItems = this.ReservationItems;
            }
        }

        if (Array.isArray(this.SelectedServices)) {
            if (this.SelectedServices.length > 0) {
                insertReservation.SelectedServices = this.SelectedServices;
            }
        }

        if (Array.isArray(this.ReservationCustomFields)) {
            if (this.ReservationCustomFields.length > 0) {
                insertReservation.ReservationCustomFields = this.ReservationCustomFields;
            }
        }

        if (Array.isArray(this.AdHocReservationItems)) {
            if (this.AdHocReservationItems.length > 0) {
                insertReservation.AdHocReservationItems = this.AdHocReservationItems;
            }
        }

        return await insertReservation.InsertReservation();
    }

    /**
     * Method that is used to recover the data of the components of a fit tour
     * 
     * @param {int} unitID PackageUnit identifier
     * @param {int} serviceID service identifier
     * @param {int} currencyID Currency identifier
     * @param {int} paymentMethodID payment method
     * @param {string} languageID language identifier
     * @returns {array} 
     */
    async getComponents(unitID, serviceID, currencyID, paymentMethodID, languageID) {

        this.ExecuteInsert = false;
        this.PaymentMethodID = paymentMethodID;
        this.CurrencyID = currencyID;
        this.LanguageID = languageID;
        this.Customer = global.constants.CUSTOMER_EMAIL;
        this.CustomerType = global.constants.CUSTOMER_TYPE;

        var startDate = await DateHelper.now();

        this.ReservationItems.push({
            ReservationItemOrder: 1,
            UnitID: unitID,
            StartDate: startDate,
            EndDate: await DateHelper.addYear(startDate),
            Passengers: [
                {
                    Name: 'Adult1',
                    Surname: 'Test',
                    DateOfBirth: '1980-01-01'
                }
            ],
            SelectedServices: [
                {
                    ServiceID: serviceID
                }
            ]
        });

        var components = [];
        var InsertResponse = await this.InsertReservation();

        if (InsertResponse.InsertReservationResult.Status.Code == 'OK') {
            if (typeof InsertResponse.InsertReservationResult.ReservationItems != "undefined") {
                if (InsertResponse.InsertReservationResult.ReservationItems) {
                    if (InsertResponse.InsertReservationResult.ReservationItems.InsertReservationItemRS.length > 0) {
                        components = InsertResponse.InsertReservationResult.ReservationItems.InsertReservationItemRS;
                    }
                }
            }
        }

        return components;
    }

    async getPriceForServiceList(UnitID, Services, StratDate, EndDate, CurrencyID, PaymentMethodID, LanguageID) {
        this.ExecuteInsert = false;
        this.PaymentMethodID = PaymentMethodID;
        this.CurrencyID = CurrencyID;
        this.LanguageID = LanguageID;
        this.Customer = global.constants.CUSTOMER_EMAIL;
        this.CustomerType = global.constants.CUSTOMER_TYPE;

        var ReservationItems = {}
        ReservationItems.Passengers = [];
        ReservationItems.SelectedServices = [];
        ReservationItems.ReservationItemOrder = 1;
        ReservationItems.UnitID = UnitID;
        ReservationItems.StartDate = StratDate;
        ReservationItems.EndDate = EndDate;
        ReservationItems.Passengers.push(
            {
                Name: 'Adult1',
                Surname: 'Test',
                DateOfBirth: '1980-01-01'
            }
        );

        if (Services.length > 0) {
            for (const i in Services) {
                ReservationItems.SelectedServices.push(
                    {
                        ServiceID: Services[i],
                        Amount: 1,
                    }
                );
            }
        }

        this.ReservationItems.push(ReservationItems);

        var components = [];
        var InsertResponse = await this.InsertReservation();
        var additionalServiceList = [];

        if (InsertResponse.InsertReservationResult.Status.Code == 'OK') {
            if (typeof InsertResponse.InsertReservationResult.ReservationItems != "undefined") {
                if (InsertResponse.InsertReservationResult.ReservationItems) {
                    if (InsertResponse.InsertReservationResult.ReservationItems.InsertReservationItemRS.length > 0) {
                        components = InsertResponse.InsertReservationResult.ReservationItems.InsertReservationItemRS;

                        if (Array.isArray(components)) {
                            if (components.length) {
                                for (const i in components) {

                                    if (components[i].ReservationItemDetailsList) {
                                        if (Array.isArray(components[i].ReservationItemDetailsList.InsertReservationItemDetailRS)) {
                                            for (const l in components[i].ReservationItemDetailsList.InsertReservationItemDetailRS) {
                                                additionalServiceList.push({
                                                    ServiceID:components[i].ReservationItemDetailsList.InsertReservationItemDetailRS[l].Service.ServiceID,
                                                    Price:components[i].ReservationItemDetailsList.InsertReservationItemDetailRS[l].SellingPrice,
                                                    PriceFormated:components[i].ReservationItemDetailsList.InsertReservationItemDetailRS[l].SellingPrice
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return additionalServiceList;
    }

}