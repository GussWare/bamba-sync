import request from 'request';
import TourDetailEntity from '../../entity/TourDetailEntity';
import LanguageHelper from '../../helpers/LanguageHelper';

export default class BambaSyncBamba {

    constructor() {

    }

    getTours() {

        return new Promise((resolve, reject) => {

            var url = global.config.bambasync.url + '/api/v1/tour-details';

            var options = {
                method: 'GET',
                url: url,
                json: true,
            };

            console.log("options", options);

            request(options, (error, response, body) => {
                if (error) {
                    reject({
                        status: global.constants.STATUS_500_INTERNAL_SERVER_ERROR,
                        body: {
                            error: LangHelper.lang('ERROR_CONNECTION_KEYSTONE')
                        }
                    });
                } else {
                    resolve({
                        status: response.statusCode,
                        body: body
                    });
                }
            });

        });
    }

    async syncToursBamba() {
        var toursBambaSync = await this.getTours();
        var response = {
            success: false,
            totalTours: 0,
            messages: [],
        };

        if (toursBambaSync.status) {
            if (Array.isArray(toursBambaSync.body)) {
                if (toursBambaSync.body.length > 0) {
                    response.totalTours = toursBambaSync.body.length;
                    response.success = true;

                    var tourDetailEntity = new TourDetailEntity();
                    for (const i in toursBambaSync.body) {
                        await tourDetailEntity.save(toursBambaSync.body[i]);
                    }
                }
            }
        } else {
            response.messages.push({ messages: await LanguageHelper.lang('BAMBA_SYNC_BAMBA_NO_TOURS') });
        }

        return response;
    }
}