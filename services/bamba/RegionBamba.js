import RegionBackOffice from '../backoffice/RegionBackOffice';

export default class RegionBamba {

    constructor() {

    }

    /**
     * Retrieve list of regions for the menu
     * 
     * @param {*} params 
     */
    async GetRegionMenuList(params) {

        var totalCountries = params.totalCountries || 6;
        var dataResponse = [];
        var regionKeyCountry = [];
        var totalCount = 0;

        var regionBackOffice = new RegionBackOffice();
        var response = await regionBackOffice.getAll(params);

        if (totalCountries > 0) {
            if (response.body.regions.last > 0) {
                var results = response.body.regions.results;

                for (const i in results) {

                    totalCount = (totalCountries > results[i].regionKeyCountry.length) ? results[i].regionKeyCountry.length : totalCountries;
                    regionKeyCountry = [];

                    for (var j = 0; j < totalCount; j++) {
                        regionKeyCountry.push(results[i].regionKeyCountry[j]);
                    }

                    results[i].regionKeyCountry = regionKeyCountry;
                    dataResponse.push(results[i]);
                }
            }
        } else {
            dataResponse = response.body.regions.results;
        }

        return dataResponse;
    }

    /**
     * Retrieve a region based on its id with related countries
     * 
     * @param {*} id 
     */
    async GetRegionMenu(id, totalCountries) {
        var params = {
            id: id,
            retrieveShow: "_id title regionKeyCountry"
        };

        var regionKeyCountry = [];
        var regionBackOffice = new RegionBackOffice();
        var response = await regionBackOffice.get(params);
        var region = response.body;

        if (!totalCountries) {
            totalCountries = 8;
        }

        if (totalCountries > 0) {
            var totalCount = (totalCountries > region.region.regionKeyCountry.length) ? region.region.regionKeyCountry.length : totalCountries;
            if (region.region.regionKeyCountry.length > 0) {
                regionKeyCountry = [];
                for (var i = 0; i < totalCount; i++) {
                    regionKeyCountry.push(region.region.regionKeyCountry[i]);
                }
                region.region.regionKeyCountry = regionKeyCountry;
            }
        }

        return region;
    }
}