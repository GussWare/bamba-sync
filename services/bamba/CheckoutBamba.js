import ReservationCanNotStartDatesBamba from './ReservationCanNotStartDatesBamba';
import CheckOutSimpleTourLibrary from '../../libraries/CheckOutSimpleTourLibrary';
import CheckOutActivitiesTourLibrary from '../../libraries/CheckOutActivitiesTourLibrary';
import CheckOutFitTourLibrary from '../../libraries/CheckOutFitTourLibrary';


export default class CheckoutBamba {

    constructor() {

        this.CheckOutSimpleTourLibrary = new CheckOutSimpleTourLibrary();
        this.CheckOutActivitiesTourLibrary = new CheckOutActivitiesTourLibrary();
        this.CheckOutFitTourLibrary = new CheckOutFitTourLibrary();
    }

    /**
     * It is responsible for retrieving the information from the checkout tab for the three types of tours
     * 
     * @param {object} params 
     * @return {object}
     */
    async getCheckout(params) {

        const customize = await this.getPackage(params);

        // datos de listado de dias
        const reservationCanNotStartDates = new ReservationCanNotStartDatesBamba();
        customize.ListNotStartDays = await reservationCanNotStartDates.getCanNotDates({
            UnitID: customize.UnitID,
            StartDate: params.StartDate,
            EndDate: params.EndDate
        });

        return customize;
    }

    /**
     * It is responsible for verifying what type of tour is the one that must be recovered to process the checkout information
     * 
     * @param {int} tourID 
     * @param {int} type 
     * @returns {object}
     */
    async getPackage(params) {
        var tour = {};

        switch (parseInt(params.Type)) {
            case global.constants.TOUR_TYPE_SIMPLE_TOUR:
                tour = await this.CheckOutSimpleTourLibrary.getCheckOut(params);
                break;
            case global.constants.TOUR_TYPE_FIT_TOUR:
                tour = await this.CheckOutFitTourLibrary.getCheckOut(params);
                break;
            case global.constants.TOUR_TYPE_ACTIVITIES_TOUR:
                tour = await this.CheckOutActivitiesTourLibrary.getCheckOut(params);
                break;
            default:
                break;
        }

        return tour;
    }
}