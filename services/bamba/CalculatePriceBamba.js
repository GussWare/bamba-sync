import CalculatePriceInterface from '../../interface/calculatePriceInterface';
import InsertReservationBamba from '../bamba/InsertReservationBamba';
import FakeHelper from '../../helpers/FakeHelper';
import CouponAndDiscountBamba from './CouponAndDiscountBamba';
import NumberHelper from '../../helpers/NumberHelper';

export default class CalculatePriceBamba {

    constructor() {
        this.CouponAndDiscountBamba = new CouponAndDiscountBamba();
    }

    /**
     * 
     * @param {*} params 
     */
    async calculatedPrice(params) {

        const insertReservationBamba = new InsertReservationBamba();
        insertReservationBamba.setExecuteInsert(false);
        insertReservationBamba.setPaymentMethodID(params.PaymentMethodID);
        insertReservationBamba.setLanguageID(params.LanguageID);
        insertReservationBamba.setCurrencyID(params.CurrencyID);
        insertReservationBamba.setCustomer(global.constants.CUSTOMER_EMAIL);
        insertReservationBamba.setCustomerType(global.constants.CUSTOMER_TYPE);

        if (params.ReservationItems) {
            insertReservationBamba.setReservationItems(await FakeHelper.reservationItems(params.ReservationItems));
        }

        if (params.AdHocItems) {
            insertReservationBamba.setAdHocItem(await FakeHelper.reservationItems(params.AdHocItems));
        }

        var InsertResponse = await insertReservationBamba.InsertReservation();
        var Price = await CalculatePriceInterface.getCalculatePriceInterface();

        if (InsertResponse.InsertReservationResult.Status.Code == 'OK') {
            Price.TotalPrice = InsertResponse.InsertReservationResult.SellingPrice;
            Price.PriceFormated = await NumberHelper.formatNumber(InsertResponse.InsertReservationResult.SellingPrice, 2);

            //no se ocupa el netprice
            //Price.NetPrice = InsertResponse.InsertReservationResult.NetPrice;

            if (typeof InsertResponse.InsertReservationResult.ReservationItems != "undefined") {

                if (Array.isArray(InsertResponse.InsertReservationResult.ReservationItems.InsertReservationItemRS)) {
                    if (InsertResponse.InsertReservationResult.ReservationItems.InsertReservationItemRS.length > 0) {
                        var insertReservationItemRS = null;
                        for (const i in InsertResponse.InsertReservationResult.ReservationItems.InsertReservationItemRS) {
                            insertReservationItemRS = InsertResponse.InsertReservationResult.ReservationItems.InsertReservationItemRS[i];

                            if (typeof insertReservationItemRS.PriceCalculationStatus != "undefined") {
                                Price.Messages.push({
                                    ServiceID: insertReservationItemRS.ReservationItemDetailsList.InsertReservationItemDetailRS[0].Service.ServiceID,
                                    Code: insertReservationItemRS.PriceCalculationStatus.Code,
                                    Description: insertReservationItemRS.PriceCalculationStatus.Description
                                });
                            }
                        }
                    }
                }
            }

            if (params.TourID && params.CouponsAndDiscount) {

                await this.CouponAndDiscountBamba.getCouponAndDiscount(params.ReservationItems[0].UnitID, params);

                Price.AvailabilityStatus = await this.CouponAndDiscountBamba.getAvailabilityStatus();

                var specialOfferResponse = await this.CouponAndDiscountBamba.applySpecialOffer(Price.TotalPrice);
                var discountResponse = await this.CouponAndDiscountBamba.applyDiscount(Price.TotalPrice, params.CouponsAndDiscount);

                if (specialOfferResponse.status) {

                    Price.PriceOriginal = InsertResponse.InsertReservationResult.SellingPrice;
                    Price.PriceOriginalFormated = await NumberHelper.formatNumber(InsertResponse.InsertReservationResult.SellingPrice, 2);

                    Price.SpecialOffer = specialOfferResponse.data;
                    Price.TotalPrice = specialOfferResponse.data.Total;
                    Price.PriceFormated = await NumberHelper.formatNumber(specialOfferResponse.data.Total, 2);

                } else if (discountResponse.status) {
                    Price.PriceOriginal = InsertResponse.InsertReservationResult.SellingPrice;
                    Price.PriceOriginalFormated = await NumberHelper.formatNumber(InsertResponse.InsertReservationResult.SellingPrice, 2);

                    Price.Discount = discountResponse.data;
                    Price.TotalPrice = discountResponse.data.Total;
                    Price.PriceFormated = await NumberHelper.formatNumber(discountResponse.data.Total, 2);
                } else if (params.CouponsAndDiscount.Coupon) {

                    var couponResponse = await this.CouponAndDiscountBamba.applyCouponDiscount(Price.TotalPrice, params.CouponsAndDiscount);
                    if (couponResponse.status) {
                        Price.PriceOriginal = InsertResponse.InsertReservationResult.SellingPrice;
                        Price.PriceOriginalFormated = await NumberHelper.formatNumber(InsertResponse.InsertReservationResult.SellingPrice, 2);

                        Price.Coupon = couponResponse.data;
                        Price.TotalPrice = couponResponse.data.Total;
                        Price.PriceFormated = await NumberHelper.formatNumber(couponResponse.data.Total, 2);
                    } else {
                        Price.Messages = [{
                            ServiceID: null,
                            Code: "Error",
                            Description: couponResponse.error,
                        }];
                    }
                }
            }

        } else {
            Price.Messages.push({
                ServiceID: null,
                Code: InsertResponse.InsertReservationResult.Status.Code,
                Description: InsertResponse.InsertReservationResult.Status.Description,
            });
        }

        return Price;
    }
}