import GetAllSEOData from '../lemax/GetAllSEOData';
import SeoDataModel from '../../models/SeoDataModel';

export default class SeoDataBamba {

    constructor() {

    }

    /**
     * 
     */
    async getAllSeoData() {

        var seoData = [];

        seoData = await SeoDataModel.find().limit(5);

        return seoData;
    }

    /**
     * 
     */
    async loadSeoData() {

        var seoData = await this.getAllSeoData();
        var contaSeoItem = 0;
        var seoModel = null;

        if(Array.isArray(seoData)) {
            if(seoData.length > 0) {
                for(const i in seoData) {

                    if(seoData[i].ObjectTypeID == global.constants.TOUR_TYPE_SIMPLE_TOUR 
                        || seoData[i].ObjectTypeID == global.constants.TOUR_TYPE_FIT_TOUR 
                        || seoData[i].ObjectTypeID == global.constants.TOUR_TYPE_ACTIVITIES_TOUR) {

                        contaSeoItem = await SeoDataModel.countDocuments({
                            lemaxId: seoData[i].ObjectID,
                            typeId: seoData[i].ObjectTypeID
                        });
    
                        if(contaSeoItem == 0) {
                            seoModel = new SeoDataModel();
                            seoModel.lemaxId = seoData[i].ObjectID;
                            seoModel.typeId = seoData[i].ObjectTypeID;
                            seoModel.title = seoData[i].Title;
                            seoModel.languageId = seoData[i].Language.LanguageID;
                            await seoModel.save();
                        }
                    }
                }
            }
        }

        return true;
    }
}