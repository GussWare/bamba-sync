import CheckUserCredentials from '../lemax/CheckUserCredentials';
import RequestResetPasswordEmail from '../lemax/RequestResetPasswordEmail';
import SetUserResetPassword from '../lemax/SetUserResetPassword';
import UserInterface from '../../interface/usuarioInterface';
import JwtHelper from '../../helpers/JwtHelper';

export default class LoginBamba {

    constructor() {

    }

    /**
     * You can check if customer credentials are valid by sending username (Customer email) and password customer entered
     * 
     * @param {object} params 
     * @returns {object}
     */
    async CheckUserCredentials(params) {

        const checkUserCredentials = new CheckUserCredentials();

        if(params.Email) {
            checkUserCredentials.Email = params.Email;
        }

        if(params.Password) {
            checkUserCredentials.Password = params.Password;
        }

        var response = await checkUserCredentials.CheckUserCredentials();

        var user = await UserInterface.getLoginInterface();  
        user.Status =  response.CheckUserCredentialsResult.Status;

        if(response.CheckUserCredentialsResult.Status.Code == 'OK') {
            var userResponse = response.CheckUserCredentialsResult.User;

            if(userResponse) {
               var UserData = Object.assign(await UserInterface.getUsuarioInterface(), userResponse);
            }

            user.Token = await JwtHelper.encode(UserData);
        } 

        return user;
    }

    /**
     * 
     * @param {object} params 
     * @returns {object}
     */
    async RequestResetPasswordEmail(params) {

        const requestResetPasswordEmail = new RequestResetPasswordEmail();

        if(params.Username) {
            requestResetPasswordEmail.Username = params.Username;
        }

        var response = await requestResetPasswordEmail.RequestResetPasswordEmail();

        return response.RequestResetPasswordEmailResult;
    }

    /**
     * 
     * @param {*} params 
     * @returns {object}
     */
    async SetUsertResetPasswordEmail(params) {

        const setUserResetPassword = new SetUserResetPassword();

        if(params.ResetPasswordToken) {
            setUserResetPassword.ResetPasswordToken = params.ResetPasswordToken;
        }

        if(params.NewPassword) {
            setUserResetPassword.NewPassword = params.NewPassword;
        }

        var response = await setUserResetPassword.SetUserResetPassword();

        return response.SetUserResetPasswordResult;
    }
}
