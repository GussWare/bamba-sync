import PackageSearchResults from '../lemax/GetPackageSearchResults';
import GetSearchResults from '../lemax/GetSearchResults';
import SimpleTourLibrary from '../../libraries/SimpleTourLibrary';
import FitTourLibrary from '../../libraries/FitTourLibrary';
import ActivitiesTourLibrary from '../../libraries/ActivitiesTourLibrary';
import LangHelper from '../../helpers/LanguageHelper';
import TourDetailEntity from '../../entity/TourDetailEntity';
import TourInterface from '../../interface/tourInterface';

export default class PackageBamba {

    constructor() {

    }

    /**
     * 
     * @param {*} params 
     */
    async getPackage(params) {

        const tourDetailEntity = new TourDetailEntity();

        var tour = await tourDetailEntity.getTour({
            TourID: params.TourID
        });

        if (!tour) {
            switch (parseInt(params.Type)) {
                case global.constants.TOUR_TYPE_SIMPLE_TOUR:
                case global.constants.TOUR_TYPE_FIT_TOUR:
                    tour = await this.getTour(params);
                    break;
                case global.constants.TOUR_TYPE_ACTIVITIES_TOUR:
                    tour = await this.getActivity(params);
                    break;
                default:
                    break;
            }

            // guardamos en la base de datos
            if (tour.TourID) {
                await tourDetailEntity.save(tour);
            }
        }

        return (tour) ? tour : {};
    }

    /**
     * 
     * @param {*} params 
     */
    async getTour(params) {
        var tour = {};
        var tourResult = {};
        var searchResults = new PackageSearchResults();

        if (params.TourID) {
            searchResults.PackageTourIDList.push(params.TourID);
        }

        if (params.LanguageID) {
            searchResults.LanguageID = params.LanguageID;
        } else {
            searchResults.LanguageID = await LangHelper.getLang();
        }

        if (params.CurrencyID) {
            searchResults.CurrencyID = params.CurrencyID;
        } else {
            searchResults.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (params.InPriceType) {
            searchResults.InPriceType = params.InPriceType;
        } else {
            searchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        searchResults.ShowFileds = [
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 11
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 11
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 10
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 10
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 10
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 10
            },

            {
                ResponseDetail: "GetHotels",
                NumberOfResults: 10
            },

            /*
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 10
            },
            
            {
                ResponseDetail: "ObjectTitle",
                NumberOfResults: 10
            },
            
            {
                ResponseDetail: "MapCoordinates",
                NumberOfResults: 10
            },
            
            {
                ResponseDetail: "ObjectSEODescription",
                NumberOfResults: 10
            },
            {
                ResponseDetail: "GetObjectInfoForAllResults",
                NumberOfResults: 10
            },
            {
                ResponseDetail: "GetPricesInAllActiveCurrencies",
                NumberOfResults: 10
            }*/
        ]

        var response = await searchResults.GetPackageSearchResults();
        var GetPackageSearchResultsResult = response.GetPackageSearchResultsResult;

        if (GetPackageSearchResultsResult.Status.Code == 'OK') {

            if (typeof GetPackageSearchResultsResult.PackageTourList != "undefined") {
                if (GetPackageSearchResultsResult.PackageTourList) {

                    if (Array.isArray(GetPackageSearchResultsResult.PackageTourList.PackageTour)) {
                        tourResult = GetPackageSearchResultsResult.PackageTourList.PackageTour[0];
                        var tourObject = null;

                        switch (tourResult.ObjectType.ObjectTypeID) {
                            case global.constants.TOUR_TYPE_SIMPLE_TOUR:
                                tourObject = new SimpleTourLibrary();
                                tour = await tourObject.getPackage(tourResult);
                                break;
                            case global.constants.TOUR_TYPE_FIT_TOUR:
                                tourObject = new FitTourLibrary();
                                tour = await tourObject.getPackage(tourResult);
                                break;
                            default:
                                tour = {};
                                break;
                        }
                    }
                }
            }
        }

        return tour;
    }

    /**
     * 
     */
    async getActivity(params) {
        var tour = {};
        var searchResults = new GetSearchResults();

        searchResults.ShowFileds = [
            {
                ResponseDetail: "CalculatedPriceInfo",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "UnitDescription",
                NumberOfResults: 100
            },
            {
                ResponseDetail: "ObjectPhotos",
                NumberOfResults: 11
            },
            {
                ResponseDetail: "UnitPhotos",
                NumberOfResults: 11
            },
            {
                ResponseDetail: "ObjectDescription",
                NumberOfResults: 100
            }
        ];

        searchResults.LanguageID = params.LanguageID || await LangHelper.getLang();
        searchResults.FilterType = [global.constants.FILTER_BY_ACTIVITIES];

        if (typeof params.TourID != "undefined") {
            searchResults.ObjectIDList = [params.TourID];
        }

        if (typeof params.PageSize != "undefined") {
            searchResults.PageSize = params.PageSize;
        } else {
            searchResults.PageSize = 1;
        }

        if (typeof params.CurrentPage != "undefined") {
            searchResults.CurrentPage = params.CurrentPage;
        } else {
            searchResults.CurrentPage = 1;
        }

        if (typeof params.CurrencyID != "undefined") {
            searchResults.CurrencyID = params.CurrencyID;
        } else {
            searchResults.CurrencyID = global.constants.DEFAULT_CURRENCY_ID;
        }

        if (typeof params.InPriceType != "undefined") {
            searchResults.InPriceType = params.InPriceType;
        } else {
            searchResults.InPriceType = global.constants.DEFAULT_INPRICE_TYPE;
        }

        var response = await searchResults.GetSearchResults();
        if (typeof response.GetSearchResultsResult != "undefined") {
            if (response.GetSearchResultsResult.Status.Code == 'OK') {
                if (response.GetSearchResultsResult.AccommodationObjectList) {
                    var activityTourLibrary = new ActivitiesTourLibrary();
                    tour = await activityTourLibrary.getPackage(response.GetSearchResultsResult);
                }
            }
        }

        return tour;
    }
}