import PackageSearchResults from '../lemax/GetPackageSearchResults';
import LanguageHelper from '../../helpers/LanguageHelper';
import AttributeGroupLibrary from '../../libraries/AttributeGroupLibrary';

export default class ChildrenToursBamba {

    constructor() {
        this.AttributeGroupLibrary = new AttributeGroupLibrary();
    }

    /**
     * 
     */
    async getChildrenTours(params) {

        const packageSearch = new PackageSearchResults();
        
        packageSearch.ParentIDFilter = params.ParentIDFilter;
        packageSearch.ShowFileds = params.ShowFileds;
        packageSearch.LanguageID = params.LanguageID;
        packageSearch.PageSize = params.PageSize;
        packageSearch.CurrentPage = params.CurrentPage;

        if(params.ShowFileds) {
            packageSearch.ShowFileds = params.ShowFileds;
        } else {
            packageSearch.ShowFileds =   [
                {
                    ResponseDetail: "ObjectPhotos",
                    NumberOfResults: 10
                },
                {
                    ResponseDetail: "UnitPhotos",
                    NumberOfResults: 10
                },
                {
                    ResponseDetail: "ObjectDetailedAttributes",
                    NumberOfResults: 100
                },
                {
                    ResponseDetail: "MapCoordinates",
                    NumberOfResults: 100
                },
                {
                    ResponseDetail: "GetHotels",
                    NumberOfResults: 100
                }
            ];
        }

        var response = await packageSearch.GetPackageSearchResults();
        var GetPackageSearchResultsResult = response.GetPackageSearchResultsResult;

        var childrenTours = [];
        if (GetPackageSearchResultsResult.Status.Code == 'OK') {
            if(GetPackageSearchResultsResult.PackageTourList) {
                childrenTours = GetPackageSearchResultsResult.PackageTourList.PackageTour;
            }
        }

        return childrenTours;
    }

    async getParentStars(TourID) {
        const packageSearch = new PackageSearchResults();
        
        packageSearch.ParentIDFilter = TourID;
        packageSearch.LanguageID = await LanguageHelper.getLang();
        packageSearch.PageSize = 100;
        packageSearch.CurrentPage = 1;

        packageSearch.ShowFileds =   [
            {
                ResponseDetail: "ObjectDetailedAttributes",
                NumberOfResults: 100
            }
        ];

        var response = await packageSearch.GetPackageSearchResults();
        var GetPackageSearchResultsResult = response.GetPackageSearchResultsResult;

        var childrenTours = [];
        var value = null;
        var listStars = [];
        if (GetPackageSearchResultsResult.Status.Code == 'OK') {
            if(GetPackageSearchResultsResult.PackageTourList) {
                childrenTours = GetPackageSearchResultsResult.PackageTourList.PackageTour;

                if(Array.isArray(childrenTours)){
                    if(childrenTours.length > 0){
                        
                        for(const i in childrenTours) {
                            value = await this.AttributeGroupLibrary.getAttribute(global.constantes.ATTRIBUTE_ACCOMODATION_TYPE, global.constantes.ATTRIBUTE_ACCOMODATION_TYPE, childrenTours[i].AttributeGroupList);

                            listStars.push(value);
                        }
                    }
                }
            }
        }

        return listStars;
    }
}