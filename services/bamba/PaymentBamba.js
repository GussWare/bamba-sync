import GetPaymentMethods from '../lemax/GetPaymentMethods';

export default class PaymentBamba {

    constructor() {

        this.LanguageID = null;
    }

    async getPaymentMethods() {

        var paymentMethods = new GetPaymentMethods();

        if (this.LanguageID) {
            paymentMethods.LanguageID = this.LanguageID;
        }

        var response = await paymentMethods.GetPaymentMethods();

        var payments = [];

        if (response.GetPaymentMethodsResult.Status.Code == 'OK') {
            if (Array.isArray(response.GetPaymentMethodsResult.PaymentMethodsList.PaymentMethod)) {
                for (const i in response.GetPaymentMethodsResult.PaymentMethodsList.PaymentMethod) {
                    if (parseInt(response.GetPaymentMethodsResult.PaymentMethodsList.PaymentMethod[i].PaymentMethodID) != 22) {
                        payments.push(response.GetPaymentMethodsResult.PaymentMethodsList.PaymentMethod[i]);
                    }
                }
            }
        }

        return payments;
    }
}