import GetReservation from '../lemax/GetReservation';
import ReservationInterface from '../../interface/reservationInterface';

export default class ReservationBamba {

    constructor() {

    }

    /**
     * 
     * 
     * @param {object} params 
     * @returns {object}
     */
    async getReservation(params) {

        const reservation = new GetReservation();

        if (params.ReservationID) {
            reservation.ReservationID = params.ReservationID;
        }

        if (params.LanguageID) {
            reservation.LanguageID = params.LanguageID;
        }

        const response = await reservation.GetReservation();

        var documents = await ReservationInterface.geDocumentsInterface();

        if (response.GetReservationResult) {
            console.log("squiiuiu si entraaa 1");
            if (typeof response.GetReservationResult.Reservation != "undefined") {

                const Reservation = response.GetReservationResult.Reservation;

                if (Reservation.DocumentList) {
                    if (typeof Reservation.DocumentList.Document != "undefined") {
                        if (Array.isArray(Reservation.DocumentList.Document)) {
                            for (const j in Reservation.DocumentList.Document) {
                                if (Reservation.DocumentList.Document[j].DocumentTypeID == global.constants.DOCUMENT_TYPE_ITINERARY) {
                                    if (Reservation.DocumentList.Document[j].DocumentPath) {
                                        documents.Itinerary.push(Reservation.DocumentList.Document[j].DocumentPath);
                                    }
                                }

                                if (Reservation.DocumentList.Document[j].DocumentTypeID == global.constants.DOCUMENT_TYPE_INVOICE) {
                                    if (Reservation.DocumentList.Document[j].DocumentPath) {
                                        documents.Invoice.push(Reservation.DocumentList.Document[j].DocumentPath);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return documents;
    }
}
