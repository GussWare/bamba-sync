import GetAllDestinations from '../lemax/GetAllDestinations';
import LanguageHelper from '../../helpers/LanguageHelper';
import CountryModel from '../../models/CountryModel';

export default class CountriesBamba {

    constructor() {
        this.LanguageID = null;
    }

    /**
     * Responsible for making a request to GetAllDestinations of lemax to retrieve the information of the countries 
     * and save them in the database 
     * 
     * @returns {boolean}
     */
    async InsertCountriesLemax() {

        var getAllDestination = new GetAllDestinations();
        getAllDestination.LanguageID = this.LanguageID || await LangHelper.getLang();

        var response = await getAllDestination.GetAllDestinations();

        if (response.GetAllDestinationsResult.CountryList) {
            if (response.GetAllDestinationsResult.CountryList.Country) {
                if (Array.isArray(response.GetAllDestinationsResult.CountryList.Country)) {
                    var contaCountry = 0;
                    var countryList = response.GetAllDestinationsResult.CountryList.Country;
                    var countryModel = null;

                    for (const i in countryList) {
                        contaCountry = await CountryModel.countDocuments({
                            lemaxId: countryList[i].CountryID
                        });

                        if (contaCountry == 0) {
                            countryModel = new CountryModel()
                            countryModel.name = countryList[i].CountryName;
                            countryModel.lemaxId = countryList[i].CountryID;
                            await countryModel.save();
                        } 
                    }
                }
            }
        }

        return true;
    }
}