'use strict'

module.exports = {

    base_url: '127.0.0.1',

    jwt: {
        secret: 'yxImk8?BNby_73A0(_Pp'
    },

    database: {
        host: '127.0.0.1',
        port: '27017',
        name: 'bamba-sync',
        user: "root",
        password: "Opl0PJDcHgX2"
    },

    lang: {
        default: 'en',
        path: 'languages'
    },

    mail: {
        host: 'smtp.mailtrap.io',
        user: '1ca1369d4d4535',
        password: '4fd7c6220d8165',
        port: '2525',
        from: 'noreply@noreply.com',
    },

    backoffice: {
        //api: 'http://localhost:3010/api',
        api: 'https://admin.pruebitas.com/api',
        uploads: '/home/bitnami/htdocs/backofficebamba-dev/uploads'
    },

    lemax: {
        // user: 'APIUserBamba',
        // pass: 'a7Cf0gyNH9',
        // url: 'https://bamba.itravelsoftware.com/itravel/API/WebService/iTravelAPI_3_0.asmx',
        // wsdl: 'http://bamba.itravelsoftware.com/itravel/API/WebService/iTravelAPI_3_0.asmx?WSDL'

        user: 'APIUserBambaTest',
        pass: 'Z8j667n2t8',
        url: 'http://bambatest.itravelsoftware.com/itravel/API/WebService/iTravelAPI_3_0.asmx',
        wsdl: 'http://bambatest.itravelsoftware.com/itravel/API/WebService/iTravelAPI_3_0.asmx?WSDL'
    },

    stripe:{
        private_token: "pk_test_pAaAeoGeuWY4csTBJNR2eCF9",
        public_token: "sk_test_AZiFLubLZDo6kSDucpvKaZL5"
    },

    bambasync:{
        url: 'http://localhost:3990'
    }
}; 