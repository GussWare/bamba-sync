'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TransactionSchema = Schema({
    StartDate: { type: Date, required: false },
    EndDate: { type: Date, required: false },
    Message: { type: String, required: false },
    Estatus: { type: String, required: false },
    NoTour: { type: Number, required: false },
    NoTourSuccess: { type: Number, required: false },
    NoTourError: { type: Number, required: false },
});

module.exports = mongoose.model('Transaction', TransactionSchema);