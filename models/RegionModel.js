'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RegionSchema = Schema({
    title: {
        type: String,
        required: true
    },
    allCountry: [{
        type: Schema.Types.ObjectId, ref: 'Country', many: true,
        required:true
    }],
});

module.exports = mongoose.model('Region', RegionSchema);