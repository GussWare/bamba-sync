'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LogModel = Schema({
    StartDate: { type: Date, required: false },
    EndDate: { type: Date, required: false },
});

module.exports = mongoose.model('log', LogModel);