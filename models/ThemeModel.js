'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ThemeSchema = Schema({
    name: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('Theme', ThemeSchema);