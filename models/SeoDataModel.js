'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SeoDataShema = Schema({
    lemaxId: { type: Number, required: true },
    typeId: { type: Number, required: true },
    title: { type: String, required: true },
    languageId: { type: String, required: true }
});

module.exports = mongoose.model('SeoData', SeoDataShema);