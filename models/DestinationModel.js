'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DestinationSchema = Schema({
    name: {type: String,required: true},
    lemaxId: { type: Number, required: true }
});

module.exports = mongoose.model('Destination', DestinationSchema);