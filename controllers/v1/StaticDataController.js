import express from 'express';
import LangMiddelWare from '../../middelwares/language';
import LangHelper from '../../helpers/LanguageHelper';
import StaticTourDetailBamba from '../../services/bamba/StaticTourDetailBamba';
import StaticTourPricesBamba from '../../services/bamba/StaticTourPricesBamba';
import FastestValidator from 'fastest-validator';

const router = express.Router();

/**
 * 
 */
router.post('/v1/sync-tour-detail', [LangMiddelWare.load], async (req, res) => {
    try {

        const params = req.body;

        const schema = {
            TourList: { type: "array" },
            LanguageID: { type: "string" },
            CurrencyID: { type: "number", positive: true, integer: true }
        };

        const validator = new FastestValidator();
        const check = validator.compile(schema);

        const valid = check({
            TourList: params.TourList,
            LanguageID: params.LanguageID,
            CurrencyID: parseInt(params.CurrencyID)
        });

        if (valid == true) {
            const staticTourDetailBamba = new StaticTourDetailBamba();
            const response = await staticTourDetailBamba.syncTourDetail(params);

            res.status(global.constants.STATUS_200_OK).send(response);

        } else {
            res.status(global.constants.STATUS_500_INTERNAL_SERVER_ERROR).send(valid);
        }

    } catch (error) {
        console.log(error);
        res.status(global.constants.STATUS_500_INTERNAL_SERVER_ERROR).send([{
            message: error
        }]);
    }
});

router.post('/v1/sync-prices', [LangMiddelWare.load], async (req, res) => {
    try {
        const params = req.body;

        if (valid == true) {
            const staticDataBamba = new StaticDataBamba();
            const response = await staticDataBamba.syncPrices(params);

            res.status(global.constants.STATUS_200_OK).send(response);

        } else {
            res.status(global.constants.STATUS_500_INTERNAL_SERVER_ERROR).send(valid);
        }

    } catch (error) {
        console.log(error);
        res.status(global.constants.STATUS_500_INTERNAL_SERVER_ERROR).send([{
            message: await LangHelper.lang('ERROR_MESSAGE_GENERIC')
        }]);
    }
});

module.exports = router;