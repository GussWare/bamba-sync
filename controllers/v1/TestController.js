import express from 'express';
import moment from 'moment';
import NumberHelper from '../../helpers/NumberHelper';

const router = express.Router();

/**
 * Test controller
 */
router.get('/v1/test', async (req, res) => {

    var n = await NumberHelper.formatNumber(3.1, 2);

    res.status(global.constants.STATUS_200_OK).send({number:n});
});

module.exports = router;