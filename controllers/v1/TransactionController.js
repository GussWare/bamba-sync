import express from 'express';
import TransactionModel from '../../models/TransactionModel';
import LogModel from '../../models/LogModel';

const router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId

router.post('/v1/transactions', async (req, res) => {

    try {
        var params = req.body;
        var filter = {};

        if(params.StartDate) {
            filter.StartDate =  {"$gte":params.StartDate};
        }

        if(params.EndDate) {
            filter.EndDate =  {"$lte":params.EndDate};
        }

        if(params.Estatus) {
            filter.Estatus = params.Estatus;
        }

        console.log("Filtro", filter);

        var response = await TransactionModel.find(filter);

        res.status(global.constants.STATUS_200_OK).send(response);
        
    } catch (error) {
        console.log(error);
        res.status(global.constants.STATUS_500_INTERNAL_SERVER_ERROR).send([{
            message: await LangHelper.lang('ERROR_MESSAGE_GENERIC')
        }]);
    }
});

router.post('/v1/log', async (req, res) => {

    try {
        var params = req.body;
        var filter = {};

        if(params.TransactionID) {
            filter.TransactionID =  new ObjectId(params.TransactionID);
        }

        if(params.TourID) {
            filter.TourID =  parseInt(params.TourID);
        }

        if(params.Type) {
            filter.Type =  parseInt(params.Type);
        }

        if(params.LanguageID) {
            filter.LanguageID =  params.LanguageID;
        }

        if(params.StartDate) {
            filter.StartDate =  {"$gte":params.StartDate};
        }

        if(params.EndDate) {
            filter.EndDate =  {"$lte":params.EndDate};
        }

        if(params.Estatus) {
            filter.Estatus = params.Estatus;
        }

        console.log("Filtro", filter);

        var response = await LogModel.find(filter);

        res.status(global.constants.STATUS_200_OK).send(response);
        
    } catch (error) {
        console.log(error);
        res.status(global.constants.STATUS_500_INTERNAL_SERVER_ERROR).send([{
            message: await LangHelper.lang('ERROR_MESSAGE_GENERIC')
        }]);
    }
});

module.exports = router;