import express from 'express';
import TourDetailModel from '../../models/TourDetailModel'

const router = express.Router();

router.get('/v1/tour-details', async (req, res) => {

    try {

        var response = await TourDetailModel.find();
        res.status(global.constants.STATUS_200_OK).send(response);
        
    } catch (error) {
        console.log(error);
        res.status(global.constants.STATUS_500_INTERNAL_SERVER_ERROR).send([{
            message: await LangHelper.lang('ERROR_MESSAGE_GENERIC')
        }]);
    }
});

module.exports = router;